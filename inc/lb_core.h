#ifndef __LB_CORE_PUBLIC_H__
#define __LB_CORE_PUBLIC_H__
#include <glib-2.0/glib.h>
#include <glib-2.0/gio/gio.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <systemd/sd-bus.h>
#include "lb_base.h"
#include "lb_model.h"
#include "lb_error.h"
#include "lb_persistence.h"

#ifdef __cplusplus
extern "C" {
#endif

#define G_OPTION_ARG_MUST 0x80
#define G_OPTION_ARG_OPTIONAL 0x7f

#define REG_METHOD_NOT_AVALIBALE    -9999

#ifdef __cplusplus
}
#endif

#endif
