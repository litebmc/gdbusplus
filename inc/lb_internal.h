#ifndef __LB_INTERNAL_ERROR_H__
#define __LB_INTERNAL_ERROR_H__

#ifdef __cplusplus
extern "C" {
#endif

GSList *lb_interfaces(gboolean is_remote);

// @notes: 创建新对象，不存在时创建新对象，intf_desc必须是服务端接口描述符
// @notes: 返回的对象引用计数已加1，使用完后调用lbo_unref减引用计数
LBO *lbo_new(const LBInterface *intf_desc, const gchar *obj_name, gboolean *exist);
// @notes: 减少引用计数，当对象引用计数为零时会释放对象并将obj置空
void lbo_unref(LBO **obj);
// @notes: 增加引用计数
LBO *lbo_ref(LBO *obj);

static inline void lbo_unref_p(LBO *obj)
{
    lbo_unref(&obj);
}

static inline void _cleanup_lbo_(LBO **ptr)
{
    if (ptr && *ptr) {
        lbo_unref(ptr);
    }
}

#define cleanup_lbo __attribute__((cleanup(_cleanup_lbo_)))

/*
 * 不建议手工调用本方法创建对象，除非你确定服务、对象、接口必然存在
 * 建议的方式：lb_subscribe_interface订阅接口结合interface_manager服务以及对象事件完成对象自发现
 *
 * @notes: 获取新cli对象，不存在时创建新对象,intf_desc必须是客户端接口描述符
 * @notes: 客户端对象一般是使用interface_manager自动发现服务实现自动创建
 * @notes: 具体见lb_subscribe_interface接口说明，开发者仅需监听对象事件即可(lb_interface_on_changed)
 * @notes: 手工调用本方法创建的对象处于"不在位"状态，需要调用lbo_present_set设置为在位。
 * @notes: 返回的对象引用计数已加1，使用完后调用lbo_unref减引用计数
 */
LBO *lbo_cli_new(const LBInterface *intf_desc, const gchar *well_known, const gchar *obj_name);
LBO *lbo_cli_get(const LBInterface *intf_desc, const gchar *well_known, const gchar *obj_name);

void lbo_present_set(LBO *lb_obj, gboolean present);
gboolean lbo_present(LBO *lb_obj);

#ifdef __cplusplus
}
#endif

#endif
