#ifndef __LB_CORE_MODEL_H__
#define __LB_CORE_MODEL_H__

#ifdef __cplusplus
extern "C" {
#endif

// 仅用于获取当前服务实现的接口描述，用于类似Connector服务中动态创建对象
const LBInterface *lb_get_interface(gboolean is_remote, const gchar *intf_name);

// 订阅接口，从interface_manager获取整个系统所有实现该接口的对象并接收对象创建、销毁事件，实现cli对象自动发现
gint lb_subscribe_interface(const gchar *interface_name);

const LBProperty *lbo_get_property_desc(LBO *lb_obj, const gchar *pname);

// 获取GDBusConnection句柄
GDBusConnection *lb_get_bus(void);

const gchar *lb_bus_name(void);
const gchar *lb_app_name(void);

typedef struct {
    guint32 magic;
    guint32 reserved;
    GDBusMethodInvocation *invocation;
    GVariant *direct_replay;
} LBMethodExt;
void lb_start(const gchar *bus_name);
// NOTICE: MUST BE called before lb_start
void lb_set_bus_type(GBusType bus_type);
GBusType lb_get_bus_type(void);

// 使用unique_name的well_known服务名
gchar *lb_well_known(const gchar *unique_name);
// 获取well_known的unique_name服务名
gchar *lb_unique_name(const gchar *well_known);

// 定义com.litebmc.interface_manager服务名和对象
#define INTERFACE_MAMAGER_SERVICE      "com.litebmc.interface_manager"
#define INTERFACE_MAMAGER_PATH         "/com/litebmc/interface_manager"

// 定义org.freedesktop.DBus服务名及对象
#define DBUS_SERVICE "org.freedesktop.DBus"
#define DBUS_PATH "/org/freedesktop/DBus"

#ifdef BUILD_TEST
gboolean lb_wait_internal_queue_empty(guint32 timeout_sec);
#endif
#ifndef NDEBUG
// 调试功能，用于打印本地所有对象状态
void lb_print_all_objects(void);
#endif

#ifdef __cplusplus
}
#endif

#endif