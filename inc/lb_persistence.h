#ifndef __PERSISTENCE_H__
#define __PERSISTENCE_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    PER_SAVE = 1,      // 掉电持久化
    PER_POWER_OFF = 1, // 掉电持久化（掉电不丢失）
    PER_REBOOT = 2,    // 内存持久化（重启不丢失，掉电丢失）
} PER_TYPE;

typedef struct {
    const gchar *group;
    const gchar *key;
    guchar *data;
    gsize length;
    struct {
        PER_TYPE type : 3;
        guint32 is_static: 1;
    };
} PER_ITEM;

// 恢复持久化记录
gint per_load(PER_ITEM *item);
// 保存持久化数据
gint per_save(PER_ITEM *item);

void persist_save_property(const gchar *object, const gchar *prop, GVariant *data, PER_TYPE per_type);
gint persist_load_property(const gchar *object, const gchar *prop, GVariant **value, PER_TYPE per_type);

#ifdef __cplusplus
}
#endif

#endif