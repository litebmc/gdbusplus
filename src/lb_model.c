#include "sys/file.h"
#include "lb_core.h"
#include "lb_internal.h"

#define OBJECT_MAGIC "LiTeBmC"
static GBusType g_bus_type = G_BUS_TYPE_SESSION;

typedef enum { LB_BUS_FREE = 0, LB_BUS_CONNECTING = 1, LB_BUS_BUS_ACCQUIRED = 2, LB_BUS_NAME_ACCQUIRED = 4 } LB_BUS_STATE;

typedef struct {
    gchar *bus_name;
    GDBusConnection *bus;
    LB_BUS_STATE bus_state;
} GdbDbusConnection;

static GSList *server_intf = NULL;
static GSList *remote_intf = NULL;

typedef struct _CoreObject CoreObject;

#ifndef NDEBUG
LBList g_objects;
#endif

typedef struct {
    GQuark iid;             // interface id
    LBList ols;            // ObjectListner list
    LBList objects;        // objects list
    GHashTable *object_table;
    LBInterface *lb_intf;
    guint8 prop_cnt;
} CoreInterface;

typedef struct {
    CoreInterface *intf;
    lbo_signal_handler callback;
    gpointer user_data;
    guint id;
    const LBSignal *signal;
} CoreSignal;

typedef enum {
    PROPERTY_BEFORE_CHANGED = (1 << 0),
    PROPERTY_AFTER_CHANGED = (1 << 1),
    OBJECT_AFTER_CHANGED = (1 << 2),
} PROPERTY_EVNET_TYPE;

typedef struct {
    struct {
        CoreObject *core_obj;
        PROPERTY_EVNET_TYPE type;
        GVariant *value;
        GVariant *prev_value;
        LBProperty *prop;
        guint32 force_emit_change: 1;
    };
    guint flags;
} CorePropertyExt;

static guint oid_allocater = 1;
typedef struct _CoreObject {
    char magic[8];
    LBO *lb_obj;
    gchar *bus_name;
    char *name;
    guint oid;
    LBList list_head;    // 挂接在intf->objects链接上
    gatomicrefcount ref_count;
    union {
        struct {
            guint32 is_present : 1;
            guint32 is_object_path : 1;
        };
    };
    union {
        guint object_register_id;
        guint property_changed_subscribe;
    };
    gpointer bind_data;
    GDestroyNotify bind_destroy;
    CoreInterface *intf;
    LBList pls;    // 挂接属性变更状态变更事件的监听钓子
    LBList free_ols;   // 挂接对象释放事件的监听钓子
#ifndef NDEBUG
    LBList object_list;    // 链接到g_object_list
#endif
    CorePropertyExt extend[0]; // CorePropertyExt list
} CoreObject;

typedef struct {
    LBList list_head;  // 挂载到CoreObject的pls链表
    const LBProperty *prop;
    lbo_after_changed_hook hook;
    gpointer user_data;
    GDestroyNotify destroy;
} PropListener;

typedef struct {
    LBList list_head;  // 挂载到CoreObject的ols链表或者CoreInterface的ols链表
    LbObjectHook hook;
    gpointer user_data;
    GDestroyNotify destroy;
} ObjectListener;

typedef struct {
    LBList list_head;
    GHookFunc hook;
    gpointer user_data;
} ObjectFreeHook;

G_LOCK_DEFINE_STATIC(lock);

static GAsyncQueue *prop_chg_tp[4];
static GAsyncQueue *objt_chg_tp[4];

#ifdef BUILD_TEST
#define INVALID_QUEUE_MSG GUINT_TO_POINTER(1)
static gint _dbus_msg_cnt = 0;
#define dbus_msg_call_and_send() g_atomic_int_inc(&_dbus_msg_cnt)
#define dbus_msg_cnt() g_atomic_int_get(&_dbus_msg_cnt)
#else
#define dbus_msg_call_and_send()
#define dbus_msg_cnt() 0
#endif

static GdbDbusConnection g_session_bus;

static void _property_after_changed(LBO *core_obj, const LBProperty *prop);
static gint lb_get_remote_all_val(LBO *lb_obj);
static LBO *lbo_get(const LBInterface *intf_desc, const gchar *obj_name);
static GSList *lb_objects(const LBInterface *intf_desc);

static inline gboolean is_remote_object(CoreObject *core_obj)
{
    return core_obj->intf->lb_intf->is_remote;
}

static inline const gchar *intf_name(CoreObject *core_obj)
{
    return core_obj->intf->lb_intf->name;
}

static inline LBProperty *intf_properties(CoreObject *core_obj)
{
    return core_obj->intf->lb_intf->properties;
}

const LBInterface *lb_get_interface(gboolean is_remote, const gchar *intf_name)
{
    g_assert(intf_name);
    GSList **intf_list;

    const LBInterface *out = NULL;
    GQuark iid = g_quark_from_string(intf_name);
    G_LOCK(lock);
    if (is_remote) {
        intf_list = &remote_intf;
    } else {
        intf_list = &server_intf;
    }
    for (GSList *node = *intf_list; node; node = node->next) {
        CoreInterface *tmp_intf = (CoreInterface *)node->data;
        if (tmp_intf->iid == iid) {
            out = tmp_intf->lb_intf;
            break;
        }
    }
    G_UNLOCK(lock);
    return out;
}

static void lb_register_interface(LBInterface *lb_intf)
{
    g_assert(lb_intf && lb_intf->interface);

    gboolean found = FALSE;
    GSList **intf_list;

    G_LOCK(lock);
    if (lb_intf->is_remote) {
        intf_list = &remote_intf;
    } else {
        intf_list = &server_intf;
    }

    GQuark iid = g_quark_from_string(lb_intf->name);
    for (GSList *item = *intf_list; item; item = item->next) {
        CoreInterface *intf = (CoreInterface *)item->data;
        if (intf->iid == iid) {
            found = TRUE;
            break;
        }
    }
    if (!found) {
        CoreInterface *intf = g_new0(CoreInterface, 1);
        intf->lb_intf = lb_intf;
        intf->iid = iid;
        lb_list_init(&intf->objects);
        lb_list_init(&intf->ols);
        intf->object_table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
        for (LBProperty *prop = lb_intf->properties; prop && prop->info; prop++) {
            if (intf->prop_cnt < (prop->id + 1)) {
                intf->prop_cnt = prop->id + 1;
            }
        }
        lb_intf->opaque = (gpointer)intf;
        *intf_list = g_slist_prepend(*intf_list, (gpointer)intf);
    }
    G_UNLOCK(lock);
    log_info("interface %s inserted", lb_intf->name);
}

static void _on_object_chg_worker(GAsyncQueue *queue)
{
    CoreObject *core_obj = NULL;
    while (1) {
        core_obj = (CoreObject *)g_async_queue_pop(queue);
#ifdef BUILD_TEST
        if (core_obj == INVALID_QUEUE_MSG) {
            continue;
        }
#endif
        GDB_LIST_FOREACH(&core_obj->intf->ols, node) {
            ObjectListener *ol = CONTAINER_OF(node, ObjectListener, list_head);
            ol->hook(core_obj->lb_obj, ol->user_data);
        }
        lbo_unref(&core_obj->lb_obj);
    }
}

/*
 * 框架感知到属性变更时发起PropertiesChanged消息
 */
static void _lb_properties_changed_event(CoreObject *core_obj, const LBProperty *prop, GVariant *value)
{
    if (!(prop->flags & (LB_FLAGS_PROPERTY_EMIT_TRUE | LB_FLAGS_PROPERTY_EMIT_INVALIDATES))) {
        log_debug("Property %s.%s changed, flags is %d, no need to send PropertiesChanged signal", core_obj->name, prop->name, prop->flags);
        return;
    }
    if (!core_obj->is_object_path)
        return;
    GVariantBuilder *value_builder = NULL;
    GVariantBuilder *invalidated_builder = NULL;
    if (prop->flags & LB_FLAGS_PROPERTY_EMIT_TRUE) {
        value_builder = g_variant_builder_new(G_VARIANT_TYPE_ARRAY);
        g_variant_ref(value);
        g_variant_builder_add(value_builder, "{sv}", prop->name, value);
    } else if (prop->flags & LB_FLAGS_PROPERTY_EMIT_INVALIDATES) {
        invalidated_builder = g_variant_builder_new(G_VARIANT_TYPE("as"));
        g_variant_builder_add(invalidated_builder, "s", prop->name);
    }
    log_mass("send property changed message, property: %s.%s", core_obj->name, prop->name);
    GVariant *parameters = g_variant_new("(sa{sv}as)", intf_name(core_obj), value_builder, invalidated_builder);
    dbus_msg_call_and_send();
    g_dbus_connection_emit_signal(g_session_bus.bus, NULL, core_obj->name, "org.freedesktop.DBus.Properties", "PropertiesChanged",
                                  parameters, NULL);
    if (value_builder)
        g_variant_builder_unref(value_builder);
    if (invalidated_builder)
        g_variant_builder_unref(invalidated_builder);
}

/**
 * @brief 属性变更
 *
 * @param core_obj 对象句柄
 * @param prop 属性描述
 * @param value 属性值
 */
static void _property_changed(CoreObject *core_obj, const LBProperty *prop, GVariant *value)
{
    GDB_LIST_FOREACH(&core_obj->pls, node) {
        PropListener *pl = CONTAINER_OF(node, PropListener, list_head);
        if (pl->hook && pl->prop == prop) {
            pl->hook(core_obj->lb_obj, prop, value, pl->user_data);
        }
    }
}

/**
 * @brief 对象变更
 *
 * @param lb_obj 对象句柄
 * @param prop 属性描述符
 */
static gint _lbo_before_change_hook(CoreObject *core_obj, const LBProperty *prop, GVariant *value, GError **error)
{
    gint ret;
    for (GSList *node = prop->hooks; node && node->data; node = node->next) {
        LBPropertyHook *hook = (LBPropertyHook *)node->data;
        if (hook->before) {
            lbo_lock(core_obj->lb_obj);
            ret = hook->before(core_obj->lb_obj, prop, value, hook->user_data, error);
            lbo_unlock(core_obj->lb_obj);
            if (ret != 0)
                return ret;
        }
    }
    return 0;
}

/**
 * @brief 对象变更
 *
 * @param lb_obj 对象句柄
 * @param prop 属性描述符
 */
static void _object_changed(CoreObject *core_obj, const LBProperty *prop, GVariant *value)
{
    for (GSList *node = prop->hooks; node && node->data; node = node->next) {
        LBPropertyHook *hook = (LBPropertyHook *)node->data;
        if (hook->after) {
            lbo_lock(core_obj->lb_obj);
            hook->after(core_obj->lb_obj, prop, value, hook->user_data);
            lbo_unlock(core_obj->lb_obj);
        }
    }
}

/**
 * @brief 属性变更事件队列处理函数
 *
 * @param data PropChgInfo
 * @param user_data
 */
static void _on_changed_worker(GAsyncQueue *queue)
{
    CorePropertyExt *prop_ext = NULL;
    while (1) {
        prop_ext = (CorePropertyExt *)g_async_queue_pop(queue);
#ifdef BUILD_TEST
        if (prop_ext == INVALID_QUEUE_MSG) {
            continue;
        }
#endif
        PROPERTY_EVNET_TYPE type;
        cleanup_lbo LBO *lb_obj = prop_ext->core_obj->lb_obj;
        lbo_lock(lb_obj);
        gboolean force = prop_ext->force_emit_change;
        prop_ext->force_emit_change = 0;
        GVariant *value = prop_ext->value;
        type = prop_ext->type;
        prop_ext->type = 0;
        prop_ext->value = NULL;
        lbo_unlock(lb_obj);
        if (type & PROPERTY_BEFORE_CHANGED) {
            /* 来自于不同对象的不同属性变更，设置到对象 */
            lbo_set_memory(lb_obj, prop_ext->prop, value);
            g_variant_unref(value);
            continue;
        }
        if (!value) {
            value = lbo_get_memory(lb_obj, prop_ext->prop);
            if (!value) {
                log_debug("Property %s.%s value is null", prop_ext->core_obj->name, prop_ext->prop->name);
                continue;
            }
        }
        if (prop_ext->prev_value) {
            if (!force && g_variant_equal(prop_ext->prev_value, value)) {
                continue;
            }
            g_variant_unref(prop_ext->prev_value);
        }
        prop_ext->prev_value = value;
        _object_changed(prop_ext->core_obj, prop_ext->prop, value);
        if (!is_remote_object(prop_ext->core_obj) && !(prop_ext->prop->flags & LB_FLAGS_PROPERTY_PRIVATE)) {
            _lb_properties_changed_event(prop_ext->core_obj, prop_ext->prop, value);
        }
        _property_changed(prop_ext->core_obj, prop_ext->prop, value);
    }
}

static void __attribute__((constructor(101))) _lb_init(void)
{
    GThread *thread = NULL;
    for (int i = 0; i < G_N_ELEMENTS(prop_chg_tp); i++) {
        prop_chg_tp[i] = g_async_queue_new();
        thread = g_thread_new("PpChg", (GThreadFunc)_on_changed_worker, (gpointer)prop_chg_tp[i]);
        g_thread_unref(thread);
    }
    for (int i = 0; i < G_N_ELEMENTS(objt_chg_tp); i++) {
        objt_chg_tp[i] = g_async_queue_new();
        thread = g_thread_new("ObjChg", (GThreadFunc)_on_object_chg_worker, (gpointer)objt_chg_tp[i]);
        g_thread_unref(thread);
    }
}

static gint lbo_call_method(LBO *lb_obj, const LBMethod *method, const void *req, void **rsp, gint timeout_msec, GError **error)
{
    g_assert(lb_obj && method);

#ifndef NDEBUG
    const LBInterface *intf_desc = lbo_interface(lb_obj);
    /* 仅在调试模式检查Method是否为自动分配的内存 */
    gboolean found = FALSE;
    for (const LBMethod *tmp = intf_desc->methods; tmp && tmp->name; tmp++) {
        if (tmp == method) {
            found = TRUE;
            break;
        }
    }
    if (!found) {
        *error = LbError_MethodNotExist_new(method->name);
        return -1;
    }
#endif
    GVariant *parameters = method->req_encode(req);
    if (parameters == NULL) {
        *error = LbError_MsgDecodeFailed_new();
        return -1;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    g_variant_take_ref(parameters);
    dbus_msg_call_and_send();
    GVariant *replay =
        g_dbus_connection_call_sync(g_session_bus.bus, core_obj->bus_name, core_obj->name, intf_name(core_obj), method->name, parameters,
                                    G_VARIANT_TYPE(method->rsp_signature), G_DBUS_CALL_FLAGS_NONE, timeout_msec, NULL, error);
    g_variant_unref(parameters);
    if (replay == NULL) {
        return -1;
    }
    if (rsp) {
        *rsp = method->rsp_decode(replay);
    }
    g_variant_unref(replay);
    return 0;
}

static GVariant *_if_object_properties_get(const gchar *object_path, const gchar *interface_name, const gchar *property_name,
                                           GError **error)
{
    log_mass("get property: %s.%s start", object_path, property_name);
    const LBInterface *intf_desc = lb_get_interface(FALSE, interface_name);
    if (!intf_desc) {
        *error = LbError_InterfaceNotExist_new(interface_name);
        return NULL;
    }
    cleanup_lbo LBO *lb_obj = lbo_get(intf_desc, object_path);
    if (!lb_obj) {
        *error = LbError_ObjectNotExist_new(object_path);
        return NULL;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    if (!core_obj->is_object_path) {
        *error = LbError_ObjectNotExported_new(object_path);
        return NULL;
    }
    LBProperty *prop = NULL;
    for (LBProperty *tmp_prop = intf_properties(core_obj); tmp_prop && tmp_prop->info; tmp_prop++) {
        if (!g_strcmp0(tmp_prop->name, property_name)) {
            prop = tmp_prop;
            break;
        }
    }
    if (!prop) {
        *error = LbError_PropertyNotExist_new(object_path, property_name);
        return NULL;
    }
    lbo_lock(lb_obj);
    GVariant *val = lbo_get_memory(lb_obj, prop);
    lbo_unlock(lb_obj);
    log_mass("get property: %s.%s finish", object_path, property_name);
    return g_variant_new("(v)", val);
}

static void _if_object_properties_set(const gchar *object_path, const gchar *interface_name, const gchar *property_name, GVariant *value,
                                      GError **error)
{
    gboolean need_send = TRUE;

    const LBInterface *intf_desc = lb_get_interface(FALSE, interface_name);
    if (!intf_desc) {
        *error = LbError_InterfaceNotExist_new(interface_name);
        return;
    }
    cleanup_lbo LBO *lb_obj = lbo_get(intf_desc, object_path);
    log_mass("set property: %s.%s start", object_path, property_name);
    LBProperty *prop = NULL;
    if (!lb_obj) {
        *error = LbError_ObjectNotExist_new(object_path);
        return;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    if (!core_obj->is_object_path) {
        *error = LbError_ObjectNotExported_new(object_path);
        return;
    }
    for (LBProperty *tmp_prop = intf_properties(core_obj); tmp_prop && tmp_prop->info; tmp_prop++) {
        if (!g_strcmp0(tmp_prop->name, property_name)) {
            prop = tmp_prop;
            break;
        }
    }
    if (!prop) {
        *error = LbError_PropertyNotExist_new(object_path, property_name);
        return;
    }
#ifdef LB_CODEGEN_BE_5_2
    GError *validate_error = NULL;
    if (prop->check && !prop->check(lb_obj, value, &validate_error)) {
        if (validate_error) {
            *error = LbError_PropertyValidateFailed_new(object_path, property_name, validate_error->message);
            g_error_free(validate_error);
        } else {
            *error = LbError_PropertyValidateFailed_new(object_path, property_name, "unknown");
        }
        return;
    }
#endif
    if (g_strcmp0(prop->info->signature, g_variant_get_type_string(value))) {
        *error = LbError_SignatureNotMatch_new(g_variant_get_type_string(value), prop->info->signature);
        return;
    }
    gint ret = _lbo_before_change_hook(core_obj, prop, value, error);
    if (ret != 0) {
        *error = LbError_PropertyBeforeCheckFailed_new(object_path, property_name, ret);
        return;
    }
    lbo_lock(lb_obj);
    CorePropertyExt *prop_ext = core_obj->extend + prop->id;
    prop_ext->type = PROPERTY_BEFORE_CHANGED;
    if (prop_ext->value) {
        g_variant_unref(prop_ext->value);
        need_send = FALSE;
    }
    prop_ext->value = g_variant_ref(value);
    lbo_unlock(lb_obj);
    if (need_send) {
        g_atomic_ref_count_inc(&core_obj->ref_count);
        g_async_queue_push(prop_chg_tp[core_obj->oid % G_N_ELEMENTS(prop_chg_tp)], (gpointer)prop_ext);
    }
    log_mass("set property: %s.%s finish", object_path, property_name);
}

static GVariant *_if_object_properties_getall(const gchar *interface_name, const gchar *object_path, GError **error)
{
    GVariantBuilder builder;
    const LBInterface *intf_desc = lb_get_interface(FALSE, interface_name);
    if (!intf_desc) {
        *error = LbError_InterfaceNotExist_new(interface_name);
        return NULL;
    }
    cleanup_lbo LBO *lb_obj = lbo_get(intf_desc, object_path);
    if (!lb_obj) {
        *error = LbError_ObjectNotExist_new(object_path);
        return NULL;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));
    for (LBProperty *prop = intf_properties(core_obj); prop && prop->info; prop++) {
        if ((prop->flags & LB_FLAGS_PROPERTY_PRIVATE) || !(prop->info->flags & G_DBUS_PROPERTY_INFO_FLAGS_READABLE)) {
            continue;
        }
        GVariant *val = lbo_get_memory(lb_obj, prop);
        if (!val) {
            continue;
        }
        g_variant_builder_add(&builder, "{sv}", prop->name, val);
    }
    return g_variant_new("(a{sv})", &builder);
}

static void _if_property_methods_worker(GDBusMethodInvocation *invocation)
{
    GVariant *replay = NULL;
    GError *error = NULL;
    gchar *prop_interface = NULL;
    gchar *prop_name = NULL;
    GVariant *prop_value = NULL;
    const gchar *object_path = g_dbus_method_invocation_get_object_path(invocation);
    const gchar *method_name = g_dbus_method_invocation_get_method_name(invocation);
    GVariant *parameters = g_dbus_method_invocation_get_parameters(invocation);
    if (g_strcmp0(method_name, "GetAll") == 0) {
        g_variant_get(parameters, "(&s)", &prop_interface);
        replay = _if_object_properties_getall(prop_interface, object_path, &error);
    }
    if (g_strcmp0(method_name, "Get") == 0) {
        g_variant_get(parameters, "(&s&s)", &prop_interface, &prop_name);
        replay = _if_object_properties_get(object_path, prop_interface, prop_name, &error);
    }
    if (g_strcmp0(method_name, "Set") == 0) {
        g_variant_get(parameters, "(&s&sv)", &prop_interface, &prop_name, &prop_value);
        _if_object_properties_set(object_path, prop_interface, prop_name, prop_value, &error);
        g_variant_unref(prop_value);
    }
    if (error) {
        g_dbus_method_invocation_return_gerror(invocation, error);
        g_error_free(error);
    } else {
        g_dbus_method_invocation_return_value(invocation, replay);
    }
}

static gint _if_object_method_called(GDBusMethodInvocation *invocation, LBO *lb_obj, const LBMethod *method, GVariant **replay,
                                     GError **error)
{
    GVariant *parameters = g_dbus_method_invocation_get_parameters(invocation);
    gint ret = REG_METHOD_NOT_AVALIBALE;
    if (method->handler) {
        void *req = method->req_decode(parameters);
        void *rsp = NULL;
        #ifdef LB_VERSION_BE_0_8_2
        LBMethodExtData ext = {
            .invocation = invocation,
            .direct_replay = NULL
        };
        #else
        LBMethodExt ext = {
            .magic = 0xdeadbeaf,
            .invocation = invocation,
            .direct_replay = NULL
         };
        #endif
        ret = method->handler(lb_obj, req, &rsp, error, &ext);
        #ifndef NDEBUG
        if (ret != 0) {
            if (!(*error)) {
                log_warn("Call method %s failed but *error is NULL", method->name);
            } else {
                log_debug("Call method %s failed because return non-zero code, ret: %d, error msg: %s",
                    method->name, ret, (*error)->message);
            }
        } else if (ext.direct_replay == NULL && rsp == NULL) {
            log_debug("Call method %s failed because response is NULL", method->name);
        }
        #endif
        if (ret == 0) {
            if (ext.direct_replay) {
                *replay = ext.direct_replay;
            } else {
                *replay = method->rsp_encode(rsp);
                method->rsp_free(&rsp);
            }
        }
        method->req_free(&req);
    }
    return ret;
}

static void _if_object_methods_worker(GDBusMethodInvocation *invocation)
{
    GError *error = NULL;
    GVariant *replay = NULL;
    gint ret = 0;
    const gchar *interface_name = g_dbus_method_invocation_get_interface_name(invocation);
    if (g_strcmp0(interface_name, "org.freedesktop.DBus.Properties") == 0) {
        return _if_property_methods_worker(invocation);
    }
    const gchar *object_path = g_dbus_method_invocation_get_object_path(invocation);
    const gchar *method_name = g_dbus_method_invocation_get_method_name(invocation);
    do {
        const LBInterface *intf_desc = lb_get_interface(FALSE, interface_name);
        if (!intf_desc) {
            error = LbError_InterfaceNotExist_new(interface_name);
            break;
        }
        cleanup_lbo LBO *lb_obj = lbo_get(intf_desc, object_path);
        if (!lb_obj) {
            error = LbError_ObjectNotExist_new(object_path);
            break;
        }
        for (const LBMethod *method = intf_desc->methods; method && method->name; method++) {
            if (g_strcmp0(method->name, method_name))
                continue;
            ret = _if_object_method_called(invocation, lb_obj, method, &replay, &error);
            if (ret == 0) {
                g_dbus_method_invocation_return_value(invocation, replay);
                return;
            }
            break;
        }
        if (!error) {
            if (ret == REG_METHOD_NOT_AVALIBALE) {
                error = LbError_MethodNotImplament_new(method_name);
            } else {
                error = LbError_MethodFailed_new(method_name, ret);
            }
        }
    } while (0);
    if (error) {
        g_dbus_method_invocation_return_gerror(invocation, error);
        g_error_free(error);
    }
}

static void _if_object_methods(GDBusConnection *connection, const gchar *sender_name, const gchar *object_path, const gchar *interface_name,
                               const gchar *method_name, GVariant *parameters, GDBusMethodInvocation *invocation, gpointer user_data)
{
    dbus_msg_call_and_send();
    GThread *thread = g_thread_new("MthCall", (GThreadFunc)_if_object_methods_worker, (gpointer)invocation);
    g_thread_unref(thread);
}

// get_property和set_property置空
static const GDBusInterfaceVTable _if_object_interface_vtable = {
    _if_object_methods,
    NULL,
    NULL,
};

static CoreObject *_new_core_obj(const LBInterface *intf_desc, const gchar *obj_name, const gchar *well_known)
{
    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    CoreObject *core_obj = NULL;
    // 分配内存，在PmOjbect末尾追加一个Proto3消息体
    core_obj = (CoreObject *)g_malloc0(sizeof(CoreObject) + sizeof(CorePropertyExt) * intf->prop_cnt);
    gint id = 0;
    for (LBProperty *prop = intf_desc->properties; prop && prop->info; prop++) {
        core_obj->extend[id].core_obj = core_obj;
        core_obj->extend[id].prop = prop;
        id++;
    }
    gchar *new_obj_name = g_strdup(obj_name);
    core_obj->lb_obj = intf_desc->create(new_obj_name, (gpointer)core_obj);
    core_obj->intf = intf;
    core_obj->name = new_obj_name;
    core_obj->oid = oid_allocater++;
    core_obj->bus_name = g_strdup(well_known);
    lb_list_init(&core_obj->pls);
    lb_list_init(&core_obj->free_ols);
    g_atomic_ref_count_init(&core_obj->ref_count);
#ifndef NDEBUG
    // 当前函数处于lock锁保护，无需加锁
    lb_list_insert_before(&g_objects, &core_obj->object_list);
#endif
    return core_obj;
}

/**
 * @brief 创建新对象
 *
 * @param intf_desc 接口描述符，请使用接口的服务端(server)组件定义的<interface_alias>宏
 * @param obj_name 对象名
 * @param *exist(output) 对象是否已存在
 * @return LBO 对象句柄
 * @notes: 1、只有明确给出对象路径并且是接口对象的才会注册vtable; 2. 如果是新创建的对象，其引用计数为1; 3. 如果对象已存在则引用计数加1
 */
LBO *lbo_new(const LBInterface *intf_desc, const gchar *obj_name, gboolean *exist)
{
    g_assert(intf_desc && obj_name);

    CoreObject *core_obj = NULL;
    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    g_assert(intf && !intf_desc->is_remote);
    G_LOCK(lock);
    core_obj = g_hash_table_lookup(intf->object_table, obj_name);
    if (core_obj) {
        if (exist) {
            *exist = TRUE;
        }
        // 存储在hash表的对象没有加引用计数，查询对象后需要在lock锁保护下完成引用计数加1操作
        g_atomic_ref_count_inc(&core_obj->ref_count);
        G_UNLOCK(lock);
        return core_obj->lb_obj;
    }
    if (exist) {
        *exist = FALSE;
    }
    core_obj = _new_core_obj(intf_desc, obj_name, lb_bus_name());
    g_hash_table_insert(intf->object_table, g_strdup(core_obj->name), core_obj);
    // 添加到类的对象列表
    lb_list_insert_before(&intf->objects, &core_obj->list_head);
    if (g_variant_is_object_path(obj_name))
        core_obj->is_object_path = 1;
    G_UNLOCK(lock);
    return core_obj->lb_obj;
}

LBO *lbo_ref(LBO *obj)
{
    g_assert(obj);
    if (!obj) {
        return NULL;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(obj);
    g_atomic_ref_count_inc(&core_obj->ref_count);
    return obj;
}

static void _core_obj_free(CoreObject *core_obj)
{
    CoreInterface *intf = core_obj->intf;

    // TODO: destroy object
    if (intf->prop_cnt) {
        for (int i = 0; i < intf->prop_cnt; i++) {
            if (core_obj->extend[i].value) {
                g_variant_unref(core_obj->extend[i].value);
                core_obj->extend[i].value = NULL;
            }
        }
        memset(core_obj->extend, 0, sizeof(CorePropertyExt) * intf->prop_cnt);
    }
    for (LBList *node = core_obj->free_ols.next; node != &core_obj->free_ols;) {
        LBList *next = node->next;
        ObjectFreeHook *free_hook = CONTAINER_OF(node, ObjectFreeHook, list_head);
        free_hook->hook(free_hook->user_data);
        // 链接节点一并释放
        g_free(free_hook);
        node = next;
    }
    for (LBList *node = core_obj->pls.next; node != &core_obj->pls;) {
        LBList *next = node->next;
        PropListener *pl = CONTAINER_OF(node, PropListener, list_head);
        // 释放用户数据
        if (pl->destroy) {
            pl->destroy(pl->user_data);
        }
        // 链接节点一并释放
        g_free(pl);
        node = next;
    }
    if (core_obj->bind_data && core_obj->bind_destroy) {
        core_obj->bind_destroy(core_obj->bind_data);
    }
    intf->lb_intf->destroy(core_obj->lb_obj);
    g_free(core_obj->name);
    g_free(core_obj->bus_name);
    memset(core_obj, 0, sizeof(*core_obj));
}

/**
 * @brief 减少对象的引用计数，当对象引用计数为0时释放对象并将obj置空
 * @note   调用方减少引用计数后不得再使用obj
 * @param  **obj: 对象句柄
 * @retval None
 */
void lbo_unref(LBO **obj)
{
    g_assert(obj && *obj);
    if (obj == NULL || *obj == NULL) {
        return;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(*obj);
    LBInterface *lb_intf = core_obj->intf->lb_intf;

    G_LOCK(lock);
    // 通过接口hash表查找对象时在锁保护下加1，减引用计数也必须在锁保护下执行
    if (!g_atomic_ref_count_dec(&core_obj->ref_count)) {
        G_UNLOCK(lock);
        return;
    }
    if (lb_intf->is_remote) {
        GHashTable *obj_table = g_hash_table_lookup(core_obj->intf->object_table, core_obj->bus_name);
        if (obj_table) {
            CoreObject *tmp_obj = g_hash_table_lookup(obj_table, core_obj->name);
            if (tmp_obj) {
                g_hash_table_remove(obj_table, core_obj->name);
                lb_list_remove(&core_obj->list_head);
            }
        }
    } else {
        core_obj = g_hash_table_lookup(core_obj->intf->object_table, core_obj->name);
        if (core_obj) {
            g_hash_table_remove(core_obj->intf->object_table, core_obj->name);
            lb_list_remove(&core_obj->list_head);
        }
    }
#ifndef NDEBUG
    lb_list_remove(&core_obj->object_list);
#endif
    G_UNLOCK(lock);
    _core_obj_free(core_obj);
    *obj = NULL;
}

/**
 * * @brief 获取客户端代理对象
 *
 * @param intf_desc 接口名
 * @param well_known 周知的服务名,又名bus_name
 * @param obj_name 对象名
 * @return LBO 对象句柄
 */
LBO *lbo_cli_new(const LBInterface *intf_desc, const gchar *well_known, const gchar *obj_name)
{
    g_assert(intf_desc && obj_name && well_known);

    GHashTable *obj_table = NULL;
    CoreObject *core_obj = NULL;
    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    g_assert(g_variant_is_object_path(obj_name));
    g_assert(intf && intf_desc->is_remote);
    G_LOCK(lock);
    obj_table = g_hash_table_lookup(intf->object_table, well_known);
    if (obj_table) {
        CoreObject *core_obj = g_hash_table_lookup(obj_table, obj_name);
        if (core_obj) {
            // 存储在hash表的对象没有加引用计数，查询对象后需要在lock锁保护下完成引用计数加1操作
            g_atomic_ref_count_inc(&core_obj->ref_count);
            if (!core_obj->is_present) {
                core_obj->is_present = 1;
                // 触发对象变更事件
                if (!lb_list_empty(&core_obj->intf->ols)) {
                    g_atomic_ref_count_inc(&core_obj->ref_count);
                    g_async_queue_push(objt_chg_tp[(GPOINTER_TO_UINT(core_obj->intf) / 8) % G_N_ELEMENTS(objt_chg_tp)], (gpointer)core_obj);
                }
            }
            G_UNLOCK(lock);
            return core_obj->lb_obj;
        }
    }
    core_obj = _new_core_obj(intf_desc, obj_name, well_known);
    if (!obj_table) {
        obj_table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
        g_hash_table_insert(intf->object_table, g_strdup(core_obj->bus_name), obj_table);
    }
    g_hash_table_insert(obj_table, g_strdup(core_obj->name), core_obj);
    // 添加到类的对象列表
    lb_list_insert_before(&intf->objects, &core_obj->list_head);
    core_obj->is_object_path = 1;
    core_obj->is_present = 1;
    G_UNLOCK(lock);
    // 触发属性变更事件
    if (g_strcmp0(intf_desc->name, "com.litebmc.InterfaceManager") != 0 &&
        g_strcmp0(intf_desc->name, "org.freedesktop.DBus") != 0 &&
        g_strcmp0(intf_desc->name, "org.freedesktop.DBus.ObjectManager")) {
        g_atomic_ref_count_inc(&core_obj->ref_count);
        GThread *thread = g_thread_new("GetAllVal", (GThreadFunc)lb_get_remote_all_val, (gpointer)core_obj->lb_obj);
        g_thread_unref(thread);
    }
    // 触发对象变更事件
    if (!lb_list_empty(&core_obj->intf->ols)) {
        g_atomic_ref_count_inc(&core_obj->ref_count);
        g_async_queue_push(objt_chg_tp[(GPOINTER_TO_UINT(core_obj->intf) / 8) % G_N_ELEMENTS(objt_chg_tp)], (gpointer)core_obj);
    }
    // 引用计数归零或不是接口时返回句柄
    return core_obj->lb_obj;
}

/**
 * @brief 按对象名查找远程对象句柄
 *
 * @param lb_obj 由lb_new_object申请的对象句柄
 */
LBO *lbo_cli_get(const LBInterface *intf_desc, const gchar *well_known, const gchar *obj_name)
{
    LBO *lb_obj = NULL;
    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;

    G_LOCK(lock);
    GHashTable *obj_table = g_hash_table_lookup(intf->object_table, well_known);
    if (obj_table) {
        CoreObject *core_obj = g_hash_table_lookup(obj_table, obj_name);
        if (core_obj) {
            // 存储在hash表的对象没有加引用计数，查询对象后需要在lock锁保护下完成引用计数加1操作
            g_atomic_ref_count_inc(&core_obj->ref_count);
            lb_obj = core_obj->lb_obj;
        }
    }
    G_UNLOCK(lock);
    return lb_obj;
}

void lbo_present_set(LBO *lb_obj, gboolean present)
{
    g_assert(lb_obj);

    GError *err = NULL;
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    LBInterface *lb_intf = core_obj->intf->lb_intf;
    lbo_lock(lb_obj);
    if (core_obj->is_present != present) {
        core_obj->is_present = present;
        if (!present) {
            if (!core_obj->intf->lb_intf->is_remote && core_obj->object_register_id) {
                g_dbus_connection_unregister_object(g_session_bus.bus, core_obj->object_register_id);
                core_obj->object_register_id = 0;
            }
        } else {
            if (!core_obj->intf->lb_intf->is_remote && !core_obj->object_register_id && g_variant_is_object_path(core_obj->name)) {
                core_obj->object_register_id = g_dbus_connection_register_object(g_session_bus.bus, core_obj->name, lb_intf->interface,
                                                                                &_if_object_interface_vtable, core_obj, NULL, &err);
                if (!core_obj->object_register_id) {
                    log_warn("Unable to register object, interface:%s, object:%s, message: %s", lb_intf->name, core_obj->name,
                             err->message);
                    g_error_free(err);
                }
            }
            // 触发属性变更事件
            for (const LBProperty *prop = lb_intf->properties; prop && prop->info; prop++) {
                CorePropertyExt *prop_ext = core_obj->extend + prop->id;
                prop_ext->force_emit_change = 1;
                _property_after_changed(lb_obj, prop);
            }
        }
        if (!lb_list_empty(&core_obj->intf->ols)) {
            g_atomic_ref_count_inc(&core_obj->ref_count);
            g_async_queue_push(objt_chg_tp[(GPOINTER_TO_UINT(core_obj->intf) / 8) % G_N_ELEMENTS(objt_chg_tp)], (gpointer)core_obj);
        }
    }
    lbo_unlock(lb_obj);
}

gboolean lbo_present(LBO *lb_obj)
{
    g_assert(lb_obj);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    return core_obj->is_present;
}

/**
 * @brief 按对象名查找对象句柄
 *
 * @param lb_obj 由lb_new_object申请的对象句柄
 */
static LBO *lbo_get(const LBInterface *intf_desc, const gchar *obj_name)
{
    g_assert(intf_desc && obj_name);

    LBO *lb_obj = NULL;
    CoreObject *core_obj = NULL;
    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    g_assert(intf && !intf->lb_intf->is_remote);
    if (!intf || intf->lb_intf->is_remote) {
        log_error("Interface is NULL or remote interface, object %s failed", obj_name);
        return NULL;
    }
    G_LOCK(lock);
    core_obj = g_hash_table_lookup(intf->object_table, obj_name);
    if (core_obj && core_obj->is_present) {
        g_atomic_ref_count_inc(&core_obj->ref_count);
        lb_obj = core_obj->lb_obj;
    }
    G_UNLOCK(lock);
    return lb_obj;
}

static inline LBProperty *_get_property_desc(LBO *lb_obj, const gchar *prop_name)
{
    const LBInterface *intf = lbo_interface(lb_obj);
    for (LBProperty *prop = intf->properties; prop && prop->name; prop++) {
        if (g_strcmp0(prop->name, prop_name) == 0)
            return prop;
    }
    return NULL;
}

const LBProperty *lbo_get_property_desc(LBO *lb_obj, const gchar *prop_name)
{
    g_assert(lb_obj && prop_name);

    return _get_property_desc(lb_obj, prop_name);
}

static void _mock_obj_chg(const LBInterface *intf_desc, LbObjectHook hook, gpointer user_data)
{
    GSList *objects = lb_objects(intf_desc);
    for (GSList *head = objects; head; head = head->next) {
        LBO *lb_obj = (LBO *)head->data;
        hook(lb_obj, user_data);
        lbo_unref(&lb_obj);
    }
    g_slist_free(objects);
}

/**
 * @brief 注册对象变更回调函数
 *
 * @param name 类名
 * @param hook 回调函数
 */
static void lb_interface_on_changed(const LBInterface *intf_desc, LbObjectHook hook, gpointer user_data, GDestroyNotify destroy)
{
    g_assert(intf_desc && hook);

    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    if (!intf) {
        log_error("Unknown interface %s, not registed", intf_desc->interface ? intf_desc->name : "???");
        return;
    }
    ObjectListener *ol = g_new0(ObjectListener, 1);
    ol->hook = hook;
    ol->user_data = user_data;
    ol->destroy = destroy;
    G_LOCK(lock);
    lb_list_insert_before(&intf->ols, &ol->list_head);
    G_UNLOCK(lock);
    _mock_obj_chg(intf_desc, hook, user_data);
}

/**
 * @brief 注册属性变更回调函数
 *
 * @param lb_obj 对象句柄
 * @param prop 属性名
 * @param hook 回调函数
 * @param user_data 用户回调参数
 *
 */
static gint lbo_property_on_changed(LBO *lb_obj, const gchar *prop_name, lbo_after_changed_hook hook, gpointer user_data, GDestroyNotify destroy)
{
    g_assert(lb_obj && prop_name);

    const LBProperty *prop = NULL;
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    for (const LBProperty *tmp = intf_properties(core_obj); tmp && tmp->info; tmp++) {
        if (g_strcmp0(tmp->name, prop_name) == 0) {
            prop = tmp;
            break;
        }
    }
    if (!prop) {
        return -1;
    }

    PropListener *pl = g_new0(PropListener, 1);
    pl->prop = prop;
    pl->user_data = user_data;
    pl->hook = hook;
    pl->destroy = destroy;
    lbo_lock(lb_obj);
    lb_list_insert_before(&core_obj->pls, &pl->list_head);
    // 新加入的事件，触发一次属性变更回调
    CorePropertyExt *prop_ext = core_obj->extend + pl->prop->id;
    prop_ext->force_emit_change = 1;
    _property_after_changed(lb_obj, pl->prop);
    lbo_unlock(lb_obj);
    return 0;
}

/**
 * @brief 注册属性变更回调函数
 *
 * @param lb_obj 对象句柄
 * @param prop 属性名
 * @param hook 回调函数
 * @param user_data 用户回调参数
 *
 */
static void lbo_property_on_changed_cancel(LBO *lb_obj, const gchar *prop_name, lbo_after_changed_hook hook, gconstpointer user_data)
{
    g_assert(lb_obj && prop_name);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);

    lbo_lock(lb_obj);

    GDB_LIST_FOREACH(&core_obj->pls, node) {
        PropListener *pl = CONTAINER_OF(node, PropListener, list_head);
        if (pl->hook == hook && pl->user_data == user_data && g_strcmp0(pl->prop->name, prop_name) == 0) {
            if (pl->destroy) {
                pl->destroy(pl->user_data);
            }
            lb_list_remove(node);
            g_free(pl);
            break;
        }
    }
    lbo_unlock(lb_obj);
}

/**
 * @brief 注册对象释放前的回调函数
 *
 * @param lb_obj 对象句柄
 * @param prop 属性名
 * @param hook 回调函数
 * @param user_data 用户回调参数
 *
 */
static void lbo_before_free(LBO *lb_obj, GHookFunc hook, gpointer user_data)
{
    g_assert(lb_obj && hook);
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    ObjectFreeHook *free_hook = g_new0(ObjectFreeHook, 1);
    free_hook->user_data = user_data;
    free_hook->hook = hook;
    lbo_lock(lb_obj);
    lb_list_insert_before(&core_obj->free_ols, &free_hook->list_head);
    lbo_unlock(lb_obj);
}

static gboolean _focus_changed(CoreObject *core_obj, const LBProperty *prop)
{
    if (!core_obj->is_present)
        return FALSE;
    GDB_LIST_FOREACH(&core_obj->pls, node) {
        PropListener *pl = CONTAINER_OF(node, PropListener, list_head);
        if (pl->prop == prop)
            return TRUE;
    }
    for (GSList *node = prop->hooks; node && node->data; node = node->next) {
        LBPropertyHook *hook = (LBPropertyHook *)node->data;
        if (hook->after)
            return TRUE;
    }
    // 如果有属性监听或需要发送事件，则发送属性变更异步事件，在队列中处理
    if (core_obj->is_object_path) {
        if (prop->flags & LB_FLAGS_PROPERTY_PRIVATE)
            return FALSE;
        if (prop->flags & (LB_FLAGS_PROPERTY_EMIT_TRUE | LB_FLAGS_PROPERTY_EMIT_INVALIDATES))
            return TRUE;
    }
    return FALSE;
}

static void _property_after_changed(LBO *lb_obj, const LBProperty *prop)
{
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);

    CorePropertyExt *prop_ext = core_obj->extend + prop->id;
    // 有未处理的BEFORE_CHANGED事件时不再新增事件
    if (prop_ext->type & PROPERTY_BEFORE_CHANGED) {
        return;
    }
    // 有未处理的AFTER_CHANGED事件不再新增事件
    if (prop_ext->type & PROPERTY_AFTER_CHANGED)
        return;
    // 不关注此属性变更事件
    if (!_focus_changed(core_obj, prop))
        return;
    prop_ext->type = PROPERTY_AFTER_CHANGED;
    g_atomic_ref_count_inc(&core_obj->ref_count);
    // 顺序执行，避免乱序
    g_async_queue_push(prop_chg_tp[core_obj->oid % G_N_ELEMENTS(prop_chg_tp)], (gpointer)prop_ext);
}

LBO *lbo_nth(const LBInterface *intf_desc, gint n)
{
    g_assert(intf_desc);

    LBO *lb_obj = NULL;

    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    if (!intf) {
        log_error("Unknown interface %s, not registed", intf_desc->interface ? intf_desc->name : "???");
        return NULL;
    }
    G_LOCK(lock);
    if (n >= 0) {
        // 正向遍历，直到第n个对外

        GDB_LIST_FOREACH(&intf->objects, node) {
            CoreObject *core_obj = CONTAINER_OF(node, CoreObject, list_head);
            if (!core_obj->is_present) {
                continue;
            }
            if (n == 0) {
                g_atomic_ref_count_inc(&core_obj->ref_count);
                lb_obj = core_obj->lb_obj;
                break;
            }
            n--;
        }
    } else {
        // 逆向查找，第一个成员为-1，第二个-2......
        for (LBList *node = intf->objects.prev; node != &intf->objects; node = node->prev) {
            CoreObject *core_obj = CONTAINER_OF(node, CoreObject, list_head);
            if (!core_obj->is_present) {
                continue;
            }
            n++;
            if (n == 0) {
                g_atomic_ref_count_inc(&core_obj->ref_count);
                lb_obj = core_obj->lb_obj;
                break;
            }
        }
    }
    G_UNLOCK(lock);
    return lb_obj;
}

static void lbo_set_bind(LBO *lb_obj, gpointer data, GDestroyNotify destroy)
{
    g_assert(lb_obj);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    lbo_lock(lb_obj);
    if (core_obj->bind_data && core_obj->bind_destroy)
        core_obj->bind_destroy(core_obj->bind_data);
    core_obj->bind_data = data;
    core_obj->bind_destroy = destroy;
    lbo_unlock(lb_obj);
}

static gpointer lbo_get_bind(LBO *lb_obj)
{
    g_assert(lb_obj);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    return core_obj->bind_data;
}

static GSList *lb_objects(const LBInterface *intf_desc)
{
    g_assert(intf_desc);

    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    if (!intf) {
        return NULL;
    }
    GSList *out = NULL;
    G_LOCK(lock);
    for (LBList *node = intf->objects.prev; node != &intf->objects; node = node->prev) {
        CoreObject *core_obj = CONTAINER_OF(node, CoreObject, list_head);
        if (core_obj->is_present) {
            g_atomic_ref_count_inc(&core_obj->ref_count);
            out = g_slist_prepend(out, (gpointer)core_obj->lb_obj);
        }
    }
    G_UNLOCK(lock);
    return out;
}

GSList *lb_interfaces(gboolean is_remote)
{
    GSList *list = NULL;
    GSList *intfs;
    G_LOCK(lock);
    if (is_remote) {
        intfs = remote_intf;
    } else {
        intfs = server_intf;
    }
    for (GSList *item = intfs; item; item = item->next) {
        CoreInterface *intf = (CoreInterface *)item->data;
        list = g_slist_prepend(list, (gpointer)intf->lb_intf);
    }
    G_UNLOCK(lock);
    return list;
}

GDBusConnection *lb_get_bus(void)
{
    if (!g_session_bus.bus)
        return NULL;
    return g_session_bus.bus;
}

/*
 * 信号回调,订阅和取消订阅
 */
static void _lb_signal_callback(GDBusConnection *connection, const gchar *sender_name, const gchar *object_path,
                                 const gchar *interface_name, const gchar *signal_name, GVariant *parameters, gpointer user_data)
{
    dbus_msg_call_and_send();
    log_mass("callback, interface: %s, signal: %s, object:%s", interface_name, signal_name, object_path);
    CoreSignal *sub = (CoreSignal *)user_data;
    cleanup_lbo LBO *lb_obj = NULL;
    const LBInterface *intf_desc = sub->intf->lb_intf;
    cleanup_gfree gchar *well_known = NULL;
    if (sender_name[0] == ':') {
        if (g_strcmp0(sender_name, g_dbus_connection_get_unique_name(g_session_bus.bus)) == 0) {
            sender_name = lb_bus_name();
        } else {
            well_known = lb_well_known(sender_name);
            if (!well_known) {
                log_warn("Get unique name failed, object: %s, bus_name: %s, signal: %s, interface: %s",
                    object_path, sender_name, signal_name, interface_name);
                return;
            }
            sender_name = well_known;
        }
    }
    if (intf_desc->is_remote) {
        lb_obj = lbo_cli_get(intf_desc, sender_name, object_path);
    } else {
        lb_obj = lbo_get(intf_desc, object_path);
    }
    if (!lb_obj) {
        log_warn("Unknown object %s, bus_name: %s, signal: %s, interface: %s, subscribe interface: %s",
            object_path, sender_name, signal_name, interface_name, intf_desc->name);
        return;
    }

    if (g_strcmp0(g_variant_get_type_string(parameters), sub->signal->msg_signature) != 0) {
        log_warn("Receive signal %s with unmatch signature, object %s, bus_name: %s, interface: %s", signal_name, object_path,
                 sender_name, interface_name);
        return;
    }
    void *msg = sub->signal->msg_decode(parameters);
#ifdef LB_VERSION_BE_0_8_2
    LBSignalExtData ext_data = {
        .conn = connection,
        .signal_name = signal_name,
        .sender_name = sender_name,
        .object_path = object_path,
        .interface_name = interface_name,
        .parameter = g_variant_ref(parameters)
    };
    sub->callback(lb_obj, sender_name, msg, sub->user_data, &ext_data);
    g_variant_unref(ext_data.parameter);
#else
    sub->callback(lb_obj, sender_name, msg, sub->user_data);
#endif
    sub->signal->msg_free(&msg);
}

/**
 * @brief 注册消息回调
 *
 * @param intf_desc 接口描述符，只能由代码自动生成，传入用户分配的空间将导致未定义行为
 * @param bus_name 消息发送方总线名，为NULL表示不关心
 * @param signal 消息体
 * @param object_path
 * @param callback
 * @param user_data
 * @return guint
 *
 * @notes intf_desc和signal只能使用代码自动生成的值，传入用户分配的空间将导致未定义行为
 */
static guint lb_subscribe_signal(const LBInterface *intf_desc, const gchar *bus_name, const LBSignal *signal, const gchar *object_path,
                           const gchar *arg0, lbo_signal_handler callback, gpointer user_data)
{
    if (!callback || !intf_desc || !signal) {
        log_error("Parameter error");
        return 0;
    }

    CoreInterface *intf = (CoreInterface *)intf_desc->opaque;
    if (!intf) {
        log_error("Unknown interface %s, subscribe failed", intf_desc->interface ? intf_desc->name : "???");
        return 0;
    }
#ifndef NDEBUG
    /* 仅在调试模式检查signal是否为自动分配的内存 */
    gboolean found = FALSE;
    for (const LBSignal *tmp = intf_desc->signals; tmp && tmp->name; tmp++) {
        if (tmp == signal) {
            found = TRUE;
            break;
        }
    }
    if (!found) {
        log_error("Unkown signal %s get, signal can only be allocated by codegen", signal->name);
        return 0;
    }
#endif
    CoreSignal *sub = g_new0(CoreSignal, 1);
    sub->user_data = user_data;
    sub->intf = intf;
    sub->callback = callback;
    sub->signal = signal;

    log_mass("subscribe, interface: %s, signal: %s, object:%s", intf_desc->name, signal->name, object_path);
    sub->id = g_dbus_connection_signal_subscribe(g_session_bus.bus, bus_name, intf_desc->name, signal->name, object_path, arg0,
                                                 G_DBUS_SIGNAL_FLAGS_NONE, _lb_signal_callback, sub, g_free);
    if (sub->id == 0) {
        log_warn("subscribe signal %s failed, bus: %s, object: %s", signal->name, bus_name, object_path);
    }
    return sub->id;
}

/* 由属性变更回调处理，此处不做任何处理直接返回 */
static void *property_changed_message_decode(GVariant *parameters)
{
    return (void *)parameters;
}
/* decode时未做任何处理，因此无需释放任何内容 */
static void property_changed_message_free(void **req)
{
    return;
}

/* 属性变更事件监听 */
static LBSignal property_changed_signal = {
    .name = "PropertiesChanged",
    .msg_signature = "(sa{sv}as)",
    .msg_decode = property_changed_message_decode,
    .msg_encode = NULL,
    .msg_free = property_changed_message_free,
};

static guint lb_subscribe_property_changed(LBO *lb_obj, lbo_signal_handler callback, LBProperty *prop, gpointer user_data)
{
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    CoreSignal *sub = g_new0(CoreSignal, 1);
    sub->callback = callback;
    sub->user_data = user_data;
    sub->intf = core_obj->intf;
    sub->signal = &property_changed_signal;
    const gchar *arg0 = prop ? prop->name : NULL;
    sub->id =
        g_dbus_connection_signal_subscribe(g_session_bus.bus, core_obj->bus_name, "org.freedesktop.DBus.Properties", "PropertiesChanged",
                                           core_obj->name, arg0, G_DBUS_SIGNAL_FLAGS_NONE, _lb_signal_callback, sub, g_free);
    return sub->id;
}

static void lb_unsubscribe_signal(guint *id)
{
    if (!id || !(*id))
        return;
    g_dbus_connection_signal_unsubscribe(g_session_bus.bus, *id);
    *id = 0;
}

/*
 * 发送信号
 */
static gboolean lbo_emit_signal(LBO *lb_obj, const gchar *destination, const LBSignal *signal, const void *msg, GError **error)
{
    if (lb_obj == NULL || signal == NULL || error == NULL) {
        log_error("Parameter error");
        return FALSE;
    }
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    const LBInterface *lb_intf = lbo_interface(lb_obj);

#ifndef NDEBUG
    /* 仅在调试模式检查signal是否为自动分配的内存 */
    gboolean found = FALSE;
    for (const LBSignal *tmp = lb_intf->signals; tmp && tmp->name; tmp++) {
        if (tmp == signal) {
            found = TRUE;
            break;
        }
    }
    if (!found) {
        *error = LbError_SignalNotExist_new(signal->name);
        return FALSE;
    }
#endif

    if (!g_variant_is_object_path(core_obj->name)) {
        *error = LbError_ObjectNotExported_new(core_obj->name);
        return FALSE;
    }
    GVariant *parameter = signal->msg_encode(msg);
    if (parameter == NULL) {
        *error = LbError_MsgDecodeFailed_new();
        return FALSE;
    }
    g_variant_take_ref(parameter);
    dbus_msg_call_and_send();
    gboolean ret =
        g_dbus_connection_emit_signal(g_session_bus.bus, destination, core_obj->name, lb_intf->name, signal->name, parameter, error);
    g_variant_unref(parameter);
    return ret;
}

static void _on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    g_session_bus.bus = connection;
    g_session_bus.bus_state |= LB_BUS_BUS_ACCQUIRED;
    log_info("Bus acquired the name %s, bus_state: %d", name, g_session_bus.bus_state);
}

static void _on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    g_session_bus.bus_state |= LB_BUS_NAME_ACCQUIRED;
    log_info("Name acquired the name %s, bus_state: %d", name, g_session_bus.bus_state);
}

static void _on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    log_alert("Lost the name %s", name);
}

/*
 * 监听服务
 */
static gpointer _if_dbus_start(gpointer data)
{
    GMainLoop *loop;
    guint id;

    /* object path取类名 */
    while (1) {
        loop = g_main_loop_new(NULL, FALSE);
        id = g_bus_own_name(g_bus_type, g_session_bus.bus_name, G_BUS_NAME_OWNER_FLAGS_NONE, _on_bus_acquired, _on_name_acquired,
                            _on_name_lost, NULL, NULL);
        g_main_loop_run(loop);

        g_bus_unown_name(id);
        g_main_loop_unref(loop);
    }
    return NULL;
}

static void _lb_check_dbus_address(void)
{
#define BUS_ADDR_KEY "DBUS_SESSION_BUS_ADDRESS"
    gchar buf[256] = {0};
    gint ret;
    log_info("Read DBUS_SESSION_BUS_ADDRESS start");
    while (TRUE) {
        if (g_getenv(BUS_ADDR_KEY))
            break;
        usleep(100000);
        cleanup_gfree gchar *dbus_file = get_rootfs_path("/var/run/dbus/session-dbus");
        gint fd = open(dbus_file, O_RDONLY);
        if (fd < 0)
            continue;
        flock(fd, LOCK_EX);
        ret = read(fd, buf, sizeof(buf) - 1);
        flock(fd, LOCK_UN);
        close(fd);
        if (ret <= strlen(BUS_ADDR_KEY) + 1)
            continue;
        for (int i = 0; i < ret; i++) {
            if (buf[i] == '\n') {
                ret = i;
                break;
            }
        }
        buf[ret] = '\0';
        buf[ret - 2] = '\0';
        ret -= 3;
        if (strncmp(buf, BUS_ADDR_KEY, strlen(BUS_ADDR_KEY)))
            continue;
        gchar *addr = buf + strlen(BUS_ADDR_KEY);
        if (addr[0] != '=')
            continue;
        addr++;
        if (addr[0] == '\'')
            addr++;
        if (g_setenv(BUS_ADDR_KEY, addr, TRUE)) {
            printf("Set DBUS_SESSION_BUS_ADDRESS successfully\n");
            break;
        }
    }
    log_info("Read DBUS_SESSION_BUS_ADDRESS successfully");
}

static void _get_dbus_connection(void)
{
    guint i = 0;
    if (g_bus_type == G_BUS_TYPE_SESSION) {
        _lb_check_dbus_address();
    }
    // 创建dbus连接
    g_session_bus.bus_state = LB_BUS_CONNECTING;
    GThread *thread = g_thread_new("dbus_run", _if_dbus_start, NULL);
    g_thread_unref(thread);

    while (g_session_bus.bus_state != (LB_BUS_NAME_ACCQUIRED | LB_BUS_BUS_ACCQUIRED | LB_BUS_CONNECTING)) {
        if (i % 300 == 0)
            log_info("Dbus (bus_name:%s, bus type: %s) connecting, state: %x...", g_session_bus.bus_name,
                (g_bus_type == G_BUS_TYPE_SESSION) ? "session" : "system", g_session_bus.bus_state);
        i++;
        usleep(100000);
    }
    log_info("Dbus (session bus) connected...");
}

const gchar *lb_bus_name(void)
{
    return g_session_bus.bus_name ? g_session_bus.bus_name : "???";
}

const gchar *lb_app_name(void)
{
    static gchar *app_name = NULL;
    if (app_name != NULL) {
        return app_name;
    }
    const gchar *bus_name = lb_bus_name();
    const gchar *pos = strrchr(bus_name, '.');
    if (pos == NULL) {
        return bus_name;
    }
    app_name = g_strdup(pos + 1);
    return app_name;
}

static void _iter_property_values(LBO *lb_obj, GVariantIter *iter)
{
    gchar *key;
    GVariant *val;
    while (g_variant_iter_next(iter, "{&sv}", &key, &val)) {
        const LBProperty *prop = _get_property_desc(lb_obj, key);
        if (!prop) {
            log_warn("Unkown property %s.%s get, interface: %s", lbo_name(lb_obj), key, lbo_interface_name(lb_obj));
        } else {
            prop->set(lb_obj, val);
        }
        g_variant_unref(val);
    }
}

#ifdef LB_VERSION_BE_0_8_2
static void _remote_val_changed(LBO *lb_obj, const gchar *sender_name, GVariant *parameters, gpointer user_data, const LBSignalExtData *ext_data)
#else
static void _remote_val_changed(LBO *lb_obj, const gchar *sender_name, GVariant *parameters, gpointer user_data)
#endif
{
    GVariantIter iter;
    gchar *intf_name = NULL;
    GVariant *changed_properties = NULL;
    gchar **invalidated_properties;

    g_variant_get(parameters, "(&s@a{sv}^a&s)", &intf_name, &changed_properties, &invalidated_properties);
    g_variant_iter_init(&iter, changed_properties);
    _iter_property_values(lb_obj, &iter);
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);

    if (invalidated_properties && invalidated_properties[0]) {
        lbo_lock(lb_obj);
        for (int i = 0; invalidated_properties[i] != NULL; i++) {
            LBProperty *prop = _get_property_desc(lb_obj, invalidated_properties[i]);
            if (prop) {
                CorePropertyExt *prop_ext = core_obj->extend + prop->id;
                prop_ext->flags |= LB_FLAGS_PROPERTY_INVALIDATE;
            }
        }
        lbo_unlock(lb_obj);
    }
    g_free(invalidated_properties);
    g_clear_pointer(&changed_properties, g_variant_unref);
}

static gint lb_get_remote_all_val(LBO *lb_obj)
{
    GVariantIter *iter = NULL;
    GError *error = NULL;
    dbus_msg_call_and_send();
    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    log_info("start get remote value: %s", lbo_name(lb_obj));
    if (!core_obj->is_present) {
        lbo_unref(&lb_obj);
        return 0;
    }
    GVariant *result = g_dbus_connection_call_sync(g_session_bus.bus, core_obj->bus_name, core_obj->name, "org.freedesktop.DBus.Properties",
                                                   "GetAll", g_variant_new("(s)", intf_name(core_obj)), G_VARIANT_TYPE("(a{sv})"),
                                                   G_DBUS_CALL_FLAGS_NONE, 10000, /* timeout */
                                                   NULL, &error);
    if (!result) {
        log_error("Call GetAll method failed, object: %s, bus_name: %s, error: %s", core_obj->name, core_obj->bus_name, error->message);
        g_error_free(error);
    } else {
        g_variant_get(result, "(a{sv})", &iter);
        _iter_property_values(core_obj->lb_obj, iter);
        g_variant_iter_free(iter);
        g_variant_unref(result);
    }
    if (!core_obj->property_changed_subscribe) {
        core_obj->property_changed_subscribe = lb_subscribe_property_changed(lb_obj, (lbo_signal_handler)_remote_val_changed, NULL, NULL);
    }
    lbo_unref(&lb_obj);
    return 0;
}

static gint lbo_set_property(LBO *lb_obj, const LBProperty *prop, GVariant *val, GError **error)
{
    g_assert(lb_obj && prop && val);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    if (!is_remote_object(core_obj)) {
        lbo_set_memory(lb_obj, prop, val);
        return 0;
    }
    // 不可写属性
    if (!(prop->info->flags & G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE)) {
        if (error)
            *error = LbError_PropertyNotWritable_new(core_obj->name, prop->name);
        return -1;
    }
    g_variant_ref(val);
    dbus_msg_call_and_send();
    GVariant *result = g_dbus_connection_call_sync(g_session_bus.bus, core_obj->bus_name, core_obj->name, "org.freedesktop.DBus.Properties",
                                                   "Set", g_variant_new("(ssv)", intf_name(core_obj), prop->name, val), G_VARIANT_TYPE("()"),
                                                   G_DBUS_CALL_FLAGS_NONE, 10000, /* timeout */
                                                   NULL, error);
    if (!result)
        return -1;
    g_variant_unref(result);
    return 0;
}

static gint lbo_get_property(LBO *lb_obj, const LBProperty *prop, GVariant **val, GError **error)
{
    g_assert(lb_obj && prop);

    CoreObject *core_obj = (CoreObject *)lbo_opaque(lb_obj);
    if (!is_remote_object(core_obj)) {
        *val = lbo_get_memory(lb_obj, prop);
        return 0;
    }
    CorePropertyExt *prop_ext = core_obj->extend + prop->id;
    lbo_lock(lb_obj);
    LBPropertyFlags flags = prop_ext->flags;
    prop_ext->flags &= (~LB_FLAGS_PROPERTY_INVALIDATE);
    lbo_unlock(lb_obj);

    // 常量或发送变更事件,或接收到属性invalidate事件
    if (flags & (LB_FLAGS_PROPERTY_EMIT_TRUE | LB_FLAGS_PROPERTY_EMIT_CONST) && (!(flags & LB_FLAGS_PROPERTY_INVALIDATE))) {
        *val = lbo_get_memory(lb_obj, prop);
        return 0;
    }
    // 不可读属性
    if (!(prop->info->flags & G_DBUS_PROPERTY_INFO_FLAGS_READABLE)) {
        if (error)
            *error = LbError_PropertyNotReadable_new(core_obj->name, prop->name);
        return -1;
    }
    dbus_msg_call_and_send();
    GVariant *result = g_dbus_connection_call_sync(g_session_bus.bus, core_obj->bus_name, core_obj->name, "org.freedesktop.DBus.Properties",
                                                   "Get", g_variant_new("(ss)", intf_name(core_obj), prop->name), G_VARIANT_TYPE("(v)"),
                                                   G_DBUS_CALL_FLAGS_NONE, 10000, /* timeout */
                                                   NULL, error);
    if (!result)
        return -1;
    g_variant_get(result, "(v)", val);
    g_variant_unref(result);
    prop->set(lb_obj, *val);
    return 0;
}

static void lb_prop_hook(LBProperty *prop, const LBPropertyHook *hook)
{
    if (prop == NULL || hook == NULL) {
        log_error("Parameter error");
        return;
    }
    if (hook->after == NULL && hook->before == NULL) {
        log_error("Parameter error");
        return;
    }
    G_LOCK(lock);
    for (GSList *item = prop->hooks; item && item->data; item = item->next) {
        LBPropertyHook *tmp  = (LBPropertyHook *)item->data;
        if (tmp->after == hook->after && tmp->before == hook->before && tmp->user_data == hook->user_data) {
            G_UNLOCK(lock);
            return;
        }
    }
    LBPropertyHook *new_hook = g_new0(LBPropertyHook, 1);
    memcpy(new_hook, hook, sizeof(*hook));
    prop->hooks = g_slist_append(prop->hooks, new_hook);
    G_UNLOCK(lock);
}

void lb_set_bus_type(GBusType bus_type)
{
    if (bus_type == G_BUS_TYPE_SESSION || bus_type == G_BUS_TYPE_SYSTEM) {
        g_bus_type = bus_type;
    } else {
        log_error("Set bus_type failed, only G_BUS_TYPE_SESSION and G_BUS_TYPE_SYSTEMD can be acceptted");
    }
}

GBusType lb_get_bus_type(void)
{
    return g_bus_type;
}

void lb_start(const gchar *bus_name)
{
    g_session_bus.bus_name = g_strdup(bus_name);
    if (!g_session_bus.bus_name) {
        log_alert("bus_name is NULL, must call lb_set_bus_name first, abort");
    }
    const gchar *app_name = lb_app_name();
    lb_log_set_service(app_name);
    _get_dbus_connection();
    LBImpl lb_impl = {
        .property_changed = _property_after_changed,
        .emit_signal = lbo_emit_signal,
        .subscribe_signal = lb_subscribe_signal,
        .unsubscribe_signal = lb_unsubscribe_signal,
        .call_method = lbo_call_method,
        .read_property = lbo_get_property,
        .write_property = lbo_set_property,
        ._get = lbo_get,
        ._new = lbo_new,
        ._cli_get = lbo_cli_get,
        ._cli_new = lbo_cli_new,
        ._unref = lbo_unref,
        ._ref = lbo_ref,
        ._present_set = lbo_present_set,
        ._present = lbo_present,
        ._bind = lbo_set_bind,
        ._data = lbo_get_bind,
        ._on_prop_changed = lbo_property_on_changed,
        ._on_prop_changed_cancel = lbo_property_on_changed_cancel,
        ._on_changed = lb_interface_on_changed,
        ._before_destroy = lbo_before_free,
        ._prop_hook = lb_prop_hook,
        ._nth = lbo_nth,
        ._list = lb_objects,
    };
    lb_init(&lb_impl);
}

static void interfaces_start(void)
{
    GSList *intfs = lb_interfaces_origin();
    for (GSList *item = intfs; item && item->data; item = item->next) {
        LBInterface *intf = (LBInterface *)item->data;
        lb_register_interface(intf);
    }
    g_slist_free(intfs);
}

static void __attribute__((constructor(150))) lb_register(void)
{
#ifndef NDEBUG
    lb_list_init(&g_objects);
#endif
    // plugin加载之后启动，优先级设置为2
    lb_module_register(interfaces_start, "gdb", 2);
}

#ifndef NDEBUG
void lb_print_all_objects(void)
{
    G_LOCK(lock);
    GDB_LIST_FOREACH(&g_objects, node) {
        CoreObject *core_obj = CONTAINER_OF(node, CoreObject, object_list);
        log_info("object %s exist, intf: %s, present: %d, ref_count: %d\n", core_obj->name,
            core_obj->intf->lb_intf->name, core_obj->is_present, core_obj->ref_count);
    }
    G_UNLOCK(lock);
}
#endif
#ifdef BUILD_TEST
gboolean lb_wait_internal_queue_empty(guint32 timeout_sec) {
# define __DBUS_EMPTY_TIMEOUT__ 50
    gboolean finished = FALSE;
    guint32 timeout_ms = timeout_sec * 1000;
    // 连续10ms未收到dbus消息时认为队列为空
    guint dbus_empty_timeout = __DBUS_EMPTY_TIMEOUT__;
    gint last_recved_cnt = 0;
    // 队列为空回调可能回调正在处理，所以需要向队列插入NULL确保所有有效事件都已处理完成
    for (int i = 0; i < G_N_ELEMENTS(prop_chg_tp); i++) {
        g_async_queue_push(prop_chg_tp[i], INVALID_QUEUE_MSG);
    }
    for (int i = 0; i < G_N_ELEMENTS(objt_chg_tp); i++) {
        g_async_queue_push(objt_chg_tp[i], INVALID_QUEUE_MSG);
    }
    while (timeout_ms > 0 && dbus_empty_timeout > 0) {
        finished = TRUE;
        for (int i = 0; finished == TRUE && i < G_N_ELEMENTS(prop_chg_tp); i++) {
            if (g_async_queue_length(prop_chg_tp[i]) > -1) {
                finished = FALSE;
            }
        }
        for (int i = 0; finished == TRUE && i < G_N_ELEMENTS(objt_chg_tp); i++) {
            if (g_async_queue_length(objt_chg_tp[i]) > -1) {
                finished = FALSE;
            }
        }
        // 发生变更时重新计数
        if (finished == FALSE || last_recved_cnt != dbus_msg_cnt()) {
            last_recved_cnt = dbus_msg_cnt();
            dbus_empty_timeout = __DBUS_EMPTY_TIMEOUT__;
        } else {
            dbus_empty_timeout--;
        }
        timeout_ms--;
        usleep(1000);
    }
    return last_recved_cnt == 0 ? TRUE : FALSE;
}
#endif