#include "lb_core.h"

static int _check_key(const gchar *key)
{
    while (*key != '\0') {
        if (isalnum(*key)) {
            key++;
            continue;
        }
        if (*key == '_' || *key == '/' || *key == '-' || *key == '.') {
            key++;
            continue;
        }
        log_warn("Check key failed, key: %s", key);
        return -1;
    }
    return 0;
}

/*
 * Notes: 单线程操作，多线程不安全
 */
void persist_save_property(const gchar *object, const gchar *prop, GVariant *value, PER_TYPE per_type)
{
    g_assert(object && prop);
    g_assert(per_type == PER_SAVE || per_type == PER_POWER_OFF || per_type == PER_REBOOT);
    if (!object || !prop || !(per_type == PER_SAVE || per_type == PER_POWER_OFF || per_type == PER_REBOOT)) {
        log_error("Parameter error");
        return;
    }

    if (_check_key(object) || _check_key(prop)) {
        return;
    }

    guchar buffer[4096];
    guchar *new_buffer = NULL;
    PER_ITEM item = {NULL};
    item.group = object;
    item.key = prop;
    // 非持久化
    if (value) {
        const gchar *type = g_variant_get_type_string(value);
        const gchar *data = g_variant_get_data(value);
        gsize type_len = strlen(type) + 1;
        gsize data_len = (gsize)g_variant_get_size(value);
        item.length = type_len + data_len;
        if (item.length <= sizeof(buffer)) {
            item.data = buffer;
        } else {
            item.data = (guchar *)g_malloc(item.length);
            new_buffer = item.data;
        }
        memcpy(item.data, type, type_len);
        memcpy(item.data + type_len, data, data_len);
    }
    item.type = per_type;
    per_save(&item);
    if (new_buffer) {
        g_free(new_buffer);
    }
}

/*
 * Notes: 从数据库中加载
 */
gint persist_load_property(const gchar *object, const gchar *prop, GVariant **value, PER_TYPE per_type)
{
    gint ret = -1;

    if (!object || !prop || !value) {
        log_error("Parameter error");
        return -1;
    }

    if (_check_key(object) || _check_key(prop))
        return -1;

    PER_ITEM item = {NULL};
    item.group = object;
    item.key = prop;
    item.type = per_type;
    if (per_load(&item) == 0 && item.data) {
        gsize i = 0;
        for (; i < item.length; i++) {
            if (item.data[i] == '\0') {
                break;
            }
        }
        // 是有效的variant类型字符串
        if (i < item.length && g_variant_type_string_is_valid((const gchar *)item.data)) {
            gchar *data = (gchar *)item.data + strlen((gchar *)item.data) + 1;
            gsize length = item.length - strlen((gchar *)item.data) - 1;
            *value = g_variant_new_from_data((const GVariantType *)item.data, data, length, TRUE, g_free, item.data);
            item.data = NULL;
            if (*value) {
                ret = 0;
            }
        }
    }
    if (item.data) {
        g_free(item.data);
    }

    return ret;
}

#ifndef SHA256_DIGEST_LEN
#define SHA256_DIGEST_LEN 32
#endif
typedef struct {
    gchar *data_file;
    gchar *back_file;
    gchar *temp_file;
    GKeyFile *key_file;
    GMutex lock;
    struct {
        guint32 is_changed: 1;
    };
    guint8 digest[SHA256_DIGEST_LEN];
} PER_DATA;

static PER_DATA glb_per_power_off = {NULL};
static PER_DATA glb_per_reboot = {NULL};

static inline PER_DATA *get_per_data(guint32 per_type)
{
    if (per_type == PER_POWER_OFF) {
        return &glb_per_power_off;
    } else {
        return &glb_per_reboot;
    }
}

static void sha256_sum(gchar *buffer, gsize length, guint8 *digest)
{
    GChecksum *checksum;
    gsize digest_len = 32;
    checksum = g_checksum_new(G_CHECKSUM_SHA256);
    // 跳过文件头部的sha256值
    g_checksum_update(checksum, (const guchar *)buffer, length);
    g_checksum_get_digest(checksum, digest, &digest_len);
    g_checksum_free(checksum);
}

static gint read_file(gint max_len, const gchar *file_name, gchar **buf)
{
    int fd = open(file_name, O_RDONLY);
    if (fd < 0) {
        return -1;
    }
    off_t pos = lseek(fd, 0, SEEK_END);
    if (pos <= 0 || pos > max_len) {
        close(fd);
        return 0;
    }
    *buf = g_malloc0(pos);
    lseek(fd, 0, SEEK_SET);
    ssize_t length = read(fd, *buf, pos);
    close(fd);
    if (pos != length) {
        g_free(*buf);
        *buf = NULL;
        return 0;
    }
    return length;
}

static gint persist_data_recovery(PER_DATA *per, const gchar *file_name, gboolean ignore_load_failed)
{
    cleanup_error GError *error = NULL;
    gboolean ok = FALSE;
    cleanup_gfree char *buffer = NULL;
    gint length = 0;

    gint ret = access(file_name, F_OK);
    if (ret != 0) {
        log_debug("File %s not exist.", file_name);
        return -1;
    }
    // 从持久化文件中恢复数据
    length = read_file(0x100000, file_name, &buffer);
    if (length <= 0) {
        if (ignore_load_failed == TRUE) {
            return -1;
        }
        log_notice("Load persistent %s failed.", file_name);
        return -1;
    }
    // 持久化内容的第一行为sha256值
    if (length <= SHA256_DIGEST_LEN) {
        log_error("Load persistent %s failed because data is broken", file_name);
        return -1;
    }

    sha256_sum(buffer + SHA256_DIGEST_LEN + 1, length - SHA256_DIGEST_LEN - 1, per->digest);
    if (memcmp(per->digest, buffer, SHA256_DIGEST_LEN) != 0) {
        log_error("Load persistent %s failed because checksum not match", file_name);
        return -1;
    }
    // 加载keyfile内容
    ok = g_key_file_load_from_data(per->key_file, buffer + SHA256_DIGEST_LEN + 1, length - SHA256_DIGEST_LEN - 1,
                                   G_KEY_FILE_NONE, &error);
    if (ok != TRUE) {
        log_error("Load persistent %s failed, error: %s", file_name, error->message);
        return -1;
    }
    return 0;
}

static void persist_data_init(PER_DATA *per, const gchar *basedir)
{
    gint ret = 0;

    g_mutex_init(&per->lock);
    per->key_file = g_key_file_new();
    // 构建持久化目录并创建目录
    cleanup_gfree gchar *tmp_basedir = get_rootfs_path(basedir);
    cleanup_gfree gchar *dir_name = lb_printf("%s/%s/", tmp_basedir, lb_bus_name());
    ret = mkdir(dir_name, S_IRWXU);
    if (ret != 0 && errno != EEXIST) {
        log_alert("call mkdir(%s) failed, error: %s", dir_name, strerror(errno));
    }
    (void)chmod(dir_name, S_IRWXU);

    // 创建持久化文件名
    per->data_file = lb_printf("%s%s", dir_name, "data.ini");
    per->back_file = lb_printf("%s%s", dir_name, "back.ini");
    per->temp_file = lb_printf("%s%s", dir_name, "temp.ini");

    ret = persist_data_recovery(per, per->temp_file, TRUE);
    if (ret != 0) {
        ret = persist_data_recovery(per, per->data_file, FALSE);
        if (ret != 0) {
            ret = persist_data_recovery(per, per->back_file, FALSE);
        }
    }
    if (ret != 0) {
        log_error("load persistence failed");
    }
}

// 恢复持久化记录
gint per_load(PER_ITEM *item)
{
    cleanup_error GError *error = NULL;

    if (item == NULL) {
        log_error("Parameter error, item is NULL");
        return -1;
    }
    if (item->group == NULL) {
        item->group = "_";
    }
    PER_DATA *per = get_per_data(item->type);

    cleanup_gfree gchar *base64_data = g_key_file_get_string(per->key_file, item->group, item->key, &error);
    if (error != NULL) {
        if (g_error_matches(error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_KEY_NOT_FOUND) ||
            g_error_matches(error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND)) {
            log_mass("Load persist data %s.%s failed, error: %s", item->group, item->key, error->message);
            return 0;
        }
        log_info("Load persist data %s.%s failed, error: %s", item->group, item->key, error->message);
        return -1;
    }

    gsize origin_len = 0;
    guchar *origin_data = g_base64_decode(base64_data, &origin_len);
    if (origin_data == NULL) {
        return -1;
    }
    if (item->length >= origin_len && item->data) {
        memcpy(item->data, origin_data, origin_len);
        g_free(origin_data);
        item->length = origin_len;
        return 0;
    }
    if (item->is_static == 0 && item->data) {
        g_free(item->data);
    }
    item->data = origin_data;
    item->length = origin_len;
    item->is_static = 0;
    return 0;
}

static gint per_file_write(const gchar *file_name, const guint8 *digest, gsize digest_len, const gchar *buffer, gsize length)
{
    cleanup_fclose FILE *fp = fopen(file_name, "w+");
    if (fp == NULL) {
        log_error("Create persist file %s failed, error: %s.", file_name, strerror(errno));
        return -1;
    }
    fchmod(fileno(fp), S_IRUSR | S_IWUSR);
    size_t ret = fwrite(digest, digest_len, 1, fp);
    ret += fwrite("\n", 1, 1, fp);
    ret += fwrite(buffer, length, 1, fp);
    // 调用3次fwrite函数写入持久化数据，每次返回值都是1，结果不是3表示有错误
    if (ret != 3) {
        return -1;
    }
    fflush(fp);
    return 0;
}

// 保存持久化数据
gint per_save(PER_ITEM *item)
{
    cleanup_error GError *error = NULL;
    guint8 digest[SHA256_DIGEST_LEN] = {0};
    gint result = -1;

    if (item == NULL) {
        log_error("Parameter error, item is NULL");
        return result;
    }
    PER_DATA *per = get_per_data(item->type);

    if (item->length != 0 && item->data) {
        cleanup_gfree gchar *base64_data = g_base64_encode(item->data, item->length);
        g_key_file_set_string(per->key_file, item->group, item->key, base64_data);
    } else {
        gboolean ok = g_key_file_remove_key(per->key_file, item->group, item->key, &error);
        if (ok != TRUE) {
            log_debug("Remove key %s.%s failed, error: %s", item->group, item->key, error->message);
            return result;
        }
    }
    gsize length = 0;
    cleanup_gfree gchar *buffer = g_key_file_to_data(per->key_file, &length, &error);
    sha256_sum(buffer, length, digest);
    g_mutex_lock(&per->lock);
    do {
        // 如果数据未变更，不写入文件
        if (memcmp(digest, per->digest, sizeof(digest)) == 0) {
            result = 0;
            break;
        }
        // 先写临时文件
        gint ret = per_file_write(per->temp_file, digest, sizeof(digest), buffer, length);
        if (ret == 0) {
            // 再写数据文件
            ret = per_file_write(per->data_file, digest, sizeof(digest), buffer, length);
        }
        // 只有两个文件都成功才算写成功
        if (ret == 0) {
            memcpy(per->digest, digest, sizeof(per->digest));
            log_debug("Write persist data %s.%s success, length: %u", item->group, item->key,
                      sizeof(digest) + 1 + length);
        } else {
            log_error("Save persist data %s.%s failed, error: %s", item->group, item->key, strerror(errno));
            break;
        }
        result = 0;
    } while (0);
    g_mutex_unlock(&per->lock);
    return result;
}

static void _persist_new_start(void)
{
    persist_data_init(&glb_per_power_off, "/opt/data/persistence/power_off/");
    persist_data_init(&glb_per_reboot, "/opt/data/persistence/reboot/");
}

static void __attribute__((constructor(200))) _persist_init(void)
{
    log_info("Init persistence");
    // 优先启动持久化模块，优先级设置为0
    lb_module_register(_persist_new_start, "persist_new", 0);
}
