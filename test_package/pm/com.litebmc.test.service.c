#include "lb_core.h"
#include "lb_internal.h"
#include "variant_helper.h"
#include "com.litebmc.test.service.h"
#include "public/com.litebmc.Test.h"
#include "client/com.litebmc.Test.h"
#include "server/com.litebmc.Test.h"
#include "server/com.litebmc.Connector.h"
#include "client/org.freedesktop.DBus.h"
#include "client/org.freedesktop.DBus.ObjectManager.h"
#include "server/org.freedesktop.DBus.ObjectManager.h"

static gboolean started = FALSE;
#ifndef TEST_LOOP_CNT
#define TEST_LOOP_CNT 100
#endif
#define OBJ_NAME1 "/com/litebmc/test/tc1"
#define OBJ_NAME2 "/com/litebmc/test/tc2"
#define OBJ_NAME3 "/com/litebmc/test/tc3"
#define LB_BUS_NAME "com.litebmc.Test"

static void test_start(void)
{
    started = TRUE;
    log_info("Start test\n");
}

static void __attribute__((constructor(200))) test_init(void)
{
    log_info("Init test");
    lb_module_register(test_start, "test", 9);
}

void test_start_called(void)
{
    g_assert_cmpint(started, ==, TRUE);
}

void test_module_and_domain(void)
{
    g_assert_cmpstr(lb_bus_name(), ==, LB_BUS_NAME);
}

static void new_test_object(void)
{
    static int init = 0;
    gboolean exist = FALSE;
    Test handler1 = Test_new(OBJ_NAME1, &exist);
    g_assert_cmpint(GPOINTER_TO_UINT(handler1), !=, 0);
    if (init == 0)
        g_assert_cmpint(exist, ==, FALSE);
    else
        g_assert_cmpint(exist, ==, TRUE);
    Test_present_set(handler1, TRUE);

    exist = FALSE;
    Test handler2 = Test_new(OBJ_NAME2, &exist);
    g_assert_cmpint(GPOINTER_TO_UINT(handler2), !=, 0);
    if (init == 0)
        g_assert_cmpint(exist, ==, FALSE);
    else
        g_assert_cmpint(exist, ==, TRUE);
    Test_present_set(handler2, TRUE);
    // 测试递归锁
    lbo_lock(handler1);
    lbo_lock(handler1);
    lbo_lock(handler2);
    lbo_lock(handler2);
    lbo_unlock(handler2);
    lbo_unlock(handler2);
    lbo_unlock(handler1);
    lbo_unlock(handler1);
    init = 1;
    LBO *handler3 = lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME1);
    g_assert_nonnull(handler3);
    LBO *handler4 = lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME2);
    g_assert_nonnull(handler4);
}

void test_present(void)
{
    cleanup_Test Test handler = Test_get(OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    Test_present_set(handler, TRUE);
    g_assert_cmpint(Test_present(handler), ==, TRUE);
    Test_present_set(handler, FALSE);
    g_assert_cmpint(Test_present(handler), ==, FALSE);
    g_assert_cmpint(GPOINTER_TO_UINT(Test_get(OBJ_NAME1)), ==, 0);
    Test_present_set(handler, TRUE);
    g_assert_cmpint(Test_present(handler), ==, TRUE);
    cleanup_Test Test handler1 = Test_get(OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler1), !=, 0);
}

static void test_object_get(void)
{
    cleanup_Test Test handler1 = Test_get(OBJ_NAME1);
    cleanup_Test Test handler2 = Test_get(OBJ_NAME2);
    g_assert_cmpint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler2), !=, 0);

    cleanup_Test_list GSList *objects = Test_list();
    g_assert_cmpint(g_slist_length(objects), ==, 2);
    g_assert_cmpint(GPOINTER_TO_UINT(handler1), ==, GPOINTER_TO_UINT(objects->data));
    g_assert_cmpint(GPOINTER_TO_UINT(handler2), ==, GPOINTER_TO_UINT(objects->next->data));
}

#define ghandle gint32
#define g_variant_new_uint8 g_variant_new_byte
#define test_case_set_arr(type, var, rand_max, assert_macro)                                                                               \
    do {                                                                                                                                   \
        gint cnt = rand() % 128 + 1;                                                                                                       \
        g##type *arr = g_malloc0(sizeof(g##type) * cnt);                                                                                   \
        for (int i = 0; i < cnt; i++)                                                                                                      \
            arr[i] = (g##type)(rand() % rand_max);                                                                                         \
        Test_set_##var(handler, arr[0]);                                                                                                   \
        assert_macro(handler->var, ==, arr[0]);                                                                                            \
        Test_set_a##var(handler, cnt, arr);                                                                                                \
        for (int i = 0; i < cnt; i++)                                                                                                      \
            assert_macro(handler->a##var[i], ==, arr[i]);                                                                                  \
        g_free(arr);                                                                                                                       \
    } while (0)

#define g_assert_cmpbool(a, op, b)                                                                                                         \
    do {                                                                                                                                   \
        gint aa = (a) ? TRUE : FALSE;                                                                                                      \
        gint bb = (b) ? TRUE : FALSE;                                                                                                      \
        if (!((a)op(b)))                                                                                                                   \
            log_error("%d != %d", a, b);                                                                                                   \
        g_assert_cmpint(aa, op, bb);                                                                                                       \
    } while (0)

#define test_case_set_remote_arr(type, var, operator)                                                                                      \
    do {                                                                                                                                   \
        GError *error = NULL;                                                                                                              \
        gint cnt = rand() % 128 + 1;                                                                                                       \
        g##type *arr = g_malloc0(sizeof(g##type) * cnt);                                                                                   \
        for (int i = 0; i < cnt; i++)                                                                                                      \
            arr[i] = (g##type)rand();                                                                                                      \
        gint ret = Test_Cli_set_##var(cli_handler, arr[0], &error);                                                                        \
        g_assert_cmpint(ret, operator, 0);                                                                                                 \
        g_assert_cmpint(GPOINTER_TO_UINT(error), operator, 0);                                                                             \
        if (error) {                                                                                                                       \
            g_error_free(error);                                                                                                           \
        }                                                                                                                                  \
        ret = Test_Cli_set_a##var(cli_handler, cnt, arr, &error);                                                                          \
        g_assert_cmpint(ret, operator, 0);                                                                                                 \
        g_assert_cmpint(GPOINTER_TO_UINT(error), operator, 0);                                                                             \
        if (error) {                                                                                                                       \
            g_error_free(error);                                                                                                           \
        }                                                                                                                                  \
        g_free(arr);                                                                                                                       \
    } while (0)

#define test_case_cmp_arr(var, assert_macro)                                                                                               \
    do {                                                                                                                                   \
        assert_macro(handler->var, ==, cli_handler->var);                                                                                  \
        for (int i = 0; i < handler->n_a##var; i++)                                                                                        \
            assert_macro(handler->a##var[i], ==, cli_handler->a##var[i]);                                                                  \
    } while (0)

#define test_case_cmp_arr_str(var)                                                                                                         \
    do {                                                                                                                                   \
        g_assert_cmpstr(handler->var, ==, cli_handler->var);                                                                               \
        g_assert_cmpint(g_strv_length(handler->a##var), ==, g_strv_length(cli_handler->a##var));                                           \
        for (int i = 0; handler->a##var[i]; i++)                                                                                           \
            g_assert_cmpstr(handler->a##var[i], ==, cli_handler->a##var[i]);                                                               \
    } while (0)

static void test_object_set_val(void)
{
#define TEST_OBJ_BASE "/com/litebmc/Ao_"
    Test handler = Test_get(OBJ_NAME1);
    Test_Cli cli_handler = (Test_Cli )lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(cli_handler), !=, 0);
    g_assert_cmpint(handler->b, ==, FALSE);
    g_assert_cmpint(handler->n_ab, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ab), ==, 0);
    g_assert_cmpint(handler->i, ==, 0);
    g_assert_cmpint(handler->n_ai, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ai), ==, 0);
    g_assert_cmpint(handler->u, ==, 0);
    g_assert_cmpint(handler->n_au, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->au), ==, 0);
    g_assert_cmpint(handler->x, ==, 0);
    g_assert_cmpint(handler->n_ax, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ax), ==, 0);
    g_assert_cmpint(handler->t, ==, 0);
    g_assert_cmpint(handler->n_at, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->at), ==, 0);
    g_assert_cmpfloat(handler->d, ==, 0);
    g_assert_cmpint(handler->n_ad, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->at), ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->s), ==, 0);
    g_assert_cmpint(handler->n_ay, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ay), ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->o), ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->g), ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ag), ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler->ao), ==, 0);
    for (int cnt = 0; cnt < TEST_LOOP_CNT; cnt++) {
        test_case_set_arr(boolean, b, 2, g_assert_cmpbool);
        test_case_set_arr(boolean, wb, 2, g_assert_cmpbool);
        test_case_set_arr(uint8, y, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(int16, n, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint16, q, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(int32, i, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint32, u, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(int64, x, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint64, t, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(double, d, RAND_MAX, g_assert_cmpfloat);

        gchar **strv = g_malloc0(sizeof(gchar *) * (cnt + 1));
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 2;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        if (cnt > 0) {
            Test_set_s(handler, strv[0]);
        }
        g_assert_cmpstr(handler->s, ==, strv[0]);
        Test_set_as(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->as[i], ==, strv[i]);
        }
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            memcpy(strv[i], TEST_OBJ_BASE, strlen(TEST_OBJ_BASE));
            for (int j = strlen(TEST_OBJ_BASE); j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        if (cnt > 0) {
            Test_set_o(handler, strv[0]);
        }
        g_assert_cmpstr(handler->o, ==, strv[0]);
        Test_set_ao(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ao[i], ==, strv[i]);
        }
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        gchar *basic_type = "bynqihutxdsog";
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = basic_type[rand() % strlen(basic_type)];
            }
        }
        if (cnt > 0) {
            Test_set_g(handler, strv[0]);
        }
        g_assert_cmpstr(handler->g, ==, strv[0]);
        Test_set_ag(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ag[i], ==, strv[i]);
        }
        g_strfreev(strv);
    }
    lb_wait_internal_queue_empty(1);
    lbo_lock(cli_handler);
    test_case_cmp_arr(wb, g_assert_cmpbool);
    test_case_cmp_arr(y, g_assert_cmpint);
    test_case_cmp_arr(n, g_assert_cmpint);
    test_case_cmp_arr(q, g_assert_cmpuint);
    test_case_cmp_arr(i, g_assert_cmpint);
    test_case_cmp_arr(u, g_assert_cmpuint);
    test_case_cmp_arr(x, g_assert_cmpint);
    test_case_cmp_arr(t, g_assert_cmpuint);
    test_case_cmp_arr(d, g_assert_cmpfloat);
    test_case_cmp_arr_str(s);
    test_case_cmp_arr_str(g);
    test_case_cmp_arr_str(o);
    Test_Cli_unlock(cli_handler);
    Test_unref(&handler);
    Test_Cli_unref(&cli_handler);
}

static gint _before_prop_changed(Test handler, const LBProperty *desc, GVariant *value, gpointer user_data, GError **error)
{
    gint *cnt = (gint *)user_data;
    (*cnt) += 1;
    return 0;
}

static gint _before_prop_changed_failed(Test handler, const LBProperty *desc, GVariant *value, gpointer user_data, GError **error)
{
    log_warn("return failed");
    return -1;
}

static void test_object_set_remote_val(void)
{
#define TEST_OBJ_BASE "/com/litebmc/Ao_"
    GError *error = NULL;
    gint ret;
    Test handler = Test_get(OBJ_NAME1);
    Test_Cli cli_handler = (Test_Cli )lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(cli_handler), !=, 0);
    // 测试远程对象在位状态设置和查询
    lbo_present_set(cli_handler, FALSE);
    lbo_present_set(cli_handler, TRUE);
    gboolean present = lbo_present(cli_handler);
    g_assert_cmpint(present, ==, TRUE);
    static int wb_cnt = 0;
    static int awb_cnt = 0;
    static int y_cnt = 0;
    static int ay_cnt = 0;
    static int n_cnt = 0;
    static int an_cnt = 0;
    static int q_cnt = 0;
    static int aq_cnt = 0;
    static int i_cnt = 0;
    static int ai_cnt = 0;
    static int u_cnt = 0;
    static int au_cnt = 0;
    static int x_cnt = 0;
    static int ax_cnt = 0;
    static int t_cnt = 0;
    static int at_cnt = 0;
    static int d_cnt = 0;
    static int ad_cnt = 0;
    static int s_cnt = 0;
    static int as_cnt = 0;
    static int o_cnt = 0;
    static int ao_cnt = 0;
    static int g_cnt = 0;
    static int ag_cnt = 0;
    gpointer user_data = (gpointer)&wb_cnt;
    Test_wb_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&awb_cnt;
    Test_awb_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&y_cnt;
    Test_y_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ay_cnt;
    Test_ay_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&n_cnt;
    Test_n_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&an_cnt;
    Test_an_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&q_cnt;
    Test_q_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&aq_cnt;
    Test_aq_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&i_cnt;
    Test_i_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ai_cnt;
    Test_ai_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&u_cnt;
    Test_u_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&au_cnt;
    Test_au_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&x_cnt;
    Test_x_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ax_cnt;
    Test_ax_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&t_cnt;
    Test_t_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&at_cnt;
    Test_at_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&d_cnt;
    Test_d_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ad_cnt;
    Test_ad_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&s_cnt;
    Test_s_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&as_cnt;
    Test_as_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&o_cnt;
    Test_o_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ao_cnt;
    Test_ao_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&g_cnt;
    Test_g_hook(_before_prop_changed, NULL, user_data);
    user_data = (gpointer)&ag_cnt;
    Test_ag_hook(_before_prop_changed, NULL, user_data);
    for (int cnt = 0; cnt < TEST_LOOP_CNT; cnt++) {
        test_case_set_remote_arr(boolean, wb, ==);
        test_case_set_remote_arr(uint8, y, ==);
        test_case_set_remote_arr(int16, n, ==);
        test_case_set_remote_arr(uint16, q, ==);
        test_case_set_remote_arr(int32, i, ==);
        test_case_set_remote_arr(uint32, u, ==);
        test_case_set_remote_arr(int64, x, ==);
        test_case_set_remote_arr(uint64, t, ==);
        test_case_set_remote_arr(double, d, ==);

        gchar **strv = g_malloc0(sizeof(gchar *) * (cnt + 1));
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 2;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        if (cnt > 0) {
            Test_Cli_set_s(cli_handler, strv[0], &error);
            g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        }
        Test_Cli_set_as(cli_handler, strv, &error);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            memcpy(strv[i], TEST_OBJ_BASE, strlen(TEST_OBJ_BASE));
            for (int j = strlen(TEST_OBJ_BASE); j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        if (cnt > 0) {
            Test_Cli_set_o(cli_handler, strv[0], &error);
            g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        }
        Test_Cli_set_ao(cli_handler, strv, &error);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        gchar *basic_type = "bynqihutxdsog";
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = basic_type[rand() % strlen(basic_type)];
            }
        }
        if (cnt > 0) {
            Test_Cli_set_g(cli_handler, strv[0], &error);
            g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        }
        Test_Cli_set_ag(cli_handler, strv, &error);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
        g_strfreev(strv);
    }
    lb_wait_internal_queue_empty(1);
    lbo_lock(cli_handler);
    test_case_cmp_arr(wb, g_assert_cmpbool);
    test_case_cmp_arr(y, g_assert_cmpint);
    test_case_cmp_arr(n, g_assert_cmpint);
    test_case_cmp_arr(q, g_assert_cmpuint);
    test_case_cmp_arr(i, g_assert_cmpint);
    test_case_cmp_arr(u, g_assert_cmpuint);
    test_case_cmp_arr(x, g_assert_cmpint);
    test_case_cmp_arr(t, g_assert_cmpuint);
    test_case_cmp_arr(d, g_assert_cmpfloat);
    test_case_cmp_arr_str(s);
    test_case_cmp_arr_str(g);
    test_case_cmp_arr_str(o);
    lbo_unlock(cli_handler);
    g_assert_cmpint(wb_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(awb_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(y_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(ay_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(n_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(an_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(q_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(aq_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(i_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(ai_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(u_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(au_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(x_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(ax_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(t_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(at_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(d_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(ad_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(s_cnt, ==, TEST_LOOP_CNT - 1);
    g_assert_cmpint(as_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(o_cnt, ==, TEST_LOOP_CNT - 1);
    g_assert_cmpint(ao_cnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(g_cnt, ==, TEST_LOOP_CNT - 1);
    g_assert_cmpint(ag_cnt, ==, TEST_LOOP_CNT);

    // 测试before_changed返回失败场景
    user_data = NULL;
    Test_wb_hook(_before_prop_changed_failed, NULL, user_data);
    Test_awb_hook(_before_prop_changed_failed, NULL, user_data);
    error = NULL;
    ret = Test_Cli_set_wb(cli_handler, TRUE, &error);
    g_assert_cmpint(ret, ==, -1);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
    g_error_free(error);
    error = NULL;
    gboolean vals[2] = {TRUE, FALSE};
    ret = Test_Cli_set_awb(cli_handler, 2, vals, &error);
    g_assert_cmpint(ret, ==, -1);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
    g_error_free(error);
    Test_unref(&handler);
    Test_Cli_unref(&cli_handler);
}
static void assert_get_remote_value(Test_Cli cli, const gchar *name, gboolean expect_ok)
{
    const LBProperty *desc;
    GVariant *remote_value = NULL;
    GVariant *get_value = NULL;
    GError *error = NULL;
    gint ret;
    desc = lbo_get_property_desc(cli, name);
    g_assert_cmpuint(GPOINTER_TO_UINT(desc), !=, 0);

    get_value = lbo_get_memory(cli, desc);
    g_assert_cmpuint(GPOINTER_TO_UINT(get_value), !=, 0);

    ret = lb_impl.read_property(cli, desc, &remote_value, &error);
    if (expect_ok == FALSE) {
        g_assert_cmpint(ret, !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(remote_value), ==, 0);
        g_variant_unref(get_value);
        g_error_free(error);
        return;
    }
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(remote_value), !=, 0);

    ret = g_variant_equal(get_value, remote_value);

    g_assert_cmpint(ret, ==, TRUE);
    g_variant_unref(remote_value);
    g_variant_unref(get_value);
}

static void assert_get_invalidate_remote_value(Test_Cli cli, Test local, const gchar *name)
{
    const LBProperty *cli_desc;
    const LBProperty *local_desc;
    GVariant *remote_value = NULL;
    GVariant *get_value = NULL;
    GError *error = NULL;
    gint ret;
    cli_desc = lbo_get_property_desc(cli, name);
    g_assert_cmpuint(GPOINTER_TO_UINT(cli_desc), !=, 0);
    local_desc = lbo_get_property_desc(local, name);
    g_assert_cmpuint(GPOINTER_TO_UINT(local_desc), !=, 0);
    get_value = lbo_get_memory(local, local_desc);
    g_assert_cmpuint(GPOINTER_TO_UINT(get_value), !=, 0);

    ret = lb_impl.read_property(cli, cli_desc, &remote_value, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(remote_value), !=, 0);

    ret = g_variant_equal(get_value, remote_value);

    g_assert_cmpint(ret, ==, TRUE);
    g_variant_unref(remote_value);
    g_variant_unref(get_value);
}

static void test_object_get_remote_val(void)
{
#define TEST_OBJ_BASE "/com/litebmc/Ao_"
    Test handler = Test_get(OBJ_NAME1);
    Test_Cli cli_handler = (Test_Cli )lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(cli_handler), !=, 0);
    for (int cnt = 1; cnt < TEST_LOOP_CNT; cnt++) {
        test_case_set_arr(boolean, b, 2, g_assert_cmpbool);
        test_case_set_arr(boolean, wb, 2, g_assert_cmpbool);
        test_case_set_arr(uint8, y, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(int16, n, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint16, q, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(int32, i, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint32, u, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(int64, x, RAND_MAX, g_assert_cmpint);
        test_case_set_arr(uint64, t, RAND_MAX, g_assert_cmpuint);
        test_case_set_arr(double, d, RAND_MAX, g_assert_cmpfloat);

        gchar **strv = g_malloc0(sizeof(gchar *) * (cnt + 1));
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 2;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        Test_set_s(handler, strv[0]);
        g_assert_cmpstr(handler->s, ==, strv[0]);
        Test_set_as(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->as[i], ==, strv[i]);
        }
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            memcpy(strv[i], TEST_OBJ_BASE, strlen(TEST_OBJ_BASE));
            for (int j = strlen(TEST_OBJ_BASE); j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        Test_set_o(handler, strv[0]);
        g_assert_cmpstr(handler->o, ==, strv[0]);
        Test_set_ao(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ao[i], ==, strv[i]);
        }
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        gchar *basic_type = "bynqihutxdsog";
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = basic_type[rand() % strlen(basic_type)];
            }
        }
        Test_set_g(handler, strv[0]);
        g_assert_cmpstr(handler->g, ==, strv[0]);
        Test_set_ag(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ag[i], ==, strv[i]);
        }
        g_strfreev(strv);
        // 测试invalidates属性,测试设置服务端属性值时使用Invalidate标记
        // 客户端本地对象值不会主动更新，当调用get_property接口时才会主动调用GetProperty读取值
        Test_set_emit_changed_invalidates(handler, handler->emit_changed_invalidates + rand() + 1);
        lb_wait_internal_queue_empty(1);
        // wb和awb为只写属性，不可读
        assert_get_remote_value(cli_handler, "wb", FALSE);
        assert_get_remote_value(cli_handler, "awb", FALSE);
        // 调用方法获取对端属性值，同步更新到本地缓存
        assert_get_remote_value(cli_handler, "y", TRUE);
        assert_get_remote_value(cli_handler, "ay", TRUE);
        assert_get_remote_value(cli_handler, "n", TRUE);
        assert_get_remote_value(cli_handler, "an", TRUE);
        assert_get_remote_value(cli_handler, "q", TRUE);
        assert_get_remote_value(cli_handler, "aq", TRUE);
        assert_get_remote_value(cli_handler, "i", TRUE);
        assert_get_remote_value(cli_handler, "ai", TRUE);
        assert_get_remote_value(cli_handler, "u", TRUE);
        assert_get_remote_value(cli_handler, "au", TRUE);
        assert_get_remote_value(cli_handler, "x", TRUE);
        assert_get_remote_value(cli_handler, "ax", TRUE);
        assert_get_remote_value(cli_handler, "t", TRUE);
        assert_get_remote_value(cli_handler, "at", TRUE);
        assert_get_remote_value(cli_handler, "d", TRUE);
        assert_get_remote_value(cli_handler, "ad", TRUE);
        assert_get_remote_value(cli_handler, "s", TRUE);
        assert_get_remote_value(cli_handler, "as", TRUE);
        assert_get_remote_value(cli_handler, "g", TRUE);
        assert_get_remote_value(cli_handler, "ag", TRUE);
        assert_get_remote_value(cli_handler, "o", TRUE);
        assert_get_remote_value(cli_handler, "ao", TRUE);
        // 测试invalidates属性
        // 不调用Test_Cli_get_emit_changed_invalidates前，属性值不会相同
        g_assert_cmpint(cli_handler->emit_changed_invalidates, !=, handler->emit_changed_invalidates);
        assert_get_invalidate_remote_value(cli_handler, handler, "emit_changed_invalidates");
        g_assert_cmpint(handler->emit_changed_invalidates, ==, cli_handler->emit_changed_invalidates);

        lbo_lock(cli_handler);
        // 已同步更新到本地缓存，无需等待应该值相同
        test_case_cmp_arr(wb, g_assert_cmpbool);
        test_case_cmp_arr(y, g_assert_cmpint);
        test_case_cmp_arr(n, g_assert_cmpint);
        test_case_cmp_arr(q, g_assert_cmpuint);
        test_case_cmp_arr(i, g_assert_cmpint);
        test_case_cmp_arr(u, g_assert_cmpuint);
        test_case_cmp_arr(x, g_assert_cmpint);
        test_case_cmp_arr(t, g_assert_cmpuint);
        test_case_cmp_arr(d, g_assert_cmpfloat);
        test_case_cmp_arr_str(s);
        test_case_cmp_arr_str(g);
        test_case_cmp_arr_str(o);
        lbo_unlock(cli_handler);
    }
    Test_unref(&handler);
    Test_Cli_unref(&cli_handler);
}

#define test_case_set_and_get_var(type, var, rand_max, assert_macro)                                                                       \
    do {                                                                                                                                   \
        gint cnt = rand() % 128 + 1;                                                                                                       \
        g##type *arr = g_malloc0(sizeof(g##type) * cnt);                                                                                   \
        g##type out;                                                                                                                       \
        g##type *out_arr = NULL;                                                                                                           \
        gsize n_out_arr = 0;                                                                                                               \
        for (int i = 0; i < cnt; i++)                                                                                                      \
            arr[i] = (g##type)(rand() % rand_max);                                                                                         \
        Test_set_##var(handler, arr[0]);                                                                                                   \
        assert_macro(handler->var, ==, arr[0]);                                                                                            \
        Test_set_a##var(handler, cnt, arr);                                                                                                \
        for (int i = 0; i < cnt; i++)                                                                                                      \
            assert_macro(handler->a##var[i], ==, arr[i]);                                                                                  \
        cleanup_error GError *error = NULL;                                                                                                \
        gint ret = Test_Cli_get_##var(cli_handler, &out, &error);                                                                          \
        g_assert_null(error);                                                                                                              \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        ret = Test_Cli_get_a##var(cli_handler, &n_out_arr, &out_arr, &error);                                                              \
        g_assert_null(error);                                                                                                              \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        assert_macro(arr[0], ==, out);                                                                                                     \
        assert_macro(cnt, ==, n_out_arr);                                                                                                  \
        assert_macro(arr[0], ==, out_arr[0]);                                                                                              \
        assert_macro(arr[1], ==, out_arr[1]);                                                                                              \
        g_free(out_arr);                                                                                                                   \
        g_free(arr);                                                                                                                       \
    } while (0)

static void test_object_set_and_get_remote_val(void)
{
#define TEST_OBJ_BASE "/com/litebmc/Ao_"
    Test handler = Test_get(OBJ_NAME1);
    Test_Cli cli_handler = (Test_Cli )lbo_cli_new(TEST_CLI, lb_bus_name(), OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(cli_handler), !=, 0);
    for (int cnt = 1; cnt < TEST_LOOP_CNT; cnt++) {
        cleanup_error GError *error = NULL;
        gint ret = 0;
        test_case_set_and_get_var(uint8, y, RAND_MAX, g_assert_cmpint);
        test_case_set_and_get_var(int16, n, RAND_MAX, g_assert_cmpint);
        test_case_set_and_get_var(uint16, q, RAND_MAX, g_assert_cmpuint);
        test_case_set_and_get_var(int32, i, RAND_MAX, g_assert_cmpint);
        test_case_set_and_get_var(uint32, u, RAND_MAX, g_assert_cmpuint);
        test_case_set_and_get_var(int64, x, RAND_MAX, g_assert_cmpint);
        test_case_set_and_get_var(uint64, t, RAND_MAX, g_assert_cmpuint);
        test_case_set_and_get_var(double, d, RAND_MAX, g_assert_cmpfloat);

        gchar **strv = g_malloc0(sizeof(gchar *) * (cnt + 1));
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 2;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        Test_set_s(handler, strv[0]);
        g_assert_cmpstr(handler->s, ==, strv[0]);
        Test_set_as(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->as[i], ==, strv[i]);
        }
        cleanup_gfree gchar *out_s = NULL;
        cleanup_strv gchar **out_s_arr = NULL;
        ret = Test_Cli_get_s(cli_handler, &out_s, &error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_null(error);
        ret = Test_Cli_get_as(cli_handler, &out_s_arr, &error);
        g_assert_null(error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_cmpstr(out_s, ==, strv[0]);
        g_assert_cmpstr(out_s_arr[0], ==, strv[0]);
        g_assert_cmpstr(out_s_arr[1], ==, strv[1]);
        g_assert_cmpstr(out_s_arr[2], ==, strv[2]);
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            memcpy(strv[i], TEST_OBJ_BASE, strlen(TEST_OBJ_BASE));
            for (int j = strlen(TEST_OBJ_BASE); j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
        }
        Test_set_o(handler, strv[0]);
        g_assert_cmpstr(handler->o, ==, strv[0]);
        Test_set_ao(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ao[i], ==, strv[i]);
        }
        cleanup_gfree gchar *out_o = NULL;
        cleanup_strv gchar **out_o_arr = NULL;
        ret = Test_Cli_get_o(cli_handler, &out_o, &error);
        g_assert_null(error);
        g_assert_cmpint(ret, ==, 0);
        ret = Test_Cli_get_ao(cli_handler, &out_o_arr, &error);
        g_assert_null(error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_cmpstr(out_o, ==, strv[0]);
        g_assert_cmpstr(out_o_arr[0], ==, strv[0]);
        g_assert_cmpstr(out_o_arr[1], ==, strv[1]);
        g_assert_cmpstr(out_o_arr[2], ==, strv[2]);
        g_strfreev(strv);

        strv = g_new0(gchar *, cnt + 1);
        gchar *basic_type = "bynqihutxdsog";
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = basic_type[rand() % strlen(basic_type)];
            }
        }
        Test_set_g(handler, strv[0]);
        g_assert_cmpstr(handler->g, ==, strv[0]);
        Test_set_ag(handler, strv);
        for (int i = 0; i < cnt; i++) {
            g_assert_cmpstr(handler->ag[i], ==, strv[i]);
        }
        cleanup_gfree gchar *out_g = NULL;
        cleanup_strv gchar **out_g_arr = NULL;
        ret = Test_Cli_get_g(cli_handler, &out_g, &error);
        g_assert_null(error);
        g_assert_cmpint(ret, ==, 0);
        ret = Test_Cli_get_ag(cli_handler, &out_g_arr, &error);
        g_assert_null(error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_cmpstr(out_g, ==, strv[0]);
        g_assert_cmpstr(out_g_arr[0], ==, strv[0]);
        g_assert_cmpstr(out_g_arr[1], ==, strv[1]);
        g_assert_cmpstr(out_g_arr[2], ==, strv[2]);
        g_strfreev(strv);
    }
    Test_unref(&handler);
    Test_Cli_unref(&cli_handler);
}

#define test_case_set_variant(type, var, assert_macro)                                                                                     \
    do {                                                                                                                                   \
        GVariantBuilder builder;                                                                                                           \
        const LBProperty *desc = lbo_get_property_desc(handler, "a" #var);                                                                  \
        g_variant_builder_init(&builder, G_VARIANT_TYPE("a" #var));                                                                        \
        gint cnt = rand() % 128 + 1;                                                                                                       \
        g##type *arr = g_malloc0(sizeof(g##type) * cnt);                                                                                   \
        if (!g_strcmp0(#var, "b")) {                                                                                                       \
            for (int i = 0; i < cnt; i++) {                                                                                                \
                arr[i] = (g##type)(rand() % 2);                                                                                            \
                g_variant_builder_add(&builder, #var, arr[i]);                                                                             \
            }                                                                                                                              \
        } else {                                                                                                                           \
            for (int i = 0; i < cnt; i++) {                                                                                                \
                arr[i] = (g##type)rand();                                                                                                  \
                g_variant_builder_add(&builder, #var, arr[i]);                                                                             \
            }                                                                                                                              \
        }                                                                                                                                  \
        GVariant *w_value = g_variant_builder_end(&builder);                                                                               \
        lbo_set_memory(handler, desc, w_value);                                                                                             \
        GVariant *r_value = lbo_get_memory(handler, desc);                                                                                  \
        g_assert_cmpint(g_variant_equal(r_value, w_value), ==, TRUE);                                                                      \
        g_variant_unref(r_value);                                                                                                          \
        g_variant_unref(w_value);                                                                                                          \
                                                                                                                                           \
        desc = lbo_get_property_desc(handler, #var);                                                                                         \
        w_value = g_variant_new_##type(arr[0]);                                                                                            \
        lbo_set_memory(handler, desc, w_value);                                                                                             \
        r_value = lbo_get_memory(handler, desc);                                                                                            \
        g_assert_cmpint(g_variant_equal(r_value, w_value), ==, TRUE);                                                                      \
        g_variant_unref(r_value);                                                                                                          \
        g_variant_unref(w_value);                                                                                                          \
        assert_macro(handler->var, ==, arr[0]);                                                                                            \
        for (int i = 0; i < cnt; i++) {                                                                                                    \
            assert_macro(handler->a##var[i], ==, arr[i]);                                                                                  \
        }                                                                                                                                  \
        g_free(arr);                                                                                                                       \
    } while (0)

#define test_empty_string ""
#define test_empty_object_path "/"
#define test_empty_signature "i"
#define test_str_variant(builder, handler, strv, type, gtype)                                                                              \
    do {                                                                                                                                   \
        const LBProperty *desc;                                                                                                           \
        GVariant *w_value;                                                                                                                 \
        GVariant *r_value;                                                                                                                 \
        w_value = g_variant_builder_end(&builder);                                                                                         \
        desc = lbo_get_property_desc(handler, "a" #type);                                                                                    \
        lbo_set_memory(handler, desc, w_value);                                                                                             \
        r_value = lbo_get_memory(handler, desc);                                                                                            \
        g_assert_cmpint(g_variant_equal(w_value, r_value), ==, TRUE);                                                                      \
        g_variant_unref(w_value);                                                                                                          \
        g_variant_unref(r_value);                                                                                                          \
                                                                                                                                           \
        gchar *str = strv[0] ? strv[0] : test_empty_##gtype;                                                                               \
        w_value = g_variant_new_##gtype(str);                                                                                              \
        desc = lbo_get_property_desc(handler, #type);                                                                                        \
        lbo_set_memory(handler, desc, w_value);                                                                                             \
        r_value = lbo_get_memory(handler, desc);                                                                                            \
        g_assert_cmpint(g_variant_equal(w_value, r_value), ==, TRUE);                                                                      \
        g_variant_unref(w_value);                                                                                                          \
        g_variant_unref(r_value);                                                                                                          \
        g_assert_cmpstr(handler->type, ==, str);                                                                                           \
        for (int i = 0; i < cnt; i++) {                                                                                                    \
            g_assert_cmpstr(handler->a##type[i], ==, strv[i]);                                                                             \
        }                                                                                                                                  \
    } while (0)

static void test_object_set_val_variant(void)
{
#define TEST_OBJ_BASE "/com/litebmc/Ao_"
    Test handler = Test_get(OBJ_NAME1);
    GVariantBuilder builder;
    for (int cnt = 0; cnt < TEST_LOOP_CNT; cnt++) {
        test_case_set_variant(int32, i, g_assert_cmpint);
        test_case_set_variant(uint8, y, g_assert_cmpint);
        test_case_set_variant(int16, n, g_assert_cmpint);
        test_case_set_variant(uint16, q, g_assert_cmpuint);
        test_case_set_variant(uint32, u, g_assert_cmpuint);
        test_case_set_variant(int64, x, g_assert_cmpint);
        test_case_set_variant(uint64, t, g_assert_cmpuint);
        test_case_set_variant(double, d, g_assert_cmpfloat);
        test_case_set_variant(boolean, b, g_assert_cmpint);

        // 测试as
        g_variant_builder_init(&builder, G_VARIANT_TYPE("as"));
        gchar **strv = g_malloc0(sizeof(gchar *) * (cnt + 1));
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 2;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
            g_variant_builder_add(&builder, "s", strv[i]);
        }
        test_str_variant(builder, handler, strv, s, string);
        g_strfreev(strv);

        // 测试ao
        g_variant_builder_init(&builder, G_VARIANT_TYPE("ao"));
        strv = g_new0(gchar *, cnt + 1);
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            memcpy(strv[i], TEST_OBJ_BASE, strlen(TEST_OBJ_BASE));
            for (int j = strlen(TEST_OBJ_BASE); j < len - 1; j++) {
                strv[i][j] = ('a' + rand() % 26);
            }
            g_variant_builder_add(&builder, "o", strv[i]);
        }
        test_str_variant(builder, handler, strv, o, object_path);
        g_strfreev(strv);

        g_variant_builder_init(&builder, G_VARIANT_TYPE("ag"));
        strv = g_new0(gchar *, cnt + 1);
        gchar *basic_type = "bynqihutxdsog";
        for (int i = 0; i < cnt; i++) {
            gint len = (rand() % 100) + 34;
            strv[i] = g_malloc0(len);
            for (int j = 0; j < len - 1; j++) {
                strv[i][j] = basic_type[rand() % strlen(basic_type)];
            }
            g_variant_builder_add(&builder, "g", strv[i]);
        }
        test_str_variant(builder, handler, strv, g, signature);
        g_strfreev(strv);
    }
    Test_unref(&handler);
}

static void test_pm_object_field(void)
{
    cleanup_Test Test handler = Test_get(OBJ_NAME1);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
    const LBProperty *field;
    field = lbo_get_property_desc(handler, "xxxxxxxxxx");
    g_assert_cmpuint(GPOINTER_TO_UINT(field), ==, 0);
    field = lbo_get_property_desc(handler, "b");
    g_assert_cmpstr(field->info->name, ==, "b");
    field = lbo_get_property_desc(handler, "ab");
    g_assert_cmpstr(field->info->name, ==, "ab");
    field = lbo_get_property_desc(handler, "i");
    g_assert_cmpstr(field->info->name, ==, "i");
    field = lbo_get_property_desc(handler, "ai");
    g_assert_cmpstr(field->info->name, ==, "ai");
    field = lbo_get_property_desc(handler, "u");
    g_assert_cmpstr(field->info->name, ==, "u");
    field = lbo_get_property_desc(handler, "au");
    g_assert_cmpstr(field->info->name, ==, "au");
    field = lbo_get_property_desc(handler, "x");
    g_assert_cmpstr(field->info->name, ==, "x");
    field = lbo_get_property_desc(handler, "ax");
    g_assert_cmpstr(field->info->name, ==, "ax");
    field = lbo_get_property_desc(handler, "t");
    g_assert_cmpstr(field->info->name, ==, "t");
    field = lbo_get_property_desc(handler, "at");
    g_assert_cmpstr(field->info->name, ==, "at");
    field = lbo_get_property_desc(handler, "d");
    g_assert_cmpstr(field->info->name, ==, "d");
    field = lbo_get_property_desc(handler, "ad");
    g_assert_cmpstr(field->info->name, ==, "ad");
    field = lbo_get_property_desc(handler, "s");
    g_assert_cmpstr(field->info->name, ==, "s");
    field = lbo_get_property_desc(handler, "as");
    g_assert_cmpstr(field->info->name, ==, "as");
    field = lbo_get_property_desc(handler, "ay");
    g_assert_cmpstr(field->info->name, ==, "ay");
    field = lbo_get_property_desc(handler, "g");
    g_assert_cmpstr(field->info->name, ==, "g");
    field = lbo_get_property_desc(handler, "ag");
    g_assert_cmpstr(field->info->name, ==, "ag");
    field = lbo_get_property_desc(handler, "o");
    g_assert_cmpstr(field->info->name, ==, "o");
    field = lbo_get_property_desc(handler, "ao");
    g_assert_cmpstr(field->info->name, ==, "ao");
}

static gboolean changed_bool;
G_LOCK_DEFINE_STATIC(lock);
static gchar *changed_str = NULL;
static gchar **changed_strv = NULL;
static gint changed_cnt = 0;
static void __object_changed(Test handler, const LBProperty *field, GVariant *value, gpointer user_data)
{
    g_assert_cmpuint(GPOINTER_TO_UINT(&changed_cnt), ==, GPOINTER_TO_UINT(user_data));
    Test test = (Test)handler;
    changed_cnt++;
    if (!field)
        return;
    G_LOCK(lock);
    Test_Properties *properties = Test_properties();
    if (field == &properties->b) {
        changed_bool = test->b ? TRUE : FALSE;
    } else if (field == &properties->s) {
        if (changed_str)
            g_free(changed_str);
        changed_str = g_strdup(test->s);
    } else if (field == &properties->as) {
        if (changed_strv)
            g_strfreev(changed_strv);
        changed_strv = g_strdupv(test->as);
    }
    G_UNLOCK(lock);
}

/**
 * @brief 测试pm_interface_on_property_changed接口，覆盖1.值变更触发事件;2.值未变时不触发
 *
 */
static void test_object_on_changed(void)
{
    // 同样的参数只有一次有效
    gpointer user_data = (gpointer)&changed_cnt;
    Test_b_hook(NULL, __object_changed, user_data);
    Test_ab_hook(NULL, __object_changed, user_data);
    Test_y_hook(NULL, __object_changed, user_data);
    Test_ay_hook(NULL, __object_changed, user_data);
    Test_n_hook(NULL, __object_changed, user_data);
    Test_an_hook(NULL, __object_changed, user_data);
    Test_q_hook(NULL, __object_changed, user_data);
    Test_aq_hook(NULL, __object_changed, user_data);
    Test_i_hook(NULL, __object_changed, user_data);
    Test_ai_hook(NULL, __object_changed, user_data);
    Test_u_hook(NULL, __object_changed, user_data);
    Test_au_hook(NULL, __object_changed, user_data);
    Test_x_hook(NULL, __object_changed, user_data);
    Test_ax_hook(NULL, __object_changed, user_data);
    Test_t_hook(NULL, __object_changed, user_data);
    Test_at_hook(NULL, __object_changed, user_data);
    Test_d_hook(NULL, __object_changed, user_data);
    Test_ad_hook(NULL, __object_changed, user_data);
    Test_s_hook(NULL, __object_changed, user_data);
    Test_as_hook(NULL, __object_changed, user_data);
    Test_o_hook(NULL, __object_changed, user_data);
    Test_ao_hook(NULL, __object_changed, user_data);
    Test_g_hook(NULL, __object_changed, user_data);
    Test_ag_hook(NULL, __object_changed, user_data);
    Test_emit_changed_hook(NULL, __object_changed, user_data);
    Test_not_emit_changed_hook(NULL, __object_changed, user_data);
    Test_emit_changed_invalidates_hook(NULL, __object_changed, user_data);
    // 等待1秒，确保事件队列清空
    lb_wait_internal_queue_empty(1);
    cleanup_Test Test handler = Test_get(OBJ_NAME1);
    changed_cnt = 0;
    // 覆盖从不在位变更为在位时触发的事件
    Test_present_set(handler, FALSE);
    Test_present_set(handler, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(changed_cnt, ==, 27);
    changed_cnt = 0;
    Test_present_set(handler, FALSE);
    Test_present_set(handler, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(changed_cnt, ==, 27);
    // 其它场景
    changed_bool = FALSE;
    Test_set_b(handler, FALSE);
    lb_wait_internal_queue_empty(1);
    Test_set_b(handler, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(changed_bool, ==, TRUE);

    // 值不变时不发送事件
    changed_bool = FALSE;
    Test_set_b(handler, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(changed_bool, ==, FALSE);
    gchar test[] = "1234";

    Test_set_s(handler, test);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(GPOINTER_TO_UINT(changed_str), !=, 0);
    g_assert_cmpstr(changed_str, ==, test);
    // 值不变时不发送事件
    g_free(changed_str);
    changed_str = NULL;
    Test_set_s(handler, test);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(GPOINTER_TO_UINT(changed_str), ==, 0);

    char *test_v[] = {"123123123", "123123123", NULL};
    Test_set_as(handler, test_v);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(GPOINTER_TO_UINT(changed_strv), !=, 0);
    for (int i = 0; i < 2; i++)
        g_assert_cmpstr(changed_strv[i], ==, test_v[i]);
    // 值不变时不发送事件
    g_strfreev(changed_strv);
    changed_strv = NULL;
    Test_set_as(handler, test_v);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(GPOINTER_TO_UINT(changed_strv), ==, 0);
}

typedef struct {
    LBO *handler;
    gint *cnt;
} ObjectPropertyChangedData;

static gint property_changed_cnt = 0;

static void __on_property_changed(Test handler, const LBProperty *field, GVariant *value, gpointer user_data)
{
    GVariantIter iter;
    char **strv;
    ObjectPropertyChangedData *data = (ObjectPropertyChangedData *)user_data;
    g_assert_cmpint(GPOINTER_TO_UINT(field), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(user_data), !=, 0);
    Test_Properties *properties = Test_properties();
    if (field == &properties->b) {
        log_debug("%s.%s %d", lbo_name(data->handler), "b", g_variant_get_boolean(value));
        Test_set_b(data->handler, g_variant_get_boolean(value));
    } else if (field == &properties->i) {
        log_debug("%s.%s %d", lbo_name(data->handler), "i", g_variant_get_int32(value));
        Test_set_i(data->handler, g_variant_get_int32(value));
    } else if (field == &properties->s) {
        gchar *str = g_strdup(g_variant_get_string(value, NULL));
        Test_set_s(data->handler, str);
        g_free(str);
    } else if (field == &properties->as) {
        g_variant_iter_init(&iter, value);
        gsize n = g_variant_n_children(value);
        strv = g_new0(char *, n + 1);
        int i = 0;
        while (g_variant_iter_loop(&iter, "s", &strv[i++]))
            ;
        Test_set_as(data->handler, strv);
        g_strfreev(strv);
    } else {
        log_error("Unkown property changed event, abort");
        g_assert_cmpbool(FALSE, ==, TRUE);
    }
    G_LOCK(lock);
    *(data->cnt) = *(data->cnt) + 1;
    G_UNLOCK(lock);
}

static ObjectPropertyChangedData data1;
static ObjectPropertyChangedData data2;
// 确认属性值多次变更时事件不乱序
static void test_object_on_property_changed_multi_times(void)
{
    gint bval;
    cleanup_Test Test tc1 = Test_get(OBJ_NAME1);
    cleanup_Test Test tc2 = Test_get(OBJ_NAME2);
    // 设置监听函数，随机生成bool值，等待一段时间后检查最终值是否是最后一次设置的值
    data1.handler = (LBO *)tc1;
    data1.cnt = &property_changed_cnt;
    gint ret = Test_on_prop_changed(tc2, "i", __on_property_changed, (gpointer)&data1, NULL);
    g_assert_cmpint(ret, ==, 0);
    ret = Test_on_prop_changed(tc2, "i", __on_property_changed, (gpointer)&data1, NULL);
    g_assert_cmpint(ret, ==, 0);
    lb_wait_internal_queue_empty(1);
    for (int j = 0; j < TEST_LOOP_CNT; j++) {
        property_changed_cnt = 0;
        for (int i = 0; i < TEST_LOOP_CNT; i++) {
            bval = rand();
            Test_set_i(tc2, bval);
        }
        lb_wait_internal_queue_empty(1);
        g_assert_cmpint(property_changed_cnt, !=, 0);
        g_assert_cmpint(tc2->i, ==, bval);
        g_assert_cmpint(tc1->i, ==, bval);
    }
    Test_set_b(tc2, FALSE);

    // 设置监听函数，随机生成字符串，等待一段时间后检查最终值是否是最后一次设置的字符串
    char *str = NULL;
    int len;
    property_changed_cnt = 0;
    ret = Test_on_prop_changed(tc2, "s", __on_property_changed, (gpointer)&data1, NULL);
    g_assert_cmpint(ret, ==, 0);
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        if (str)
            g_free(str);
        len = rand() % 1000 + 2;
        str = g_malloc0(len);
        for (int j = 0; j < len - 1; j++)
            str[j] = rand() % 26 + 'a';
        Test_set_s(tc2, str);
    }
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(*(data1.cnt), !=, 0);
    g_assert_cmpstr(tc1->s, ==, str);
    g_assert_cmpstr(tc2->s, ==, str);
    g_free(str);

    char **strs = NULL;
    int cnt = 0;
    property_changed_cnt = 0;
    ret = Test_on_prop_changed(tc2, "as", __on_property_changed, (gpointer)&data1, NULL);
    g_assert_cmpint(ret, ==, 0);

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        if (strs)
            g_strfreev(strs);
        cnt = rand() % 100 + 2;
        strs = g_new0(char *, cnt);
        for (int j = 0; j < cnt - 1; j++) {
            len = rand() % 1000 + 2;
            strs[j] = g_malloc0(len);
            for (int k = 0; k < len - 1; k++)
                strs[j][k] = rand() % 26 + 'a';
        }
        Test_set_as(tc2, strs);
    }
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(*(data1.cnt), !=, 0);
    for (int i = 0; i < cnt - 1; i++) {
        g_assert_cmpstr(tc2->as[i], ==, strs[i]);
        g_assert_cmpstr(tc1->as[i], ==, strs[i]);
    }
    g_strfreev(strs);
    property_changed_cnt = 0;
    // 事件已注册，重复触发会触发新消息
    ret = Test_on_prop_changed(tc2, "i", __on_property_changed, (gpointer)&data1, NULL);
    // 值变更触发一次
    Test_set_b(tc2, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(property_changed_cnt, ==, 3);
    // user_data发生变更，此次注册为新的事件，值变更时会发生两次事件
    data2.handler = tc1;
    data2.cnt = &property_changed_cnt;
    property_changed_cnt = 0;
    // 新注册事件时会触发一次消息，共4个回调
    ret = Test_on_prop_changed(tc2, "i", __on_property_changed, (gpointer)&data2, NULL);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(property_changed_cnt, ==, 4);

    // 值变更时会触发一次消息，共8个回调，changed_cnt值为8
    Test_set_i(tc2, rand());
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(property_changed_cnt, ==, 8);
}

static gboolean present = FALSE;
static void __object_removed(LBO *handler, gpointer user_data)
{
    present = lbo_present(handler);
}

static void test_lbo_changed(void)
{
    cleanup_Test Test handler1 = Test_get(OBJ_NAME1);
    cleanup_Test Test handler2 = Test_get(OBJ_NAME2);
    lb_impl._on_changed(TEST, __object_removed, NULL, NULL);
    Test_present_set(handler1, TRUE);
    Test_present_set(handler1, FALSE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(present, ==, FALSE);
    Test_present_set(handler1, TRUE);
    Test_present_set(handler2, TRUE);
    Test_present_set(handler2, FALSE);
    Test_present_set(handler2, TRUE);
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(present, ==, TRUE);
}

static void test_object_nth(void)
{
    Test handler = Test_nth(0);
    g_assert_cmpstr(lbo_name(handler), ==, OBJ_NAME1);
    Test_unref(&handler);
    handler = Test_nth(1);
    g_assert_cmpstr(lbo_name(handler), ==, OBJ_NAME2);
    Test_unref(&handler);
    handler = Test_nth(2);
    g_assert_cmpint(GPOINTER_TO_UINT(handler), ==, 0);
}

static void test_class_name(void)
{
    Test handler = Test_nth(0);
    g_assert_cmpstr(lbo_interface_name((LBO *)handler), ==, TEST->name);
    Test_unref(&handler);
    handler = Test_nth(1);
    g_assert_cmpstr(lbo_interface_name((LBO *)handler), ==, TEST->name);
    Test_unref(&handler);
}

static gchar *bind_data = NULL;
static void __free_bind_data(gchar *data)
{
    G_LOCK(lock);
    if (bind_data) {
        g_free(bind_data);
        bind_data = NULL;
    }
    bind_data = g_strdup(data);
    G_UNLOCK(lock);
}

static void test_object_bind(void)
{
    cleanup_Test Test handler = Test_nth(0);
    gchar str[] = "test23493824923423";
    Test_bind(handler, str, (GDestroyNotify)__free_bind_data);
    g_assert_cmpint(GPOINTER_TO_UINT(bind_data), ==, 0);
    g_assert_cmpstr(Test_data(handler), ==, str);

    Test_bind(handler, str, (GDestroyNotify)__free_bind_data);
    g_assert_cmpstr(bind_data, ==, str);
    g_assert_cmpstr(Test_data(handler), ==, str);
}

static GDBusConnection *bus = NULL;
static GMainLoop *bus_loop = NULL;
static void _on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    bus = connection;
    log_info("Bus acquired the name %s", name);
}

static void _on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    log_info("Name acquired the name %s", name);
}

static void _on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    log_warn("Lost the name %s, service name:%s", name);
    bus = NULL;
    exit(-1);
}

static gpointer _if_dbus_start(gpointer data)
{
    guint id;

    /* object path取类名 */
    while (1) {
        bus_loop = g_main_loop_new(NULL, FALSE);
        id = g_bus_own_name(G_BUS_TYPE_SESSION, "com.litebmc.TestClient",
                            G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT | G_BUS_NAME_OWNER_FLAGS_REPLACE, _on_bus_acquired, _on_name_acquired,
                            _on_name_lost, bus_loop, NULL);
        g_main_loop_run(bus_loop);

        g_bus_unown_name(id);
        g_main_loop_unref(bus_loop);
        bus_loop = NULL;
        bus = NULL;
    }
    return NULL;
}

static void start_dbus(void)
{
    GThread *thread = g_thread_new("dbus_run", _if_dbus_start, NULL);
    g_thread_unref(thread);
    sleep(1);
    log_info("Start client dbus");
}

static void dbus_init(void)
{
    if (!bus)
        start_dbus();
}

static int SetArgEmpty_ret = 0;

static int Test_SetArgEmpty_handler(Test object, const Test_SetArgEmpty_Req *req,
#ifdef LB_VERSION_BE_0_8_2
    Test_SetArgEmpty_Rsp **rsp, GError **error, LBMethodExtData *ext_data)
#else
    Test_SetArgEmpty_Rsp **rsp, GError **error, gpointer ext_data)
#endif
{
    log_debug("Call SetArgEmpty method");
    if (SetArgEmpty_ret == 0) {
        *rsp = g_new0(Test_SetArgEmpty_Rsp, 1);
    }
    return SetArgEmpty_ret;
}

static void test_call_set_arg_empty_method(void)
{
    Test_Methods *methods = Test_methods();
    methods->SetArgEmpty.handler = Test_SetArgEmpty_handler;
    GError *error = NULL;
    GVariant *replay = NULL;

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        SetArgEmpty_ret = 0;
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetArgEmpty", NULL, G_VARIANT_TYPE_UNIT,
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        if (error) {
            log_error("Call method SetArgEmpty Failed, error: %s", error->message);
        }
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
        g_variant_unref(replay);

        SetArgEmpty_ret = -1;
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetArgEmpty", NULL, G_VARIANT_TYPE_UNIT,
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), ==, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
        log_debug("Call method with error, message: %s, code: %d", error->message, error->code);
        g_error_free(error);
        error = NULL;
    }
}

static GVariant *set_string_replay = NULL;

#ifdef LB_VERSION_BE_0_8_2
static int Test_SetString_direct_replay_handler(Test object, const Test_SetString_Req *req,
    Test_SetString_Rsp **rsp, GError **error, LBMethodExtData *ext)
{
#else
static int Test_SetString_direct_replay_handler(Test object, const Test_SetString_Req *req,
    Test_SetString_Rsp **rsp, GError **error, gpointer ext_data)
{
    LBMethodExt *ext = (LBMethodExt *)ext_data;
    g_assert_cmpuint(ext->magic, ==, 0xdeadbeaf);
    g_assert_cmpuint(ext->reserved, ==, 0);
#endif
    g_assert_nonnull(ext->invocation);
    ext->direct_replay = g_variant_ref(set_string_replay);
    return 0;
}

static void test_call_SetString_direct_replay(void)
{
    Test_Methods *methods = Test_methods();
    methods->SetString.handler = Test_SetString_direct_replay_handler;
    GError *error = NULL;
    GVariant *replay = NULL;
    GVariant *request = NULL;
    gchar *test_str = "test_ssssss";
    gchar *test_arr[3] = {NULL};
    test_arr[0] = test_str;
    test_arr[1] = test_str;

    Test_SetString_Rsp out = {
        .out = test_str,
        .out_array = test_arr
    };

    set_string_replay = methods->SetString.rsp_encode(&out);
    g_variant_take_ref(set_string_replay);

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        request = g_variant_ref(set_string_replay);
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetString", request, G_VARIANT_TYPE("(sas)"),
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        if (error) {
            printf("Call method SetString Failed, error: %s\n", error->message);
        }
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
        g_variant_equal(replay, set_string_replay);
        g_variant_unref(replay);
    }
    g_variant_unref(set_string_replay);
}

/* 重新实现方法 */
int Test_WeakMethod2(Test object, const Test_WeakMethod2_Req *req,
#ifdef LB_VERSION_BE_0_8_2
    Test_WeakMethod2_Rsp **rsp, GError **error, LBMethodExtData *ext_data)
#else
    Test_WeakMethod2_Rsp **rsp, GError **error, gpointer ext_data)
#endif
{
    return 0;
}

static void test_call_weak_method(void)
{
    Test_Methods *methods = Test_methods();
    gint ret;
    GError *error = NULL;
    // handler句柄实现是当前文件实现的Test_WeakMethod2方法
    ret = methods->WeakMethod2.handler(NULL, NULL, NULL, &error, NULL);
    g_assert_cmpint(ret, ==, 0);
    // handler句柄实现是自动生成代码实现的Test_WeakMethod1方法
    ret = methods->WeakMethod1.handler(NULL, NULL, NULL, &error, NULL);
    g_assert_cmpint(ret, ==, -1);
    g_assert_nonnull(error);
    g_error_free(error);
}

#define method_copy_value(x) x
#define method_copy_string(x) g_strdup(x)
#define method_equal_value(x, y) ((x) == (y))
#define method_equal_string(x, y) (g_strcmp0(x, y) == 0)
#define Test_Method_type_array_type(method, c_type, variant_new_type, variant_type, a0, a1, a2, value_copy_func, cmp_func, ext_type)       \
    static int Test_##method##_ret = 0;                                                                                                    \
    static int Test_##method##_handler(Test handler, const Test_##method##_Req *req, Test_##method##_Rsp **rsp, GError **error,     \
        ext_type *ext_data)                                                                                                                 \
    {                                                                                                                                      \
        log_debug("Call method, n_in_array: %d", req->n_in_array);                                                                         \
        if (!cmp_func(req->in, a0))                                                                                                        \
            return -2;                                                                                                                     \
        if (req->n_in_array != 2 && req->n_in_array != 0)                                                                                  \
            return -3;                                                                                                                     \
        if (req->n_in_array == 2) {                                                                                                        \
            if (!cmp_func(req->in_array[0], a1))                                                                                           \
                return -4;                                                                                                                 \
            if (!cmp_func(req->in_array[1], a2))                                                                                           \
                return -5;                                                                                                                 \
        }                                                                                                                                  \
        if (Test_##method##_ret == 0) {                                                                                                    \
            *rsp = g_new0(Test_##method##_Rsp, 1);                                                                                         \
            (*rsp)->out = value_copy_func(req->in);                                                                                           \
            (*rsp)->n_out_array = req->n_in_array;                                                                                            \
            (*rsp)->out_array = g_new0(g##c_type, req->n_in_array);                                                                           \
            for (int i = 0; i < req->n_in_array; i++)                                                                                      \
                (*rsp)->out_array[i] = value_copy_func(req->in_array[i]);                                                                     \
        }                                                                                                                                  \
        return Test_##method##_ret;                                                                                                        \
    }                                                                                                                                      \
    static void test_call_##method##_method(void)                                                                                          \
    {                                                                                                                                      \
        Test_Methods *methods = Test_methods();                                                                                            \
        methods->method.handler = Test_##method##_handler;                                                                                 \
                                                                                                                                           \
        GError *error = NULL;                                                                                                              \
        GVariant *replay = NULL;                                                                                                           \
        GVariant *parameters = NULL;                                                                                                       \
        GVariantBuilder builder;                                                                                                           \
        g##c_type b;                                                                                                                       \
        g##c_type ab[2];                                                                                                                   \
        gint method_ret = -1;                                                                                                              \
                                                                                                                                           \
        b = a0;                                                                                                                            \
        ab[0] = a1;                                                                                                                        \
        ab[1] = a2;                                                                                                                        \
                                                                                                                                           \
        for (int i = 0; i < TEST_LOOP_CNT; i++) {                                                                                          \
            log_debug("Run test case: %s, step 1", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            g_variant_builder_add(&builder, variant_type, ab[0]);                                                                          \
            g_variant_builder_add(&builder, variant_type, ab[1]);                                                                          \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_error("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_variant_unref(replay);                                                                                                       \
                                                                                                                                           \
            log_debug("Run test case: %s, step 2", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_error("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_variant_unref(replay);                                                                                                       \
                                                                                                                                           \
            log_debug("Run test case: %s, step 3", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = -1;                                                                                                      \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_debug("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), ==, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);                                                                              \
            g_error_free(error);                                                                                                           \
            error = NULL;                                                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            cleanup_lbo LBO *cli_handler = lbo_cli_new(TEST_CLI, "com.litebmc.Test", OBJ_NAME1);                               \
            if (!cli_handler) {                                                                                                            \
                log_error("Create object %s failed, interface: %s", OBJ_NAME1, TEST_CLI->name);                                             \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(cli_handler), !=, 0);                                                                        \
            Test_##method##_Req req = {.in = b, .n_in_array = 2, .in_array = ab};                                                          \
            Test_##method##_Rsp *rsp = NULL;                                                                                                 \
            method_ret = Test_Cli_Call_##method(cli_handler, &req, &rsp, 10000, &error);                                                   \
            if (error) {                                                                                                                   \
                log_info("Call method" #method " Failed, error: %s", error->message);                                                      \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_assert_cmpuint(method_ret, ==, 0);                                                                                           \
            g_assert_cmpuint(b, ==, rsp->out);                                                                                              \
            g_assert_cmpuint(rsp->n_out_array, ==, 2);                                                                                      \
            g_assert_cmpuint(rsp->out_array[0], ==, ab[0]);                                                                                 \
            g_assert_cmpuint(rsp->out_array[1], ==, ab[1]);                                                                                 \
            methods->method.rsp_free(&rsp);                                                                                     \
            g_assert_null(rsp);                                                                                                             \
        }                                                                                                                                  \
        log_info("Run test case: %s finish", __FUNCTION__);                                                                                \
    }

#define Test_Method_string_array_type(method, c_type, variant_new_type, variant_type, a0, a1, a2, value_copy_func, cmp_func, ext_type)     \
    static int Test_##method##_ret = 0;                                                                                                    \
    static int Test_##method##_handler(Test handler, const Test_##method##_Req *req, Test_##method##_Rsp **rsp, GError **error,     \
        ext_type *ext_data)                                                                                                                 \
    {                                                                                                                                      \
        if (!cmp_func(req->in, a0))                                                                                                        \
            return -2;                                                                                                                     \
        if (Test_##method##_ret == 0) {                                                                                                    \
            *rsp = g_new0(Test_##method##_Rsp, 1);                                                                                          \
            (*rsp)->out = value_copy_func(req->in);                                                                                           \
            int n;                                                                                                                         \
            for (n = 0; req->in_array && req->in_array[n]; n++)                                                                            \
                ;                                                                                                                          \
            (*rsp)->out_array = g_new0(g##c_type, n + 1);                                                                                     \
            for (int i = 0; i < n; i++)                                                                                                    \
                (*rsp)->out_array[i] = value_copy_func(req->in_array[i]);                                                                     \
        }                                                                                                                                  \
        return Test_##method##_ret;                                                                                                        \
    }                                                                                                                                      \
    static void test_call_##method##_method(void)                                                                                          \
    {                                                                                                                                      \
        Test_Methods *methods = Test_methods();                                                                                            \
        methods->method.handler = Test_##method##_handler;                                                                                 \
                                                                                                                                           \
        GError *error = NULL;                                                                                                              \
        GVariant *replay = NULL;                                                                                                           \
        GVariant *parameters = NULL;                                                                                                       \
        GVariantBuilder builder;                                                                                                           \
        g##c_type b;                                                                                                                       \
        g##c_type ab[3] = {NULL};                                                                                                          \
        gint method_ret = -1;                                                                                                              \
                                                                                                                                           \
        b = a0;                                                                                                                            \
        ab[0] = a1;                                                                                                                        \
        ab[1] = a2;                                                                                                                        \
                                                                                                                                           \
        for (int i = 0; i < TEST_LOOP_CNT; i++) {                                                                                          \
            log_debug("Run test case: %s, step 1", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            g_variant_builder_add(&builder, variant_type, ab[0]);                                                                          \
            g_variant_builder_add(&builder, variant_type, ab[1]);                                                                          \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_error("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_variant_unref(replay);                                                                                                       \
                                                                                                                                           \
            log_debug("Run test case: %s, step 2", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_error("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_variant_unref(replay);                                                                                                       \
                                                                                                                                           \
            log_debug("Run test case: %s, step 3", __FUNCTION__);                                                                          \
            g_variant_builder_init(&builder, (const GVariantType *)"a" variant_type);                                                      \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);                                                                        \
            g_variant_builder_add_value(&builder, g_variant_new_##variant_new_type(b));                                                    \
            g_variant_builder_add_value(&builder, parameters);                                                                             \
            parameters = g_variant_builder_end(&builder);                                                                                  \
            Test_##method##_ret = -1;                                                                                                      \
            replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, #method, parameters,                              \
                                                 (const GVariantType *)"(" variant_type "a" variant_type ")", G_DBUS_CALL_FLAGS_NONE,      \
                                                 10000, NULL, &error);                                                                     \
            if (error) {                                                                                                                   \
                log_debug("Call method" #method " Failed, error: %s", error->message);                                                     \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(replay), ==, 0);                                                                             \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);                                                                              \
            g_error_free(error);                                                                                                           \
            error = NULL;                                                                                                                  \
            Test_##method##_ret = 0;                                                                                                       \
            cleanup_lbo LBO *cli_handler = lbo_cli_new(TEST_CLI, "com.litebmc.Test", OBJ_NAME1);                               \
            if (!cli_handler) {                                                                                                            \
                log_error("Create object %s failed, interface: %s", OBJ_NAME1, TEST_CLI->name);                                             \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(cli_handler), !=, 0);                                                                        \
            Test_##method##_Req req = {.in = b, .in_array = ab};                                                                           \
            Test_##method##_Rsp *rsp = NULL;                                                                                                 \
            method_ret = Test_Cli_Call_##method(cli_handler, &req, &rsp, 10000, &error);                                                   \
            if (error) {                                                                                                                   \
                log_info("Call method" #method " Failed, error: %s", error->message);                                                      \
            }                                                                                                                              \
            g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);                                                                              \
            g_assert_cmpuint(method_ret, ==, 0);                                                                                           \
            g_assert_cmpstr(b, ==, rsp->out);                                                                                               \
            g_assert_cmpstr(ab[0], ==, rsp->out_array[0]);                                                                                  \
            g_assert_cmpstr(ab[1], ==, rsp->out_array[1]);                                                                                  \
            g_assert_cmpuint(0, ==, GPOINTER_TO_UINT(rsp->out_array[2]));                                                                   \
            methods->method.rsp_free(&rsp);                                                                                     \
            g_assert_null(rsp);                                                                                                             \
        }                                                                                                                                  \
        log_info("Run test case: %s finish", __FUNCTION__);                                                                                \
    }

#ifdef LB_VERSION_BE_0_8_2
Test_Method_type_array_type(SetBool, boolean, boolean, "b", TRUE, FALSE, TRUE, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetByte, uint8, byte, "y", 0x01, 0x02, 0x03, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetInt16, int16, int16, "n", 4, 5, 6, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetUint16, uint16, uint16, "q", 7, 8, 9, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetInt32, int32, int32, "i", 0, 1, 2, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetUint32, uint32, uint32, "u", 3, 4, 5, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetInt64, int64, int64, "x", 6, 7, 8, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetUint64, uint64, uint64, "t", 9, 0, 1, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_type_array_type(SetDouble, double, double, "d", 1.1, 2.2, 3.3, method_copy_value, method_equal_value, LBMethodExtData);
Test_Method_string_array_type(SetString, char *, string, "s", "123", "456", "789", method_copy_string, method_equal_string, LBMethodExtData);
Test_Method_string_array_type(SetObjectPath, char *, object_path, "o", "/abc/123", "/abc/456", "/abc/789", method_copy_string,
                              method_equal_string, LBMethodExtData);
Test_Method_string_array_type(SetSignature, char *, signature, "g", "ay", "ax", "at", method_copy_string, method_equal_string, LBMethodExtData);
#else
Test_Method_type_array_type(SetBool, boolean, boolean, "b", TRUE, FALSE, TRUE, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetByte, uint8, byte, "y", 0x01, 0x02, 0x03, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetInt16, int16, int16, "n", 4, 5, 6, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetUint16, uint16, uint16, "q", 7, 8, 9, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetInt32, int32, int32, "i", 0, 1, 2, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetUint32, uint32, uint32, "u", 3, 4, 5, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetInt64, int64, int64, "x", 6, 7, 8, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetUint64, uint64, uint64, "t", 9, 0, 1, method_copy_value, method_equal_value, void);
Test_Method_type_array_type(SetDouble, double, double, "d", 1.1, 2.2, 3.3, method_copy_value, method_equal_value, void);
Test_Method_string_array_type(SetString, char *, string, "s", "123", "456", "789", method_copy_string, method_equal_string, void);
Test_Method_string_array_type(SetObjectPath, char *, object_path, "o", "/abc/123", "/abc/456", "/abc/789", method_copy_string,
                              method_equal_string, void);
Test_Method_string_array_type(SetSignature, char *, signature, "g", "ay", "ax", "at", method_copy_string, method_equal_string, void);
#endif

static int SetInArgEmpty = 0;
static int Test_SetInArgEmpty_handler(Test object, const Test_SetInArgEmpty_Req *req, Test_SetInArgEmpty_Rsp **rsp, GError **error,
#ifdef LB_VERSION_BE_0_8_2
        LBMethodExtData *ext_data)
#else
        gpointer ext_data)
#endif
{
    log_debug("Call SetInArgEmpty method");
    if (SetInArgEmpty == 0) {
        *rsp = g_new0(Test_SetInArgEmpty_Rsp, 1);                                                                                          \
        (*rsp)->out = g_strdup("/abc/abc");
        (*rsp)->out_array = g_new0(gchar *, 3);
        for (int i = 0; i < 2; i++)
            (*rsp)->out_array[i] = g_strdup("/abc/abc");
    }
    return SetInArgEmpty;
}

static void test_call_SetInArgEmpty_method(void)
{
    Test_Methods *methods = Test_methods();
    methods->SetInArgEmpty.handler = Test_SetInArgEmpty_handler;
    GError *error = NULL;
    GVariant *replay = NULL;

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        SetInArgEmpty = 0;
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetInArgEmpty", NULL, (const GVariantType *)"(oao)",
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        if (error) {
            log_error("Call method SetArgEmpty Failed, error: %s", error->message);
        }
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
        g_variant_unref(replay);

        SetInArgEmpty = -1;
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetInArgEmpty", NULL, (const GVariantType *)"(oao)",
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), ==, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
        log_debug("Call method with error, message: %s, code: %d", error->message, error->code);
        g_error_free(error);
        error = NULL;

        SetInArgEmpty = 0;
        cleanup_lbo LBO *cli_handler = lbo_cli_new(TEST_CLI, "com.litebmc.Test", OBJ_NAME1);
        if (!cli_handler) {
            log_error("Create object %s failed, interface: %s", OBJ_NAME1, TEST_CLI->name);
        }
        Test_SetInArgEmpty_Rsp *rsp = NULL;
        gint ret = Test_Cli_Call_SetInArgEmpty(cli_handler, &rsp, 10000, &error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_null(error);
        g_assert_nonnull(rsp);
        g_assert_cmpstr(rsp->out, ==, "/abc/abc");
        g_assert_nonnull(rsp->out_array);
        g_assert_cmpstr(rsp->out_array[0], ==, "/abc/abc");
        g_assert_cmpstr(rsp->out_array[1], ==, "/abc/abc");
        methods->SetInArgEmpty.rsp_free(&rsp);
        g_assert_null(rsp);

    }
}

static int SetOutArgEmpty = 0;

static int Test_SetOutArgEmpty_handler(Test handler, const Test_SetOutArgEmpty_Req *req, Test_SetOutArgEmpty_Rsp **rsp,
#ifdef LB_VERSION_BE_0_8_2
                                       GError **error, LBMethodExtData *ext_data)
#else
                                       GError **error, gpointer ext_data)
#endif
{
    log_debug("Call SetOutArgEmpty method");
    if (g_strcmp0(req->in, "/abc/abc"))
        return -1;
    for (int i = 0; req->in_array && req->in_array[i]; i++)
        if (g_strcmp0(req->in_array[i], "/abc/abc"))
            return -1;
    *rsp = g_new0(Test_SetOutArgEmpty_Rsp, 1);
    return SetOutArgEmpty;
}

static void test_call_SetOutArgEmpty_method(void)
{
    Test_Methods *methods = Test_methods();
    methods->SetOutArgEmpty.handler = Test_SetOutArgEmpty_handler;
    GError *error = NULL;
    GVariant *replay = NULL;
    GVariant *parameters = NULL;
    GVariantBuilder builder;
    gchar *b = "/abc/abc";
    gchar *ab[] = {"/abc/abc", "/abc/abc", NULL};

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        SetOutArgEmpty = 0;
        // 正常场景
        g_variant_builder_init(&builder, (const GVariantType *)"ao");
        g_variant_builder_add(&builder, "o", ab[0]);
        g_variant_builder_add(&builder, "o", ab[1]);
        parameters = g_variant_builder_end(&builder);
        g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);
        g_variant_builder_add_value(&builder, g_variant_new_object_path(b));
        g_variant_builder_add_value(&builder, parameters);
        parameters = g_variant_builder_end(&builder);
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetOutArgEmpty", parameters,
                                             (const GVariantType *)"()", G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        if (error) {
            log_error("Call method SetArgEmpty Failed, error: %s", error->message);
        }
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
        g_variant_unref(replay);

        // 正常场景,数组为空时
        g_variant_builder_init(&builder, (const GVariantType *)"ao");
        parameters = g_variant_builder_end(&builder);
        g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);
        g_variant_builder_add_value(&builder, g_variant_new_object_path(b));
        g_variant_builder_add_value(&builder, parameters);
        parameters = g_variant_builder_end(&builder);
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetOutArgEmpty", parameters,
                                             (const GVariantType *)"()", G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);
        g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
        g_variant_unref(replay);

        // 异常场景：入参为空
        replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetOutArgEmpty", NULL, (const GVariantType *)"()",
                                             G_DBUS_CALL_FLAGS_NONE, 10000, NULL, &error);

        g_assert_cmpuint(GPOINTER_TO_UINT(replay), ==, 0);
        g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
        log_debug("Call method with error, message: %s, code: %d", error->message, error->code);
        g_error_free(error);
        error = NULL;

        cleanup_lbo LBO *cli_handler = lbo_cli_new(TEST_CLI, "com.litebmc.Test", OBJ_NAME1);
        if (!cli_handler) {
            log_error("Create object %s failed, interface: %s", OBJ_NAME1, TEST_CLI->name);
        }
        Test_SetOutArgEmpty_Req req = {.in = b, .in_array = ab};
        gint ret = Test_Cli_Call_SetOutArgEmpty(cli_handler, &req, 10000, &error);
        g_assert_cmpint(ret, ==, 0);
        g_assert_null(error);
    }
}

static gchar *TestSignalAs[] = {"890", "", NULL};
static gint TestSignal_recv_cnt = 0;

#ifdef LB_VERSION_BE_0_8_2
static void TestSignal_callback(Test_Cli object, const gchar *sender_name, const Test_TestSignal_Msg *req, gpointer user_data, const LBSignalExtData *ext)
{
    TestSignal_recv_cnt++;
}

static void _Test_Signal_TestSignal(Test_Cli object, const gchar *destination, const Test_TestSignal_Msg *req, gpointer user_data, const LBSignalExtData *ext)
{
    TestSignal_recv_cnt++;
}
#else
static void TestSignal_callback(Test_Cli object, const gchar *sender_name, const Test_TestSignal_Msg *req, gpointer user_data)
{
    TestSignal_recv_cnt++;
}

static void _Test_Signal_TestSignal(Test_Cli object, const gchar *destination, const Test_TestSignal_Msg *req, gpointer user_data)
{
    TestSignal_recv_cnt++;
}
#endif

static void test_send_TestSignal(void)
{
    StructASS *parameters = g_new0(StructASS, 1);
    GError *error = NULL;

    Test_Signals *signals = Test_signals();
    guint sub1 = lb_impl.subscribe_signal(TEST, "com.litebmc.Test", (const LBSignal *)&signals->TestSignal, OBJ_NAME1, NULL,
                                      (lbo_signal_handler)TestSignal_callback, NULL);
    g_assert_cmpint(sub1, !=, 0);
    guint sub2 = Test_Cli_Subscribe_TestSignal(TestSignal_callback, "com.litebmc.Test", OBJ_NAME1, NULL, NULL);
    g_assert_cmpint(sub2, !=, 0);
    parameters->array_str = g_new0(gchar *, 3);
    parameters->array_str[0] = g_strdup(TestSignalAs[0]);
    parameters->array_str[1] = g_strdup(TestSignalAs[1]);
    parameters->str = g_strdup("123123");
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        cleanup_Test Test handler = Test_get(OBJ_NAME1);
        g_assert_cmpint(GPOINTER_TO_UINT(handler), !=, 0);
        Test_TestSignal_Msg msg = {0};
        msg.Enabled10 = parameters;
        #ifdef LB_VERSION_BE_0_8_2
        gboolean ret = Test_Emit_TestSignal(handler, NULL, &msg, &error);
        #else
        gboolean ret = Test_TestSignal_Signal(handler, NULL, &msg, &error);
        #endif
        if (error) {
            log_error("Send signal \"TestSignal\" with error, message: %s, code: %d", error->message, error->code);
        }
        g_assert_cmpint(ret, ==, TRUE);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
    }
    lb_wait_internal_queue_empty(1);
    // 等待1分钟,检查信号计数，因为关注了两次，故收到的消息数量应该是TEST_LOOP_CNT＊2
    g_assert_cmpint(TestSignal_recv_cnt, ==, TEST_LOOP_CNT * 2);
    TestSignal_recv_cnt = 0;

    guint sub3 = Test_Cli_Subscribe_TestSignal(_Test_Signal_TestSignal, NULL, NULL, NULL, NULL);
    g_assert_cmpint(sub3, !=, 0);
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        cleanup_Test Test handler = Test_get(OBJ_NAME1);
        Test_TestSignal_Msg msg = {0};
        msg.Enabled10 = parameters;
        #ifdef LB_VERSION_BE_0_8_2
        gboolean ret = Test_Emit_TestSignal(handler, NULL, &msg, &error);
        #else
        gboolean ret = Test_TestSignal_Signal(handler, NULL, &msg, &error);
        #endif
        if (error) {
            log_error("Send signal \"TestSignal\" with error, message: %s, code: %d", error->message, error->code);
        }
        g_assert_cmpint(ret, ==, TRUE);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
    }
    lb_wait_internal_queue_empty(1);
    // 等待1分钟,检查信号计数，因为关注了三次，故收到的消息数量应该是TEST_LOOP_CNT＊3
    g_assert_cmpint(TestSignal_recv_cnt, ==, (TEST_LOOP_CNT * 3));
    // 取消事件订阅
    Test_Cli_Unsubscribe_TestSignal(NULL);
    Test_Cli_Unsubscribe_TestSignal(&sub3);
    g_assert_cmpint(sub3, ==, 0);
    Test_Cli_Unsubscribe_TestSignal(&sub2);
    g_assert_cmpint(sub2, ==, 0);
    Test_Cli_Unsubscribe_TestSignal(&sub1);
    g_assert_cmpint(sub1, ==, 0);
    guint sub4 = Test_Cli_Subscribe_TestSignal(NULL, NULL, NULL, NULL, NULL);
    g_assert_cmpint(sub4, ==, 0);

    TestSignal_recv_cnt = 0;
    // 取消事件
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        cleanup_Test Test handler = Test_get(OBJ_NAME1);
        Test_TestSignal_Msg msg = {0};
        msg.Enabled10 = parameters;
        #ifdef LB_VERSION_BE_0_8_2
        gboolean ret = Test_Emit_TestSignal(handler, NULL, &msg, &error);
        #else
        gboolean ret = Test_TestSignal_Signal(handler, NULL, &msg, &error);
        #endif
        if (error) {
            log_error("Send signal \"TestSignal\" with error, message: %s, code: %d", error->message, error->code);
        }
        g_assert_cmpint(ret, ==, TRUE);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
    }
    // 检查计数不会变更
    g_assert_cmpint(TestSignal_recv_cnt, ==, 0);
    StructASS_free(&parameters);
}

#ifdef LB_VERSION_BE_0_8_2
static void _Signal_TestEmptyArgSignal(Test_Cli object, const gchar *destination, const Test_TestEmptyArgSignal_Msg *req,
                                       gpointer user_data, const LBSignalExtData *ext)
#else
static void _Signal_TestEmptyArgSignal(Test_Cli object, const gchar *destination, const Test_TestEmptyArgSignal_Msg *req,
                                       gpointer user_data)
#endif
{
    guint *cnt = (guint *)user_data;
    *cnt = *cnt + 1;
}

static void test_send_empty_arg_TestSignal(void)
{
    GError *error = NULL;
    guint cnt = 0;

    guint sub1 = Test_Cli_Subscribe_TestEmptyArgSignal(_Signal_TestEmptyArgSignal, NULL, NULL, NULL, (gpointer)&cnt);
    g_assert_cmpint(sub1, !=, 0);
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        cleanup_Test Test handler = Test_get(OBJ_NAME1);
        #ifdef LB_VERSION_BE_0_8_2
        gboolean ret = Test_Emit_TestEmptyArgSignal(handler, NULL, &error);
        #else
        gboolean ret = Test_TestEmptyArgSignal_Signal(handler, NULL, &error);
        #endif
        if (error) {
            log_error("Send signal \"TestEmptyArgSignal\" with error, message: %s, code: %d", error->message, error->code);
        }
        g_assert_cmpint(ret, ==, TRUE);
        g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
    }
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(cnt, ==, TEST_LOOP_CNT);
    Test_Cli_Subscribe_TestEmptyArgSignal(NULL, NULL, NULL, NULL, NULL);
}

static void test_variant_by_double_array(void)
{
    gdouble *dbl;
    gint n;
    GVariant *value;
    for (int i = 0; i < 1000; i++) {
        n = rand() % 10 + 1;
        dbl = g_new0(gdouble, n);
        for (int j = 0; j < n; j++) {
            dbl[j] = rand() / 3.1415926;
        }
        value = variant_by_double_array(dbl, n, "ab", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ab", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ay", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ay", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "an", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "an", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "aq", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "aq", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ai", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ai", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "au", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "au", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ax", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ax", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "at", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "at", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ad", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "ad", TRUE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "as", FALSE);
        g_variant_unref(value);
        value = variant_by_double_array(dbl, n, "as", TRUE);
        g_variant_unref(value);
        g_free(dbl);
    }
}

#define test_persist_basic(type, val_str, cmp_key, per_type)                                                                               \
    do {                                                                                                                                   \
        g##type val_i = val_str[0] == 'b' ? rand() % 2 : rand();                                                                           \
        GVariant *value = g_variant_new_##type(val_i);                                                                                     \
        persist_save_property(lbo_name(handler), val_str, value, per_type);                                                         \
        g_variant_unref(value);                                                                                                            \
        value = 0;                                                                                                                         \
                                                                                                                                           \
        persist_load_property(lbo_name(handler), val_str, &value, per_type);                                                        \
        g_assert_cmpuint(GPOINTER_TO_UINT(value), !=, 0);                                                                                  \
        g_assert_cmp##cmp_key(val_i, ==, g_variant_get_##type(value));                                                                     \
        g_variant_unref(value);                                                                                                            \
                                                                                                                                           \
        persist_save_property(lbo_name(handler), val_str, NULL, per_type);                                                          \
        value = (gpointer)1;                                                                                                               \
        persist_load_property(lbo_name(handler), val_str, &value, per_type);                                                        \
        g_assert_cmpuint(GPOINTER_TO_UINT(value), ==, 1);                                                                                  \
    } while (0)

#define gstring gchar *
#define gobject_path gchar *
#define gsignature gchar *
#define test_persist_string(type, val_str, val_i, cmp_key, per_type)                                                                       \
    do {                                                                                                                                   \
        GVariant *value = g_variant_new_##type(val_i);                                                                                     \
        persist_save_property(lbo_name(handler), val_str, value, per_type);                                                         \
        g_variant_unref(value);                                                                                                            \
        value = 0;                                                                                                                         \
                                                                                                                                           \
        persist_load_property(lbo_name(handler), val_str, &value, per_type);                                                        \
        g_assert_cmpuint(GPOINTER_TO_UINT(value), !=, 0);                                                                                  \
        g_assert_cmp##cmp_key(val_i, ==, g_variant_get_string(value, NULL));                                                               \
        g_variant_unref(value);                                                                                                            \
                                                                                                                                           \
        persist_save_property(lbo_name(handler), val_str, NULL, per_type);                                                          \
        value = (gpointer)1;                                                                                                               \
        persist_load_property(lbo_name(handler), val_str, &value, per_type);                                                        \
        g_assert_cmpuint(GPOINTER_TO_UINT(value), ==, 1);                                                                                  \
    } while (0)

static void test_persist(void)
{
    cleanup_Test Test handler = Test_get(OBJ_NAME1);
    g_assert_nonnull(handler);

    const gchar *val_str = "xxxxxxx";
    GVariant *value = NULL;
    persist_save_property(lbo_name(handler), "where TRUE", NULL, PER_SAVE);
    persist_save_property("where TRUE", val_str, NULL, PER_SAVE);
    persist_save_property(lbo_name(handler), val_str, NULL, PER_SAVE);
    persist_save_property(lbo_name(handler), val_str, NULL, PER_SAVE);

    persist_load_property(lbo_name(handler), val_str, NULL, PER_REBOOT);
    persist_load_property(lbo_name(handler), val_str, &value, PER_REBOOT);
    persist_load_property("where TRUE", val_str, &value, PER_REBOOT);
    persist_load_property(lbo_name(handler), "where TRUE;", &value, PER_REBOOT);

    for (int i = 0; i < 20; i++) {
        test_persist_basic(boolean, "b", uint, PER_POWER_OFF);
        test_persist_basic(int16, "n", int, PER_POWER_OFF);
        test_persist_basic(uint16, "q", uint, PER_POWER_OFF);
        test_persist_basic(int32, "i", int, PER_POWER_OFF);
        test_persist_basic(uint32, "u", uint, PER_POWER_OFF);
        test_persist_basic(int64, "x", int, PER_POWER_OFF);
        test_persist_basic(uint64, "t", uint, PER_POWER_OFF);
        test_persist_basic(double, "d", float, PER_POWER_OFF);
        test_persist_string(string, "s", "SADFLKASFJKLASFKALSKF", str, PER_POWER_OFF);
        test_persist_string(object_path, "o", "/com/litebmc/test_obj", str, PER_POWER_OFF);
        test_persist_string(signature, "g", "bynqiuxtdas", str, PER_POWER_OFF);
        test_persist_basic(boolean, "b", uint, PER_REBOOT);
        test_persist_basic(int16, "n", int, PER_REBOOT);
        test_persist_basic(uint16, "q", uint, PER_REBOOT);
        test_persist_basic(int32, "i", int, PER_REBOOT);
        test_persist_basic(uint32, "u", uint, PER_REBOOT);
        test_persist_basic(int64, "x", int, PER_REBOOT);
        test_persist_basic(uint64, "t", uint, PER_REBOOT);
        test_persist_basic(double, "d", float, PER_REBOOT);
        test_persist_string(string, "s", "SADFLKASFJKLASFKALSKF", str, PER_REBOOT);
        test_persist_string(object_path, "o", "/com/litebmc/test_obj", str, PER_REBOOT);
        test_persist_string(signature, "g", "bynqiuxtdas", str, PER_REBOOT);
    }
}

#ifdef LB_VERSION_BE_0_8_2
gint Test_Method_SetBool_ret_val(Test handler, const Test_SetBool_Req *req,
    Test_SetBool_Rsp **rsp, GError **error, LBMethodExtData *ext_data)
{
    return -1;
}

gint Test_Method_SetBool_ret_error(Test handler, const Test_SetBool_Req *req,
    Test_SetBool_Rsp **rsp, GError **error, LBMethodExtData *ext_data)
{
    *error = g_error_new(G_DBUS_ERROR, G_DBUS_ERROR_FAILED, "Method error, 12345678");
    return -1;
}
#else
gint Test_Method_SetBool_ret_val(Test handler, const Test_SetBool_Req *req,
    Test_SetBool_Rsp **rsp, GError **error, gpointer ext_data)
{
    return -1;
}

gint Test_Method_SetBool_ret_error(Test handler, const Test_SetBool_Req *req,
    Test_SetBool_Rsp **rsp, GError **error, gpointer ext_data)
{
    *error = g_error_new(G_DBUS_ERROR, G_DBUS_ERROR_FAILED, "Method error, 12345678");
    return -1;
}
#endif

static void test_method_failed(void)
{
    cleanup_lbo LBO *cli_handler = lbo_cli_new(TEST_CLI, "com.litebmc.Test", OBJ_NAME1);
    if (!cli_handler) {
        log_error("Create object %s failed, interface: %s", OBJ_NAME1, TEST_CLI->name);
    }
    g_assert_cmpuint(GPOINTER_TO_UINT(cli_handler), !=, 0);

    gboolean ab[2] = {FALSE, TRUE};
    GError *error = NULL;
    gint ret;

    Test_SetBool_Req req = {.in = TRUE, .n_in_array = 2, .in_array = ab};
    Test_SetBool_Rsp *rsp = NULL;

    ret = Test_Cli_Call_SetBool(cli_handler, &req, &rsp, 10000, &error);
    g_assert_cmpint(ret, !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(rsp), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
    log_info("Except OK, message: %s", error->message);
    g_error_free(error);
    error = NULL;

    Test_Methods *methods = Test_methods();
    methods->SetBool.handler = Test_Method_SetBool_ret_val;
    ret = Test_Cli_Call_SetBool(cli_handler, &req, &rsp, 10000, &error);
    g_assert_cmpint(ret, !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(rsp), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
    log_info("Except OK, message: %s", error->message);
    g_error_free(error);
    error = NULL;
    methods->SetBool.handler = Test_Method_SetBool_ret_error;

    ret = Test_Cli_Call_SetBool(cli_handler, &req, &rsp, 10000, &error);
    g_assert_cmpint(ret, !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(rsp), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), !=, 0);
    log_info("Except OK, message: %s", error->message);
    g_error_free(error);
    error = NULL;
}

static void test_property_changed_parameter_error(void)
{
    gint registed = 0;
    cleanup_Test Test handler1 = Test_get(OBJ_NAME1);
    registed = Test_on_prop_changed(handler1, "bbbbbbbbbbbb", __on_property_changed, NULL, NULL);
    // bbbbbbbbbbbb不存在时返回非0
    g_assert_cmpint(registed, !=, 0);
}

#define cmp_variant_to_double(g_type, c_type, rand_max, div)                                                                               \
    do {                                                                                                                                   \
        c_type value = ((c_type)(rand() % (rand_max)) / div);                                                                              \
        GVariant *variant = g_variant_new_##g_type(value);                                                                                 \
        gdouble out = variant_to_double(variant);                                                                                          \
        g_variant_unref(variant);                                                                                                          \
        g_assert_cmpfloat(out, ==, value);                                                                                                 \
    } while (0);

static void test_helper(void)
{
    GVariant *variant;
    g_assert_cmpfloat(variant_to_double(NULL), ==, 0);
    for (gint i = 0; i < TEST_LOOP_CNT; i++) {
        cmp_variant_to_double(boolean, gint, 2, 1);
        cmp_variant_to_double(byte, guint8, RAND_MAX, 1);
        cmp_variant_to_double(int16, gint16, RAND_MAX, 1);
        cmp_variant_to_double(uint16, guint16, RAND_MAX, 1);
        cmp_variant_to_double(int32, gint32, RAND_MAX, 1);
        cmp_variant_to_double(uint32, guint32, RAND_MAX, 1);
        cmp_variant_to_double(int64, gint64, RAND_MAX, 1);
        cmp_variant_to_double(uint64, guint64, RAND_MAX, 1);
        cmp_variant_to_double(double, gdouble, RAND_MAX, rand());
        cmp_variant_to_double(handle, gint32, RAND_MAX, 1);
        // string转double
        gdouble value = ((gdouble)(rand() % (RAND_MAX)) / 13);
        gchar *str = lb_printf("%.20f", value);
        variant = g_variant_new_string(str);
        g_free(str);

        gdouble out = variant_to_double(variant);
        g_variant_unref(variant);
        g_assert_cmpfloat(out, ==, value);

        variant = g_variant_new_object_path("/abc/efg");
        g_assert_cmpfloat(variant_to_double(variant), ==, 0);
        g_variant_unref(variant);
    }
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_by_double(0, '?', FALSE)), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_to_double_array(NULL, 0)), ==, 0);
    variant = g_variant_new_string("/abc/str");
    gint32 cnt = 0;
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_to_double_array(variant, &cnt)), ==, 0);
    g_variant_unref(variant);

    gdouble dbl_aar[3] = {1.2345, 2.345678, 3.45678901};
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_by_double_array(NULL, 2, "as", FALSE)), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_by_double_array(dbl_aar, 0, NULL, FALSE)), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(variant_by_double_array(dbl_aar, 3, "xx", FALSE)), ==, 0);

    // 构建as数组
    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE("as"));
    g_variant_builder_add_value(&builder, g_variant_new_string("1.234500"));
    g_variant_builder_add_value(&builder, g_variant_new_string("2.345678"));
    g_variant_builder_add_value(&builder, g_variant_new_string("3.456789"));
    variant = g_variant_builder_end(&builder);
    // 从dbl_aar转换成数组
    GVariant *out = variant_by_double_array(dbl_aar, 3, "as", FALSE);
    g_assert_cmpuint(GPOINTER_TO_UINT(out), !=, 0);
    // 比较数组
    g_assert_cmpint(g_variant_equal(out, variant), ==, TRUE);
    g_variant_unref(variant);
    g_variant_unref(out);
}

static void dict_ass_value_equal(GVariant *except, Test handler)
{
    GVariantIter iter;
    GVariantIter kv_iter;
    GVariant *next_mem = NULL;
    DictAss *ass = handler->ass;
    g_variant_iter_init(&iter, except);
    while (TRUE) {
        cleanup_unref GVariant *next_item = g_variant_iter_next_value(&iter);
        if (!next_item) {
            return;
        }
        g_variant_iter_init(&kv_iter, next_item);
        cleanup_unref GVariant *key = g_variant_iter_next_value(&kv_iter);

        const gchar *next_key = g_variant_get_string(key, NULL);
        next_mem = g_variant_iter_next_value(&kv_iter);
        const gchar *next_val = g_variant_get_string(next_mem, NULL);\
        DictAssItem *val = ass->lookup(ass, next_key);
        g_assert_cmpstr(val->Str, ==, next_val);
        g_variant_unref(next_mem);
    }
}

static void test_property_type_variant(void)
{
    gchar str1[128];
    gchar str2[128];
    gint ret;
    GVariant *val;

    LBO *handler = (LBO *)Test_get(OBJ_NAME1);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler), !=, 0);

    GVariantBuilder builder;
    const LBProperty *ass_desc = lbo_get_property_desc(handler, "ass");
    for (gint k = 0; k < TEST_LOOP_CNT; k++) {
        g_variant_builder_init(&builder, G_VARIANT_TYPE("a{ss}"));
        for (gint i = 0; i < rand() % 100; i++) {
            gint cnt = rand() % 127 + 1;
            for (gint j = 0; j < cnt; j++) {
                str1[j] = 'a' + rand() % 26;
                str2[j] = 'a' + rand() % 26;
            }
            str1[cnt] = '\0';
            str2[cnt] = '\0';
            g_variant_builder_add(&builder, "{ss}", str1, str2);
        }
        val = g_variant_builder_end(&builder);
        lbo_set_memory(handler, ass_desc, val);
        cleanup_unref GVariant *real_val = lbo_get_memory(handler, ass_desc);
        dict_ass_value_equal(val, handler);
        g_variant_unref(val);
    }
    // 类型不正确
    val = g_variant_new_boolean(rand() % 2);
    lbo_set_memory(handler, ass_desc, val);
    cleanup_unref GVariant *real_val = lbo_get_memory(handler, ass_desc);
    ret = g_variant_equal(val, real_val);
    g_assert_cmpint(ret, ==, FALSE);

    g_variant_unref(val);
    lbo_unref((LBO **)&handler);
}

static void test_bus_lost(void)
{
    g_main_loop_quit(bus_loop);
}

static void test_log_init(void)
{
    lb_log_set_service("test_pm");
    log_info("log start");
    lb_log_set_alert_abort(FALSE);
    log_alert("log_alert");
}

static gboolean get_name_owner_changed_signal = FALSE;
static void _org_freedesktop_DBus_Cli_Listen_NameOwnerChanged(DBus_Cli object, const gchar *destination,
#ifdef LB_VERSION_BE_0_8_2
    const DBus_NameOwnerChanged_Msg *req, gpointer user_data, const LBSignalExtData *ext)
#else
    const DBus_NameOwnerChanged_Msg *req, gpointer user_data)
#endif
{
    log_info("NameOwnerChanged, name: %s, old_owner: %s, new_owner: %s", req->name, req->old_owner, req->new_owner);
    get_name_owner_changed_signal = TRUE;
}

static void test_owner_changed(void)
{
    DBus_Cli_Subscribe_NameOwnerChanged(_org_freedesktop_DBus_Cli_Listen_NameOwnerChanged, NULL, NULL, "com.litebmc.TestClient", NULL);
    if (bus_loop)
        g_main_loop_quit(bus_loop);
    dbus_init();
    lb_wait_internal_queue_empty(1);
    g_assert_cmpint(get_name_owner_changed_signal, ==, TRUE);
}

static int MultiThreadCalledCnt = 0;
static int MultiThreadSendCnt = 0;
G_LOCK_DEFINE_STATIC(MultiThreadLock);
static int test_call_method_multi_thread_handler(Test object, const Test_SetArgEmpty_Req *req, Test_SetArgEmpty_Rsp **rsp,
#ifdef LB_VERSION_BE_0_8_2
    GError **error, LBMethodExtData *ext_data)
#else
    GError **error, gpointer ext_data)
#endif
{
    // 等待20ms
    usleep(20000);
    G_LOCK(MultiThreadLock);
    MultiThreadCalledCnt++;
    G_UNLOCK(MultiThreadLock);
    *rsp = g_new0(Test_SetArgEmpty_Rsp, 1);
    return 0;
}

static void test_call_method_multi_thread_task(gpointer arg)
{
    log_debug("Run test case: %s", __FUNCTION__);
    GError *error = NULL;
    GVariant *replay = NULL;
    replay = g_dbus_connection_call_sync(bus, LB_BUS_NAME, OBJ_NAME1, TEST->name, "SetArgEmpty", NULL, G_VARIANT_TYPE_UNIT,
                                         G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
    if (error) {
        printf("Call method failed, message: %s\n", error->message);
    }
    g_assert_cmpuint(GPOINTER_TO_UINT(replay), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(error), ==, 0);
    g_variant_unref(replay);
    G_LOCK(MultiThreadLock);
    MultiThreadSendCnt++;
    G_UNLOCK(MultiThreadLock);
}

static void test_call_method_multi_thread(void)
{
    Test_Methods *methods = Test_methods();
    methods->SetArgEmpty.handler = test_call_method_multi_thread_handler;

    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        GThread *thread = g_thread_new("DelayMethod", (GThreadFunc)test_call_method_multi_thread_task, NULL);
        g_thread_unref(thread);
    }

    // 最多等60s,判断命令回调次数等于命令调用次数
    for (int i = 0; i < 60 && MultiThreadSendCnt != TEST_LOOP_CNT; i++)
        sleep(1);
    g_assert_cmpint(MultiThreadCalledCnt, ==, TEST_LOOP_CNT);
    g_assert_cmpint(MultiThreadSendCnt, ==, TEST_LOOP_CNT);
}

guint tc1_removed = 0;
guint tc2_removed = 0;
guint tc1_added = 0;
guint tc2_added = 0;
#ifdef LB_VERSION_BE_0_8_2
static void test_object_added(ObjectManager_Cli object, const gchar *destination, const ObjectManager_InterfacesAdded_Msg *req, gpointer user_data, const LBSignalExtData *ext)
#else
static void test_object_added(ObjectManager_Cli object, const gchar *destination, const ObjectManager_InterfacesAdded_Msg *req, gpointer user_data)
#endif
{
    log_info("receive added signal, object: %s", req->ObjectPath);
    if (g_strcmp0(req->ObjectPath, OBJ_NAME1) == 0) {
        tc1_added = 1;
    } else if (g_strcmp0(req->ObjectPath, OBJ_NAME2) == 0) {
        tc2_added = 1;
    }
    // 以接口为key的字典，必须存在
    InterfaceDataItem *interface_item = req->Interfaces->lookup(req->Interfaces, TEST->name);
    g_assert_nonnull(interface_item);
}

#ifdef LB_VERSION_BE_0_8_2
static void test_object_removed(ObjectManager_Cli object, const gchar *destination, const ObjectManager_InterfacesRemoved_Msg *req, gpointer user_data, const LBSignalExtData *ext)
#else
static void test_object_removed(ObjectManager_Cli object, const gchar *destination, const ObjectManager_InterfacesRemoved_Msg *req, gpointer user_data)
#endif
{
    log_info("receive removed signal, object: %s", req->ObjectPath);
    if (g_strcmp0(req->ObjectPath, OBJ_NAME1) == 0) {
        tc1_removed = 1;
    } else if (g_strcmp0(req->ObjectPath, OBJ_NAME2) == 0) {
        tc2_removed = 1;
    }
}

static void get_object_objects_foreach(const gchar *key, ObjectDataItem *value, gpointer user_data)
{
    GSList **list = (GSList **)user_data;
    *list = g_slist_append(*list, g_strdup(key));
}

void test_object_manager_get_all_objects(gchar *obj_name)
{
    GError *error = NULL;
    gint ret;
    ObjectManager_GetManagedObjects_Rsp *rsp = NULL;
    GSList *received_objects = NULL;
    tc1_removed = 0;
    tc2_removed = 0;
    tc1_added = 0;
    tc2_added = 0;

    ObjectManager_Cli test = lbo_cli_new(OBJECT_MANAGER_CLI, LB_BUS_NAME, OBJ_NAME1);
    g_assert_nonnull(test);
    ObjectManager_Cli om_cli2 = lbo_cli_new(OBJECT_MANAGER_CLI, LB_BUS_NAME, OBJ_NAME2);
    g_assert_nonnull(om_cli2);
    // 等待100000us以清除事件队列
    lb_wait_internal_queue_empty(1);
    guint sub = ObjectManager_Cli_Subscribe_InterfacesAdded(test_object_added, LB_BUS_NAME, NULL, NULL, NULL);
    g_assert_cmpuint(sub, !=, 0);
    sub = ObjectManager_Cli_Subscribe_InterfacesRemoved(test_object_removed, LB_BUS_NAME, NULL, NULL, NULL);
    g_assert_cmpuint(sub, !=, 0);
    cleanup_Test Test handler1 = Test_get(OBJ_NAME1);
    cleanup_Test Test handler2 = Test_get(OBJ_NAME2);
    g_assert_cmpint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(handler2), !=, 0);
    Test_present_set(handler1, FALSE);
    Test_present_set(handler2, FALSE);
    // 因为在位变更事件回调中会检查在位信号，所以快速变更在位并不一定会触发InterfacesAdded或InterfacesRemoved信号
    lb_wait_internal_queue_empty(1);
    ret = ObjectManager_Cli_Call_GetManagedObjects(test, &rsp, 1000, &error);
    // ObjectManager不可能独立存在，此时会报告对象不存在
    g_assert_cmpint(ret, ==, -1);
    g_assert_nonnull(error);
    g_error_free(error);
    error = NULL;

    Test_present_set(handler1, TRUE);
    Test_present_set(handler2, TRUE);
    lb_wait_internal_queue_empty(1);
    cleanup_ObjectManager ObjectManager mgr_obj = ObjectManager_get(OBJ_NAME1);
    g_assert_nonnull(mgr_obj);
    g_assert_cmpuint(tc1_removed, ==, 1);
    g_assert_cmpuint(tc2_removed, ==, 1);
    g_assert_cmpuint(tc1_added, ==, 1);
    g_assert_cmpuint(tc2_added, ==, 1);
    ret = ObjectManager_Cli_Call_GetManagedObjects(test, &rsp, 1000, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(rsp), !=, 0);
    g_assert_cmpint(GPOINTER_TO_UINT(error), ==, 0);
    rsp->Objects->foreach(rsp->Objects, get_object_objects_foreach, &received_objects);
    ObjectManager_GetManagedObjects_Rsp_free(&rsp);
    g_assert_cmpint(g_slist_length(received_objects), ==, 1);
    g_slist_free_full(received_objects, g_free);
}

void test_app_name(void)
{
    const gchar *app_name = lb_app_name();
    g_assert_cmpstr(app_name, ==, "Test");
    app_name = lb_app_name();
    g_assert_cmpstr(app_name, ==, "Test");
}

void test_object_manager(void)
{
    cleanup_ObjectManager ObjectManager obj = ObjectManager_get("/com");
    g_assert_null(obj);
    obj = ObjectManager_get("/com/litebmc");
    g_assert_null(obj);
    obj = ObjectManager_get("/com/litebmc/test");
    g_assert_null(obj);
    test_object_manager_get_all_objects(OBJ_NAME1);
}

void test_bus_type_set_and_get(void)
{
    lb_set_bus_type(G_BUS_TYPE_SYSTEM);
    g_assert_cmpint(G_BUS_TYPE_SYSTEM, ==, lb_get_bus_type());
    lb_set_bus_type(G_BUS_TYPE_NONE);
    g_assert_cmpint(G_BUS_TYPE_SYSTEM, ==, lb_get_bus_type());
    lb_set_bus_type(G_BUS_TYPE_STARTER);
    g_assert_cmpint(G_BUS_TYPE_SYSTEM, ==, lb_get_bus_type());
    lb_set_bus_type(G_BUS_TYPE_SESSION);
    g_assert_cmpint(G_BUS_TYPE_SESSION, ==, lb_get_bus_type());
}

void test_codegen_prop_flags(void)
{
    cleanup_lbo LBO *test = lbo_new(TEST, OBJ_NAME1, NULL);
    const LBProperty *prop = lbo_get_property_desc(test, "b");
    g_assert_nonnull(prop);
    // 私有成员没有emit属性
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_PRIVATE);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_READABLE);
    prop = lbo_get_property_desc(test, "wb");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_EMIT_TRUE);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE);
    prop = lbo_get_property_desc(test, "y");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_EMIT_TRUE);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE | G_DBUS_PROPERTY_INFO_FLAGS_READABLE);
    prop = lbo_get_property_desc(test, "as");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_DEPRECATED | LB_FLAGS_PROPERTY_EMIT_TRUE);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE | G_DBUS_PROPERTY_INFO_FLAGS_READABLE);
    prop = lbo_get_property_desc(test, "emit_changed_invalidates");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_EMIT_INVALIDATES);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE | G_DBUS_PROPERTY_INFO_FLAGS_READABLE);
    prop = lbo_get_property_desc(test, "emits_const");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_EMIT_CONST);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_READABLE | G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE);
    prop = lbo_get_property_desc(test, "emits_false");
    g_assert_nonnull(prop);
    g_assert_cmpuint(prop->flags, ==, LB_FLAGS_PROPERTY_EMIT_FALSE);
    g_assert_cmpuint(prop->info->flags, ==, G_DBUS_PROPERTY_INFO_FLAGS_READABLE | G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE);
}

void test_unique_name_and_well_known(void)
{
    cleanup_gfree gchar *unique_name = lb_unique_name(lb_bus_name());
    g_assert_nonnull(unique_name);
    g_assert_cmpint(unique_name[0], ==, ':');
    cleanup_gfree gchar *well_known = lb_well_known(unique_name);
    g_assert_nonnull(well_known);
    g_assert_cmpstr(well_known, ==, lb_bus_name());
}

void test_object_manager_present(void)
{
    gboolean exist = FALSE;
    cleanup_Test Test handler1 = Test_new(OBJ_NAME1, &exist);
    g_assert_nonnull(handler1);
    // 当一个对象的其它自定义接口都不在位时，ObjectManager对象也不在位
    Test_present_set(handler1, FALSE);
    lb_wait_internal_queue_empty(5);
    ObjectManager om = ObjectManager_get(OBJ_NAME1);
    g_assert_null(om);
    Test_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(5);
    // 当一个对象的其它自定义接口有一个在位时，ObjectManager对象自动创建并在位
    om = ObjectManager_get(OBJ_NAME1);
    g_assert_nonnull(om);
    g_assert_cmpbool(lbo_present(om), ==, TRUE);
    ObjectManager_unref(&om);
}

#ifdef LB_CODEGEN_BE_5_2
#define _validate_number_match_items(ctype, prop, val1, val2, val3, invalid)                                                               \
    static void test_validate_##prop(void)                                                                                                 \
    {                                                                                                                                      \
        GError *error = NULL;                                                                                                              \
        gint ret;                                                                                                                          \
        Test handler = Test_get(OBJ_NAME1);                                                                                                \
        Test_Cli cli_handler = Test_Cli_new(lb_bus_name(), OBJ_NAME1);                                                                     \
        g_assert_nonnull(handler);                                                                                                         \
        g_assert_nonnull(cli_handler);                                                                                                     \
        /* 可选值为val1, val2, val3，invalid是非法值 */                                                                           \
        /* 121 OK */                                                                                                                       \
        g##ctype array[3] = {val1};                                                                                                        \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 1, array, &error);                                                              \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* 234 OK */                                                                                                                       \
        array[0] = val2;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 1, array, &error);                                                              \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* 250 OK */                                                                                                                       \
        array[0] = val3;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 1, array, &error);                                                              \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* 122 not OK */                                                                                                                   \
        array[1] = invalid;                                                                                                                \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 2, array, &error);                                                              \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* 122 not OK */                                                                                                                   \
        array[0] = invalid;                                                                                                                \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 1, array, &error);                                                              \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* not unique*/                                                                                                                    \
        array[0] = val1;                                                                                                                   \
        array[1] = val1;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 2, array, &error);                                                              \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* less than min_items */                                                                                                          \
        array[0] = invalid;                                                                                                                \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 0, array, &error);                                                              \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* bigger than max_items */                                                                                                        \
        array[0] = val1;                                                                                                                   \
        array[1] = val2;                                                                                                                   \
        array[2] = val3;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, 3, array, &error);                                                              \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        ret = Test_Cli_set_match_##prop(cli_handler, val1, &error);                                                                        \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        ret = Test_Cli_set_match_##prop(cli_handler, invalid, &error);                                                                     \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        Test_present_set(handler, TRUE);                                                                                                   \
        Test_Cli_present_set(cli_handler, TRUE);                                                                                           \
        Test_unref(&handler);                                                                                                              \
        Test_Cli_unref(&cli_handler);                                                                                                      \
    }

_validate_number_match_items(double, double, 121, 234, 250, 122);
_validate_number_match_items(int16, int16, 121, 234, 250, 122);
_validate_number_match_items(int32, int32, 121, 234, 250, 122);
_validate_number_match_items(int64, int64, 121, 234, 250, 122);
_validate_number_match_items(int64, ssize, 121, 234, 250, 122);
_validate_number_match_items(uint8, byte, 121, 234, 250, 122);
_validate_number_match_items(uint16, uint16, 121, 234, 250, 122);
_validate_number_match_items(uint32, uint32, 121, 234, 250, 122);
_validate_number_match_items(uint64, uint64, 121, 234, 250, 122);
_validate_number_match_items(uint64, size, 121, 234, 250, 122);

#define _validate_string_match_items(prop, val1, val2, val3, invalid)                                                                      \
    static void test_validate_##prop(void)                                                                                                 \
    {                                                                                                                                      \
        GError *error = NULL;                                                                                                              \
        gint ret;                                                                                                                          \
        Test handler = Test_get(OBJ_NAME1);                                                                                                \
        Test_Cli cli_handler = Test_Cli_new(lb_bus_name(), OBJ_NAME1);                                                                     \
        g_assert_nonnull(handler);                                                                                                         \
        g_assert_nonnull(cli_handler);                                                                                                     \
        /* 可选值为val1, val2, val3，invalid是非法值 */                                                                           \
        /* 121 OK */                                                                                                                       \
        gchar *array[4] = {val1, NULL, NULL, NULL};                                                                                        \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* val1 OK */                                                                                                                      \
        array[0] = val2;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* val2 OK */                                                                                                                      \
        array[0] = val3;                                                                                                                   \
        array[1] = NULL;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        /* invalid not OK */                                                                                                               \
        array[1] = invalid;                                                                                                                \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* invalid not OK */                                                                                                               \
        array[0] = invalid;                                                                                                                \
        array[1] = NULL;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* 非unique */                                                                                                                    \
        array[0] = val1;                                                                                                                   \
        array[1] = val1;                                                                                                                   \
        array[2] = NULL;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* min_items */                                                                                                                    \
        array[0] = NULL;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* max_items */                                                                                                                    \
        array[0] = val1;                                                                                                                   \
        array[1] = val2;                                                                                                                   \
        array[2] = val3;                                                                                                                   \
        array[3] = NULL;                                                                                                                   \
        ret = Test_Cli_set_match_array_##prop(cli_handler, array, &error);                                                                 \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        ret = Test_Cli_set_match_##prop(cli_handler, val1, &error);                                                                        \
        g_assert_cmpint(ret, ==, 0);                                                                                                       \
        g_assert_null(error);                                                                                                              \
        ret = Test_Cli_set_match_##prop(cli_handler, invalid, &error);                                                                     \
        g_assert_cmpint(ret, ==, -1);                                                                                                      \
        g_assert_nonnull(error);                                                                                                           \
        g_error_free(error);                                                                                                               \
        error = NULL;                                                                                                                      \
        /* 释放资源 */                                                                                                                 \
        Test_present_set(handler, TRUE);                                                                                                   \
        Test_Cli_present_set(cli_handler, TRUE);                                                                                           \
        Test_unref(&handler);                                                                                                              \
        Test_Cli_unref(&cli_handler);                                                                                                      \
    }
_validate_string_match_items(string, "abc", "efg", "hij", "acd");
_validate_string_match_items(object_path, "/abc/121", "/efg/234", "/hij/250", "/adc/123");
_validate_string_match_items(signature, "a(ss)", "a{ss}", "a{st}", "a{si}");

static void test_validate_enum(void)
{
    GError *error = NULL;
    gint ret;
    Test handler = Test_get(OBJ_NAME1);
    Test_Cli cli_handler = Test_Cli_new(lb_bus_name(), OBJ_NAME1);
    g_assert_nonnull(handler);
    g_assert_nonnull(cli_handler);
    ret = Test_Cli_set_test_enum(cli_handler, TestFileType_Symlink, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_null(error);
    ret = Test_Cli_set_test_enum(cli_handler, _TestFileType_Invalid, &error);
    g_assert_cmpint(ret, ==, -1);
    g_assert_nonnull(error);
    g_error_free(error);
    error = NULL;
    TestFileType types[4] = {0};
    ret = Test_Cli_set_test_enum_array(cli_handler, 0, types, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_null(error);
    types[0] = TestFileType_Symlink;
    ret = Test_Cli_set_test_enum_array(cli_handler, 1, types, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_null(error);
    types[1] = TestFileType_RegularFile;
    ret = Test_Cli_set_test_enum_array(cli_handler, 2, types, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_null(error);
    types[2] = TestFileType_Directory;
    ret = Test_Cli_set_test_enum_array(cli_handler, 3, types, &error);
    g_assert_cmpint(ret, ==, 0);
    g_assert_null(error);
    types[3] = _TestFileType_Invalid;
    ret = Test_Cli_set_test_enum_array(cli_handler, 4, types, &error);
    g_assert_cmpint(ret, ==, -1);
    g_assert_nonnull(error);
    g_error_free(error);
    Test_present_set(handler, TRUE);
    Test_Cli_present_set(cli_handler, TRUE);
    Test_unref(&handler);
    Test_Cli_unref(&cli_handler);
}
#endif
void test_add_test_case(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_usec);
    lb_log_set_level(LOG_INFO);
    new_test_object();
    g_test_add_func("/test/bus_type_set_and_get", test_bus_type_set_and_get);
    g_test_add_func("/test/codegen_prop_flags", test_codegen_prop_flags);
    g_test_add_func("/test/unique_name_and_well_known", test_unique_name_and_well_known);
    g_test_add_func("/test/object_manager", test_object_manager);
    g_test_add_func("/test/object_manager_present", test_object_manager_present);
    g_test_add_func("/test/app_name", test_app_name);
    g_test_add_func("/test/owner_changed", test_owner_changed);
    g_test_add_func("/test/helper", test_helper);
    g_test_add_func("/test/bus_lost", test_bus_lost);
    g_test_add_func("/test/call_persist", test_persist);
    g_test_add_func("/test/property_type_variant", test_property_type_variant);
    g_test_add_func("/test/variant_by_double_array", test_variant_by_double_array);
    g_test_add_func("/test/started", test_start_called);
    g_test_add_func("/test/module_and_domain", test_module_and_domain);
    g_test_add_func("/test/new_object", new_test_object);
    g_test_add_func("/test/object_get", test_object_get);
    g_test_add_func("/test/present", test_present);
    g_test_add_func("/test/object_set_val", test_object_set_val);
    g_test_add_func("/test/object_set_remote_val", test_object_set_remote_val);
    g_test_add_func("/test/object_get_remote_val", test_object_get_remote_val);
    g_test_add_func("/test/object_set_and_get_remote_val", test_object_set_and_get_remote_val);
    g_test_add_func("/test/object_set_val_variant", test_object_set_val_variant);
    g_test_add_func("/test/object_field", test_pm_object_field);
    g_test_add_func("/test/object_on_changed", test_object_on_changed);
    g_test_add_func("/test/object_on_property_changed_multi_times", test_object_on_property_changed_multi_times);
    g_test_add_func("/test/object_remove", test_lbo_changed);
    g_test_add_func("/test/object_nth", test_object_nth);
    g_test_add_func("/test/class_name", test_class_name);
    g_test_add_func("/test/object_bind", test_object_bind);
    // send signal test
    g_test_add_func("/test/call_signal_TestSignal", test_send_TestSignal);
    g_test_add_func("/test/call_signal_TestEmptyArgSignal", test_send_empty_arg_TestSignal);
    // dbus method test
    g_test_add_func("/test/method_failed", test_method_failed);
    g_test_add_func("/test/call_weak_method", test_call_weak_method);
    g_test_add_func("/test/call_method_SetBool", test_call_SetBool_method);
    g_test_add_func("/test/call_method_SetByte", test_call_SetByte_method);
    g_test_add_func("/test/call_method_SetInt16", test_call_SetInt16_method);
    g_test_add_func("/test/call_method_SetUint16", test_call_SetUint16_method);
    g_test_add_func("/test/call_method_SetInt32", test_call_SetInt32_method);
    g_test_add_func("/test/call_method_SetUint32", test_call_SetUint32_method);
    g_test_add_func("/test/call_method_SetInt64", test_call_SetInt64_method);
    g_test_add_func("/test/call_method_SetUint64", test_call_SetUint64_method);
    g_test_add_func("/test/call_method_SetDouble", test_call_SetDouble_method);
    g_test_add_func("/test/call_method_SetString", test_call_SetString_method);
    g_test_add_func("/test/call_method_SetObjectPath", test_call_SetObjectPath_method);
    g_test_add_func("/test/call_method_SetSignature", test_call_SetSignature_method);
    g_test_add_func("/test/call_method_SetInArgEmpty", test_call_SetInArgEmpty_method);
    g_test_add_func("/test/call_method_SetOutArgEmpty", test_call_SetOutArgEmpty_method);
    g_test_add_func("/test/call_set_arg_empty_method", test_call_set_arg_empty_method);
    g_test_add_func("/test/call_SetString_direct_replay", test_call_SetString_direct_replay);
    g_test_add_func("/test/property_changed_parameter_error", test_property_changed_parameter_error);
    g_test_add_func("/test/call_method_multi_thread_task", test_call_method_multi_thread);
    g_test_add_func("/test/log_init", test_log_init);
    #ifdef LB_CODEGEN_BE_5_2
    g_test_add_func("/test/validate/double", test_validate_double);
    g_test_add_func("/test/validate/int16", test_validate_int16);
    g_test_add_func("/test/validate/int32", test_validate_int32);
    g_test_add_func("/test/validate/int64", test_validate_int64);
    g_test_add_func("/test/validate/ssize", test_validate_ssize);
    g_test_add_func("/test/validate/byte", test_validate_byte);
    g_test_add_func("/test/validate/uint16", test_validate_uint16);
    g_test_add_func("/test/validate/uint32", test_validate_uint32);
    g_test_add_func("/test/validate/uint64", test_validate_uint64);
    g_test_add_func("/test/validate/size", test_validate_size);
    g_test_add_func("/test/validate/string", test_validate_string);
    g_test_add_func("/test/validate/object_path", test_validate_object_path);
    g_test_add_func("/test/validate/signature", test_validate_signature);
    g_test_add_func("/test/validate/enum", test_validate_enum);
    #endif
}

static void _test_start_100(void)
{
    log_info("Test service start.");
}

static void __attribute__((constructor(200))) _test_init_10(void)
{
    lb_module_register(_test_start_100, "_test_start_100", 100);
}

static void _test_start_0(void)
{
    lb_log_set_alert_abort(FALSE);
    log_info("Test service start.");
}

static void __attribute__((constructor(200))) _test_init_0(void)
{
    lb_module_register(_test_start_0, "_test_start_0", 0);
}