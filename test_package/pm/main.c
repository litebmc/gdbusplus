#include "lb_core.h"
#include "com.litebmc.test.service.h"

int main(int argc, char *argv[])
{
    lb_start("com.litebmc.Test");
    g_test_init(&argc, &argv, NULL);
    test_add_test_case();
    return g_test_run();
}