#include <math.h>
#include "lb_core.h"
#include "lb_internal.h"
#include "variant_helper.h"
#include "server/com.litebmc.Connector.h"
#include "client/org.freedesktop.DBus.h"
#include "server/com.litebmc.Formatter.h"
#include "server/com.litebmc.Expression.h"
#include "server/com.litebmc.Test.h"

#define CONN1 "TestConn1"
#define CONN2 "TestConn2"
#ifndef TEST_LOOP_CNT
#define TEST_LOOP_CNT 100
#endif

static void test_global_mds_loaded()
{
    Expression expr = Expression_get("/com/litebmc/global/ExprTest");
    g_assert_nonnull(expr);
    Expression_unref(&expr);
}

static void test_lb_get_bus()
{
    GDBusConnection *bus = lb_get_bus();
    g_assert_nonnull(bus);
}

static void test_lb_interfaces_origin()
{
    // 获取所有对象
    GSList *intfs = lb_interfaces(TRUE);
    g_assert_nonnull(intfs);
    g_slist_free(intfs);
    intfs = lb_interfaces(FALSE);
    g_assert_nonnull(intfs);
    g_slist_free(intfs);
}

static void test_lb_get_interface()
{
    // 未加载connector客户端接口，应该返回NULL
    const LBInterface *intf = lb_get_interface(TRUE, CONNECTOR->name);
    g_assert_null(intf);
    // 加载dbus客户端接口，应该返回NULL
    intf = lb_get_interface(TRUE, DBUS_CLI->name);
    g_assert_nonnull(intf);
    intf = lb_get_interface(FALSE, CONNECTOR->name);
    g_assert_nonnull(intf);

    intf = lb_get_interface(TRUE, "com.litebmc.unknown_interface");
    g_assert_null(intf);
    intf = lb_get_interface(FALSE, "com.litebmc.unknown_interface");
    g_assert_null(intf);
}

static void test_show_connector()
{
    GSList *list = Connector_list();
    g_assert_cmpint(g_slist_length(list), >=, 2);
    Connector_list_free(&list);
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    Connector conn1 = (Connector)handler1;
    g_assert_cmpint(conn1->position, ==, 1);
    g_assert_cmpint(conn1->present, ==, 0);
    g_assert_cmpint(conn1->physical, ==, 0);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector conn2 = (Connector)handler2;
    g_assert_cmpint(conn2->position, ==, 2);
    g_assert_cmpint(conn2->present, ==, 0);
    g_assert_cmpint(conn2->physical, ==, 1);
    g_assert_cmpuint(GPOINTER_TO_UINT(conn2->interface), !=, 0);
    g_assert_cmpstr(conn2->interface[0], ==, "intf1");
    g_assert_cmpstr(conn2->interface[1], ==, "intf2");
    g_assert_cmpuint(GPOINTER_TO_UINT(conn2->interface[2]), ==, 0);
    log_info("Run test case finish: %s", __FUNCTION__);
}

static void test_object_ref(void)
{
    Connector handler1 = Connector_new("/com/litebmc/test_object_ret", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    for (int i = 0; i < test_cnt; i++) {
        Connector_ref(handler1);
    }
    for (int i = 0; i < test_cnt; i++) {
        Connector_unref(&handler1);
        g_assert_nonnull(handler1);
    }
    Connector_unref(&handler1);
    g_assert_null(handler1);

    LBO *handler_cli = lbo_cli_new(DBUS_CLI, "com.litebmc.test_object_ret", "/com/litebmc/test_object_ret");
    g_assert_nonnull(handler_cli);
    lb_wait_internal_queue_empty(1);
    for (int i = 0; i < test_cnt; i++) {
        lbo_ref(handler_cli);
    }
    for (int i = 0; i < test_cnt; i++) {
        lbo_unref(&handler_cli);
        g_assert_nonnull(handler_cli);
    }
    lbo_unref(&handler_cli);
    g_assert_null(handler_cli);
}

static void test_object_get_ref(void)
{
    Connector handler1 = Connector_new("/com/litebmc/test_object_get_ref", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    for (int i = 0; i < test_cnt; i++) {
        Connector handler2 = Connector_get("/com/litebmc/test_object_get_ref");
        g_assert_nonnull(handler2);
    }
    for (int i = 0; i < test_cnt; i++) {
        Connector_unref(&handler1);
        g_assert_nonnull(handler1);
    }
    Connector_unref(&handler1);
    g_assert_null(handler1);
}

static void test_object_nth_ref(void)
{
    // 引用计数应该为1
    // handler2先创建，nth遍历时会先查询到handler2但不在位，测试nth不处理不在位对象
    Connector handler2 = Connector_new("/com/litebmc/test_object_nth_ref2", NULL);
    Connector handler1 = Connector_new("/com/litebmc/test_object_nth_ref", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    Connector nth_obj = NULL;
    int obj_idx = 0;
    // 正向查找
    for (obj_idx = 0; ; obj_idx++) {
        nth_obj = Connector_nth(obj_idx);
        if (!nth_obj) {
            break;
        }
        if (nth_obj == handler2) {
            break;
        }
        Connector_unref(&nth_obj);
    }
    // handler2先创建，nth遍历时会先查询到handler2但不在位，测试nth不处理不在位对象
    g_assert_null(nth_obj);
    // 正向查找
    for (obj_idx = 0; ; obj_idx++) {
        nth_obj = Connector_nth(obj_idx);
        if (!nth_obj) {
            break;
        }
        if (nth_obj == handler1) {
            break;
        }
        Connector_unref(&nth_obj);
    }
    // nth_obj一定不为空，且lbo_nth会加引用计数，此时计数应该2
    g_assert_nonnull(nth_obj);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    // 引用计数加test_cnt再减test_cnt
    for (int i = 0; i < test_cnt; i++) {
        nth_obj = Connector_nth(obj_idx);
        g_assert_nonnull(nth_obj);
    }
    for (int i = 0; i < test_cnt; i++) {
        Connector_unref(&handler1);
        g_assert_nonnull(handler1);
    }
    // nth_obj引用计数应该为2（lbo_nth会加引用计数）
    Connector_unref(&handler1);
    g_assert_nonnull(handler1);
    // 减引用计数两次后应该为空
    Connector_unref(&handler1);
    g_assert_null(handler1);
    // handler2创建后立即释放，指针置空
    Connector_unref(&handler2);
    g_assert_null(handler2);
}

static void test_object_nth_dec_ref(void)
{
    // 引用计数应该为1
    Connector handler1 = Connector_new("/com/litebmc/test_object_nth_dec_ref", NULL);
    // handler2后创建，nth反向遍历时会先查询到handler2但不在位，测试nth不处理不在位对象
    Connector handler2 = Connector_new("/com/litebmc/test_object_nth_dec_ref2", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    Connector nth_obj = NULL;
    int obj_idx = 0;
    for (obj_idx = -1; ; obj_idx--) {
        nth_obj = Connector_nth(obj_idx);
        if (!nth_obj) {
            break;
        }
        if (nth_obj == handler2) {
            break;
        }
        Connector_unref(&nth_obj);
    }
    g_assert_null(nth_obj);
    for (obj_idx = -1; ; obj_idx--) {
        nth_obj = Connector_nth(obj_idx);
        if (!nth_obj) {
            break;
        }
        if (nth_obj == handler1) {
            break;
        }
        Connector_unref(&nth_obj);
    }
    // nth_obj一定不为空，且lbo_nth会加引用计数，此时计数应该2
    g_assert_nonnull(nth_obj);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    // 引用计数加test_cnt再减test_cnt
    for (int i = 0; i < test_cnt; i++) {
        nth_obj = Connector_nth(obj_idx);
        g_assert_nonnull(nth_obj);
    }
    for (int i = 0; i < test_cnt; i++) {
        Connector_unref(&handler1);
        g_assert_nonnull(handler1);
    }
    // nth_obj引用计数应该为2（lbo_nth会加引用计数）
    Connector_unref(&handler1);
    g_assert_nonnull(handler1);
    // 减引用计数两次后应该为空
    Connector_unref(&handler1);
    g_assert_null(handler1);
}

static void test_object_new_ref(void)
{
    Connector handler1 = Connector_new("/com/litebmc/test_object_new_ref", NULL);
    g_assert_nonnull(handler1);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    for (int i = 0; i < test_cnt; i++) {
        Connector handler2 = Connector_new("/com/litebmc/test_object_new_ref", NULL);
        g_assert_nonnull(handler2);
        g_assert(handler1 == handler2);
    }
    for (int i = 0; i < test_cnt; i++) {
        Connector_unref(&handler1);
        g_assert_nonnull(handler1);
    }
    Connector_unref(&handler1);
    g_assert_null(handler1);

    LBO *handler_cli = lbo_cli_new(DBUS_CLI, "com.litebmc.test_object_ret", "/com/litebmc/test_object_ret");
    g_assert_nonnull(handler_cli);
    for (int i = 0; i < test_cnt; i++) {
        LBO *handler2 = lbo_cli_new(DBUS_CLI, "com.litebmc.test_object_ret", "/com/litebmc/test_object_ret");
        g_assert_nonnull(handler2);
        g_assert(handler_cli == handler2);
    }
    for (int i = 0; i < test_cnt; i++) {
        lbo_unref(&handler_cli);
        g_assert_nonnull(handler_cli);
    }
    lbo_unref(&handler_cli);
    g_assert_null(handler_cli);
}

static void test_object_list_ref1(void)
{
    Connector handler1 = Connector_new("/com/litebmc/test_object_new_ref", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    int test_cnt = rand() % TEST_LOOP_CNT + TEST_LOOP_CNT;
    for (int i = 0; i < test_cnt; i++) {
        cleanup_Connector_list GSList *objects = Connector_list();
        g_assert_nonnull(objects);
    }
    Connector_unref(&handler1);
    g_assert_null(handler1);
}

static void test_object_list_ref2(void)
{
    Connector handler1 = Connector_new("/com/litebmc/test_object_new_ref", NULL);
    g_assert_nonnull(handler1);
    Connector_present_set(handler1, TRUE);
    lb_wait_internal_queue_empty(1);
    GSList *objects = Connector_list();
    g_assert_nonnull(objects);
    // objects持有对象，此时减引用计数并不会导致句柄置空
    Connector_unref(&handler1);
    g_assert_nonnull(handler1);
    // 释放对象链表，对象被释放
    Connector_list_free(&objects);
    // 再次查询时对外不存在
    handler1 = Connector_get("/com/litebmc/test_object_new_ref");
    g_assert_null(handler1);
}

static void test_connector_load(void)
{
    GSList *items;
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    for (int i = 0; i < 4; i++) {
        Connector_set_present(handler1, i % 2);
        Connector_set_present(handler2, i % 2);
        lb_wait_internal_queue_empty(1);
        items = Connector_list();
        g_assert_cmpint(g_slist_length(items), ==, (5 + (i % 2) * 4));
        Connector_list_free(&items);
        g_assert_null(items);
        items = Test_list();
        g_assert_cmpint(g_slist_length(items), ==, (4 + (i % 2) * 4));
        Test_list_free(&items);
        g_assert_null(items);
        // 在位时检测test对象的状态
        if (i % 2) {
            Test test = Test_get("test_02");
            // 全局对象
            g_assert_cmpstr(test->remote_obj, ==, "TestCopy1");
            // 全局对象
            g_assert_cmpstr(test->remote_obj_array[0], ==, "TestOrigin");
            // 上一级对象
            g_assert_cmpstr(test->remote_obj_array[1], ==, "TestCopy1");
            // 同级对象
            g_assert_cmpstr(test->remote_obj_array[2], ==, "TestCopy2_02");
            // 同级对象
            g_assert_cmpstr(test->remote_obj_array[3], ==, "/com/litebmc/Connector11_02");
            g_assert_cmpuint(GPOINTER_TO_UINT(test->remote_obj_array[4]), ==, 0);
            Test_unref(&test);
            test = Test_get("test");
            // 全局对象
            g_assert_cmpstr(test->remote_obj, ==, "TestCopy1");
            // 全局对象
            g_assert_cmpstr(test->remote_obj_array[0], ==, "TestOrigin");
            // 上一级对象
            g_assert_cmpstr(test->remote_obj_array[1], ==, "TestCopy1");
            // 同级对象
            g_assert_cmpstr(test->remote_obj_array[2], ==, "TestCopy2");
            // 同级对象
            g_assert_cmpstr(test->remote_obj_array[3], ==, "/com/litebmc/Connector11");
            g_assert_cmpuint(GPOINTER_TO_UINT(test->remote_obj_array[4]), ==, 0);
            Test_unref(&test);
        }
    }
    items = Connector_list();
    g_assert_cmpint(g_slist_length(items), ==, 9);
    for (GSList *item = items; item; item = item->next) {
        log_debug("connector: %s", lbo_name(item->data));
    }
    Connector_list_free(&items);
    g_assert_null(items);

    items = Test_list();
    for (GSList *item = items; item; item = item->next) {
        log_debug("test: %s", lbo_name(item->data));
    }
    Test_list_free(&items);
    g_assert_null(items);
}

static void test_connector_load_fuzz()
{
    GSList *items;
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    for (int i = 0; i < 1000; i++) {
        Connector_set_present(handler1, i % 2);
        Connector_set_present(handler2, i % 2);
        usleep(2000);
    }
    lb_wait_internal_queue_empty(1);;
    items = Connector_list();
    for (GSList *item = items; item; item = item->next)
        log_info("Conn object: %s", lbo_name(item->data));
    g_assert_cmpint(g_slist_length(items), ==, 9);
    log_info("Run test case finish: %s", __FUNCTION__);
    Connector_list_free(&items);
    items = Test_list();
    for (GSList *item = items; item; item = item->next)
        log_info("Test object: %s", lbo_name(item->data));
    g_assert_cmpint(g_slist_length(items), ==, 8);
    log_info("Run test case finish: %s", __FUNCTION__);
    Test_list_free(&items);
}

static void test_formatter()
{
    char format[] = "x$a$b$c$d$e";
    char value[] = "xabcd$e";
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector_set_present(handler1, TRUE);
    Connector_set_present(handler2, TRUE);
    lb_wait_internal_queue_empty(1);;
    cleanup_Formatter_list GSList *items = Formatter_list();
    g_assert_cmpint(g_slist_length(items), ==, 3);
    for (GSList *item = items; item; item = item->next) {
        Formatter fmt = (Formatter)item->data;
        log_info("Test formatter object %s", lbo_name(fmt));
        // 属性值由xml配置
        g_assert_cmpstr(fmt->a, ==, "a");
        g_assert_cmpstr(fmt->b, ==, "b");
        g_assert_cmpstr(fmt->c, ==, "c");
        g_assert_cmpstr(fmt->d, ==, "d");
        g_assert_cmpstr(fmt->value, ==, "abcd");
        Formatter_set_format(fmt, "$a$a$a$a");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "aaaa");
        Formatter_set_format(fmt, "$a$a$a$c");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "aaac");
        Formatter_set_format(fmt, "$a$a$a$e");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "aaa$e");
        Formatter_set_format(fmt, "\\$a\\$b\\$c\\$d");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "$a$b$c$d");
        Formatter_set_format(fmt, "");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "");
        Formatter_set_format(fmt, "    ");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpstr(fmt->value, ==, "    ");
        for (int i = 1; i < 128; i++) {
            format[0] = i;
            value[0] = i;
            Formatter_set_format(fmt, format);
            lb_wait_internal_queue_empty(1);;
            if (i != '\\') {
                g_assert_cmpstr(fmt->value, ==, value);
            } else {
                g_assert_cmpstr(fmt->value, ==, "$abcd$e");
            }
        }
    }
}

#define _strcmp_until_equal(obj, src, dst, timeout_s)                                                                                      \
    for (int j = 0; j < (timeout_s) * 100; j++) {                                                                                          \
        lbo_lock(obj);                                                                                                              \
        if (g_strcmp0(src, dst) == 0) {                                                                                                    \
            lbo_unlock(obj);                                                                                                        \
            break;                                                                                                                         \
        }                                                                                                                                  \
        lbo_unlock(obj);                                                                                                            \
        usleep(10000);                                                                                                                     \
    }

static void test_formatter_fuzz()
{
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector_set_present(handler1, TRUE);
    Connector_set_present(handler2, TRUE);
    lb_wait_internal_queue_empty(1);
    cleanup_Formatter_list GSList *items = Formatter_list();
    // 当连接器在位时有2个字符串格式化对象
    g_assert_cmpint(g_slist_length(items), ==, 3);
    for (GSList *item = items; item; item = item->next) {
        Formatter fmt = (Formatter)item->data;
        log_info("Test formatter object %s", lbo_name(fmt));
        g_assert_cmpstr(fmt->a, ==, "a");
        g_assert_cmpstr(fmt->b, ==, "b");
        g_assert_cmpstr(fmt->c, ==, "c");
        g_assert_cmpstr(fmt->d, ==, "d");
        Formatter_set_format(fmt, "$a$b$c$d$a$b$c$d\\$a\\$b\\$c\\$d\\$e");
        const gchar *str_val = "abcdabcd$a$b$c$d$e";
        _strcmp_until_equal(fmt, fmt->value, str_val, 10);
        g_assert_cmpstr(fmt->value, ==, str_val);
        for (int i = 0; i < TEST_LOOP_CNT; i++) {
            cleanup_gfree gchar *str_ina = lb_printf("%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_inb = lb_printf("%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_inc = lb_printf("%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_ind = lb_printf("%d", rand() - RAND_MAX / 2);
            Formatter_set_a(fmt, str_ina);
            Formatter_set_b(fmt, str_inb);
            Formatter_set_c(fmt, str_inc);
            Formatter_set_d(fmt, str_ind);
            cleanup_gfree gchar *str_out = lb_printf("%s%s%s%s%s%s%s%s$a$b$c$d$e", str_ina, str_inb, str_inc, str_ind, str_ina, str_inb, str_inc, str_ind);
            _strcmp_until_equal(fmt, fmt->value, str_out, 10);
            g_assert_cmpstr(fmt->value, ==, str_out);
        }
        for (int i = 0; i < TEST_LOOP_CNT; i++) {
            cleanup_gfree gchar *str_ina = lb_printf("$a$b$c$d$e%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_inb = lb_printf("$a$b$c$d$e%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_inc = lb_printf("$a$b$c$d$e%d", rand() - RAND_MAX / 2);
            cleanup_gfree gchar *str_ind = lb_printf("$a$b$c$d$e%d", rand() - RAND_MAX / 2);
            Formatter_set_a(fmt, str_ina);
            Formatter_set_b(fmt, str_inb);
            Formatter_set_c(fmt, str_inc);
            Formatter_set_d(fmt, str_ind);
            cleanup_gfree gchar *str_out = lb_printf("%s%s%s%s%s%s%s%s$a$b$c$d$e", str_ina, str_inb, str_inc, str_ind, str_ina, str_inb, str_inc, str_ind);
            _strcmp_until_equal(fmt, fmt->value, str_out, 10);
            g_assert_cmpstr(fmt->value, ==, str_out);
        }
    }
}

static void test_expression()
{
    double value;
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector_set_present(handler1, TRUE);
    Connector_set_present(handler2, TRUE);
    lb_wait_internal_queue_empty(1);;
    cleanup_Expression_list GSList *items = Expression_list();
    g_assert_cmpint(g_slist_length(items), ==, 4);
    for (GSList *item = items; item; item = item->next) {
        Expression exp = (Expression)item->data;
        log_info("Test expression object %s", lbo_name(exp));
        Expression_set_a(exp, (rand() - RAND_MAX / 2) / 3.1415);
        Expression_set_b(exp, (rand() - RAND_MAX / 2) / 3.1415);
        Expression_set_c(exp, (rand() - RAND_MAX / 2) / 3.1415);
        Expression_set_d(exp, (rand() - RAND_MAX / 2) / 3.1415);
        Expression_set_formula(exp, "a + b * c / d");
        value = exp->a + exp->b * exp->c / exp->d;
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(exp->value, ==, value);
        Expression_set_formula(exp, "a * a * a * a");
        value = exp->a * exp->a * exp->a * exp->a;
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(exp->value, ==, value);
        // 触发错误
        Expression_set_formula(exp, "a + a + e");
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(exp->value, ==, 0);
        // 无错误
        Expression_set_formula(exp, "a + b + c");
        value = exp->a + exp->b + exp->c;
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(exp->value, ==, value);
        Expression_set_a(exp, (rand() - RAND_MAX / 2));
        Expression_set_b(exp, (rand() - RAND_MAX / 2));
        Expression_set_c(exp, (rand() - RAND_MAX / 2));
        Expression_set_d(exp, (rand() - RAND_MAX / 2));
        Expression_set_formula(exp, "(a > b) ? c : d");
        lb_wait_internal_queue_empty(1);;
        if (exp->a > exp->b)
            g_assert_cmpfloat(exp->value, ==, exp->c);
        else
            g_assert_cmpfloat(exp->value, ==, exp->d);
    }
}

static gdouble op_add(gdouble a, gdouble b)
{
    return a + b;
}
static gdouble op_del(gdouble a, gdouble b)
{
    return a - b;
}
static gdouble op_mul(gdouble a, gdouble b)
{
    return a * b;
}
static gdouble op_div(gdouble a, gdouble b)
{
    return a / b;
}

typedef gdouble (*ops_func)(gdouble a, gdouble b);

static void test_expression_double_fuzz()
{
    char ops_str[] = "+-*/^";
    ops_func double_ops[] = {op_add, op_del, op_mul, op_div, pow};
    double value;
    int op1, op2, op3;
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector_set_present(handler1, TRUE);
    Connector_set_present(handler2, TRUE);
    lb_wait_internal_queue_empty(1);;
    cleanup_Expression_list GSList *items = Expression_list();
    g_assert_cmpint(g_slist_length(items), ==, 4);
    for (GSList *item = items; item; item = item->next) {
        Expression exp = (Expression)item->data;
        log_info("Test expression object %s", lbo_name(exp));
        for (int i = 0; i < TEST_LOOP_CNT; i++) {
            Expression_set_a(exp, (rand() - RAND_MAX / 2) / 3.1415);
            Expression_set_b(exp, (rand() - RAND_MAX / 2) / 3.1415);
            Expression_set_c(exp, (rand() - RAND_MAX / 2) / 3.1415);
            Expression_set_d(exp, (rand() - RAND_MAX / 2) / 3.1415);
            op1 = rand() % 5;
            op2 = rand() % 5;
            op3 = rand() % 5;
            value = double_ops[op2](double_ops[op1](exp->a, exp->b), double_ops[op3](exp->c, exp->d));
            gchar *formula = lb_printf("(a %c b) %c (c %c d)", ops_str[op1], ops_str[op2], ops_str[op3]);
            Expression_set_formula(exp, formula);
            g_free(formula);
            lb_wait_internal_queue_empty(1);;
            if (isnan(value) ^ isnan(exp->value))
                g_assert_cmpfloat(1, ==, 0);
            if (!isnan(value))
                g_assert_cmpfloat(exp->value, ==, value);
        }
    }
}

static void test_reference_property()
{
    gchar *str_v[4];
    gdouble rand_arr[3];
    Test origin = Test_get("TestOrigin");
    g_assert_cmpuint(GPOINTER_TO_UINT(origin->remote_obj), ==, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(origin->remote_obj_array), ==, 0);
    Test copy;
    for (gint i = 0; i < TEST_LOOP_CNT; i++) {
        // 测试浮点数
        rand_arr[0] = (rand() - RAND_MAX / 2) / 3.1415926;
        copy = Test_get("TestCopy1");
        g_assert_cmpuint(GPOINTER_TO_UINT(copy), !=, 0);
        Test_set_d(origin, rand_arr[0]);
        if ((((gint64)rand_arr[0]) / 1.0) == rand_arr[0])
            str_v[0] = lb_printf("%f", rand_arr[0]);
        else
            str_v[0] = lb_printf("%.6f", rand_arr[0]);
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(origin->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->b, ==, (rand_arr[0] ? TRUE : FALSE));
        g_assert_cmpint(copy->y, ==, (guint8)rand_arr[0]);
        g_assert_cmpint(copy->n, ==, (gint16)rand_arr[0]);
        g_assert_cmpint(copy->i, ==, (gint32)rand_arr[0]);
        g_assert_cmpint(copy->x, ==, (gint64)rand_arr[0]);
        g_assert_cmpint(copy->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->q, ==, (guint16)rand_arr[0]);
        g_assert_cmpint(copy->u, ==, (guint32)rand_arr[0]);
        g_assert_cmpint(copy->t, ==, (guint64)rand_arr[0]);
        g_assert_cmpstr(copy->s, ==, str_v[0]);
        g_assert_cmpint(copy->n_ab, ==, 1);
        g_assert_cmpint(copy->n_ay, ==, 1);
        g_assert_cmpint(copy->n_an, ==, 1);
        g_assert_cmpint(copy->n_ai, ==, 1);
        g_assert_cmpint(copy->n_ax, ==, 1);
        g_assert_cmpint(copy->n_ad, ==, 1);
        g_assert_cmpint(copy->n_aq, ==, 1);
        g_assert_cmpint(copy->n_au, ==, 1);
        g_assert_cmpint(copy->n_at, ==, 1);
        g_assert_cmpint(copy->ab[0], ==, (rand_arr[0] ? TRUE : FALSE));
        g_assert_cmpint(copy->ay[0], ==, (guint8)rand_arr[0]);
        g_assert_cmpint(copy->an[0], ==, (gint16)rand_arr[0]);
        g_assert_cmpint(copy->ai[0], ==, (gint32)rand_arr[0]);
        g_assert_cmpint(copy->ax[0], ==, (gint64)rand_arr[0]);
        g_assert_cmpint(copy->ad[0], ==, rand_arr[0]);
        g_assert_cmpint(copy->aq[0], ==, (guint16)rand_arr[0]);
        g_assert_cmpint(copy->au[0], ==, (guint32)rand_arr[0]);
        g_assert_cmpint(copy->at[0], ==, (guint64)rand_arr[0]);
        g_assert_cmpuint(GPOINTER_TO_UINT(copy->as), !=, 0);
        g_assert_cmpstr(copy->as[0], ==, str_v[0]);
        g_assert_cmpstr(copy->remote_obj, ==, "TestCopy2");
        g_assert_cmpstr(copy->remote_obj_array[0], ==, "TestOrigin");
        g_assert_cmpstr(copy->remote_obj_array[1], ==, "TestCopy1");
        g_assert_cmpstr(copy->remote_obj_array[2], ==, "TestCopy2");
        g_assert_cmpstr(copy->remote_obj_array[3], ==, "/com/litebmc/Connector1");
        g_free(str_v[0]);

        // 测试整数
        rand_arr[0] = (rand() - RAND_MAX / 2);
        Test_unref(&copy);
        copy = Test_get("TestCopy1");
        g_assert_cmpuint(GPOINTER_TO_UINT(copy), !=, 0);
        Test_set_d(origin, rand_arr[0]);
        if ((((gint64)rand_arr[0]) / 1.0) == rand_arr[0])
            str_v[0] = lb_printf("%f", rand_arr[0]);
        else
            str_v[0] = lb_printf("%.6f", rand_arr[0]);
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(origin->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->b, ==, (rand_arr[0] ? TRUE : FALSE));
        g_assert_cmpint(copy->y, ==, (guint8)rand_arr[0]);
        g_assert_cmpint(copy->n, ==, (gint16)rand_arr[0]);
        g_assert_cmpint(copy->i, ==, (gint32)rand_arr[0]);
        g_assert_cmpint(copy->x, ==, (gint64)rand_arr[0]);
        g_assert_cmpint(copy->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->q, ==, (guint16)rand_arr[0]);
        g_assert_cmpint(copy->u, ==, (guint32)rand_arr[0]);
        g_assert_cmpint(copy->t, ==, (guint64)rand_arr[0]);
        g_assert_cmpstr(copy->s, ==, str_v[0]);
        g_assert_cmpint(copy->n_ab, ==, 1);
        g_assert_cmpint(copy->n_ay, ==, 1);
        g_assert_cmpint(copy->n_an, ==, 1);
        g_assert_cmpint(copy->n_ai, ==, 1);
        g_assert_cmpint(copy->n_ax, ==, 1);
        g_assert_cmpint(copy->n_ad, ==, 1);
        g_assert_cmpint(copy->n_aq, ==, 1);
        g_assert_cmpint(copy->n_au, ==, 1);
        g_assert_cmpint(copy->n_at, ==, 1);
        g_assert_cmpint(copy->ab[0], ==, (rand_arr[0] ? TRUE : FALSE));
        g_assert_cmpint(copy->ay[0], ==, (guint8)rand_arr[0]);
        g_assert_cmpint(copy->an[0], ==, (gint16)rand_arr[0]);
        g_assert_cmpint(copy->ai[0], ==, (gint32)rand_arr[0]);
        g_assert_cmpint(copy->ax[0], ==, (gint64)rand_arr[0]);
        g_assert_cmpint(copy->ad[0], ==, rand_arr[0]);
        g_assert_cmpint(copy->aq[0], ==, (guint16)rand_arr[0]);
        g_assert_cmpint(copy->au[0], ==, (guint32)rand_arr[0]);
        g_assert_cmpint(copy->at[0], ==, (guint64)rand_arr[0]);
        g_assert_cmpuint(GPOINTER_TO_UINT(copy->as), !=, 0);
        g_assert_cmpstr(copy->as[0], ==, str_v[0]);
        g_assert_cmpuint(GPOINTER_TO_UINT(copy->as[1]), ==, 0);
        g_free(str_v[0]);
        g_assert_cmpstr(copy->remote_obj, ==, "TestCopy2");
        g_assert_cmpstr(copy->remote_obj_array[0], ==, "TestOrigin");
        g_assert_cmpstr(copy->remote_obj_array[1], ==, "TestCopy1");
        g_assert_cmpstr(copy->remote_obj_array[2], ==, "TestCopy2");
        g_assert_cmpstr(copy->remote_obj_array[3], ==, "/com/litebmc/Connector1");
        g_assert_cmpuint(GPOINTER_TO_UINT(copy->remote_obj_array[4]), ==, 0);

        // 测试数组设置
        for (int i = 0; i < 3; i++) {
            rand_arr[i] = (rand() - RAND_MAX / 2);
            if ((((gint64)rand_arr[i]) / 1.0) == rand_arr[i])
                str_v[i] = lb_printf("%f", rand_arr[i]);
            else
                str_v[i] = lb_printf("%.6f", rand_arr[i]);
        }
        Test_unref(&copy);
        copy = Test_get("TestCopy2");
        g_assert_cmpuint(GPOINTER_TO_UINT(copy), !=, 0);
        Test_set_ad(origin, 3, rand_arr);
        lb_wait_internal_queue_empty(1);;
        g_assert_cmpfloat(origin->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->b, ==, (rand_arr[0] ? TRUE : FALSE));
        g_assert_cmpint(copy->y, ==, (guint8)rand_arr[0]);
        g_assert_cmpint(copy->n, ==, (gint16)rand_arr[0]);
        g_assert_cmpint(copy->i, ==, (gint32)rand_arr[0]);
        g_assert_cmpint(copy->x, ==, (gint64)rand_arr[0]);
        g_assert_cmpint(copy->d, ==, rand_arr[0]);
        g_assert_cmpint(copy->q, ==, (guint16)rand_arr[0]);
        g_assert_cmpint(copy->u, ==, (guint32)rand_arr[0]);
        g_assert_cmpint(copy->t, ==, (guint64)rand_arr[0]);
        g_assert_cmpstr(copy->s, ==, str_v[0]);
        g_assert_cmpint(copy->n_ab, ==, 3);
        g_assert_cmpint(copy->n_ay, ==, 3);
        g_assert_cmpint(copy->n_an, ==, 3);
        g_assert_cmpint(copy->n_ai, ==, 3);
        g_assert_cmpint(copy->n_ax, ==, 3);
        g_assert_cmpint(copy->n_ad, ==, 3);
        g_assert_cmpint(copy->n_aq, ==, 3);
        g_assert_cmpint(copy->n_au, ==, 3);
        g_assert_cmpint(copy->n_at, ==, 3);
        g_assert_cmpstr(copy->remote_obj, ==, "TestCopy1");
        g_assert_cmpstr(copy->remote_obj_array[0], ==, "TestOrigin");
        g_assert_cmpstr(copy->remote_obj_array[1], ==, "TestCopy1");
        g_assert_cmpstr(copy->remote_obj_array[2], ==, "TestCopy2");
        g_assert_cmpstr(copy->remote_obj_array[3], ==, "/com/litebmc/Connector2");
        g_assert_cmpuint(GPOINTER_TO_UINT(copy->remote_obj_array[4]), ==, 0);


        g_assert_cmpuint(GPOINTER_TO_UINT(copy->as), !=, 0);
        for (int i = 0; i < 3; i++) {
            g_assert_cmpint(copy->ab[i], ==, (rand_arr[i] ? TRUE : FALSE));
            g_assert_cmpint(copy->ay[i], ==, (guint8)rand_arr[i]);
            g_assert_cmpint(copy->an[i], ==, (gint16)rand_arr[i]);
            g_assert_cmpint(copy->ai[i], ==, (gint32)rand_arr[i]);
            g_assert_cmpint(copy->ax[i], ==, (gint64)rand_arr[i]);
            g_assert_cmpint(copy->ad[i], ==, rand_arr[i]);
            g_assert_cmpint(copy->aq[i], ==, (guint16)rand_arr[i]);
            g_assert_cmpint(copy->au[i], ==, (guint32)rand_arr[i]);
            g_assert_cmpint(copy->at[i], ==, (guint64)rand_arr[i]);
            g_assert_cmpstr(copy->as[i], ==, str_v[i]);
            g_free(str_v[i]);
        }
        Test_unref(&copy);
    }
    Test_unref(&origin);
}

static void test_reference_object()
{
    Connector handler = NULL;
    cleanup_Connector Connector handler1 = Connector_get(CONN1);
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler1), !=, 0);
    g_assert_cmpuint(GPOINTER_TO_UINT(handler2), !=, 0);
    Connector_set_present(handler1, 1);
    Connector_set_present(handler2, 1);
    lb_wait_internal_queue_empty(1);;
    cleanup_Connector_list GSList *items = Connector_list();
    log_info("items:  %d", g_slist_length(items));
    for (GSList *item = items; item; item = item->next) {
        Connector conn = (Connector)item->data;
        log_debug("Connector: %s", lbo_name(conn));
        for (int i = 0; conn->interface && conn->interface[i]; i++) {
            log_debug("Interface: %s", conn->interface[i]);
        }
    }
    g_assert_null(((Connector)handler1)->interface);
    const gchar *strv2[] = {"intf1", "intf2", NULL};
    g_assert_cmpint(g_strv_equal((const char *const *)((Connector)handler2)->interface, strv2), ==, TRUE);
    handler = Connector_get("TestConnSub_02");
    const gchar *strv3[] = {"intf1_02", "intf2_02", NULL};
    g_assert_cmpint(g_strv_equal((const char *const *)((Connector)handler)->interface, strv3), ==, TRUE);
    Connector_unref(&handler);
    handler = Connector_get("TestConnSub1_0202");
    const gchar *strv4[] = {"intf1_0202", "intf2_0202", NULL};
    g_assert_cmpint(g_strv_equal((const char *const *)((Connector)handler)->interface, strv4), ==, TRUE);
    Connector_unref(&handler);
    handler = Connector_get("TestConnSub");
    const gchar *strv5[] = {"intf1", "intf2", NULL};
    g_assert_cmpint(g_strv_equal((const char *const *)((Connector)handler2)->interface, strv5), ==, TRUE);
    Connector_unref(&handler);
    handler = Connector_get("TestConnSub1_02");
    const gchar *strv6[] = {"intf1_02", "intf2_02", NULL};
    g_assert_cmpint(g_strv_equal((const char *const *)((Connector)handler)->interface, strv6), ==, TRUE);
    Connector_unref(&handler);
}

static void test_persistence()
{
    cleanup_Connector Connector handler2 = Connector_get(CONN2);
    Connector_set_present(handler2, 1);
    lb_wait_internal_queue_empty(1);;
    cleanup_Test_list GSList *items = Test_list();
    for (GSList *item = items; item; item = item->next) {
        LBO *handler = item->data;
        log_debug("%s: %s", TEST->name, lbo_name(handler));
    }
    Test t = Test_get("TestPersistence_02");
    g_assert_cmpuint(GPOINTER_TO_UINT(t), !=, 0);

    g_assert_cmpint(t->b, ==, TRUE);
    g_assert_cmpint(t->n_ab, ==, 2);
    g_assert_cmpint(t->ab[0], ==, TRUE);
    g_assert_cmpint(t->ab[1], ==, FALSE);
    g_assert_cmpstr(t->s, ==, "9");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as), !=, 0);
    g_assert_cmpstr(t->as[0], ==, "10");
    g_assert_cmpstr(t->as[1], ==, "10");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as[2]), ==, 0);
    Test_set_b(t, FALSE);
    Test_set_b(t, TRUE);
    g_assert_cmpint(t->b, ==, TRUE);
    gboolean ab[] = {FALSE, TRUE, FALSE};
    Test_set_ab(t, 3, ab);
    g_assert_cmpint(t->n_ab, ==, 3);
    g_assert_cmpint(t->ab[0], ==, FALSE);
    g_assert_cmpint(t->ab[1], ==, TRUE);
    g_assert_cmpint(t->ab[2], ==, FALSE);
    Test_set_s(t, "11");
    g_assert_cmpstr(t->s, ==, "11");
    gchar *as[] = {"11", "11", NULL};
    Test_set_as(t, (char **)as);
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as), !=, 0);
    g_assert_cmpstr(t->as[0], ==, "11");
    g_assert_cmpstr(t->as[1], ==, "11");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as[2]), ==, 0);
    Connector_set_present(handler2, 0);
    lb_wait_internal_queue_empty(1);;
    Connector_set_present(handler2, 1);
    lb_wait_internal_queue_empty(1);;
    t = Test_get("TestPersistence_02");
    g_assert_cmpint(t->b, ==, TRUE);
    g_assert_cmpint(t->n_ab, ==, 3);
    g_assert_cmpint(t->ab[0], ==, FALSE);
    g_assert_cmpint(t->ab[1], ==, TRUE);
    g_assert_cmpint(t->ab[2], ==, FALSE);
    g_assert_cmpstr(t->s, ==, "11");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as), !=, 0);
    g_assert_cmpstr(t->as[0], ==, "11");
    g_assert_cmpstr(t->as[1], ==, "11");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as[2]), ==, 0);

    Test_unref(&t);
    // 未修改值
    t = Test_get("TestPersistence");
    g_assert_nonnull(t);
    g_assert_cmpint(t->b, ==, TRUE);
    g_assert_cmpint(t->n_ab, ==, 2);
    g_assert_cmpint(t->ab[0], ==, TRUE);
    g_assert_cmpint(t->ab[1], ==, FALSE);
    g_assert_cmpstr(t->s, ==, "9");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as), !=, 0);
    g_assert_cmpstr(t->as[0], ==, "10");
    g_assert_cmpstr(t->as[1], ==, "10");
    g_assert_cmpuint(GPOINTER_TO_UINT(t->as[2]), ==, 0);
    Test_unref(&t);
}

static gint per_power_off_key1 = 0;
static gint per_power_off_key3 = 0;

static PER_ITEM per_items[] = {
    {
        .group = "_",
        .key = "key1",
        .data = (guint8 *)&per_power_off_key1,
        .length = sizeof(per_power_off_key1),
        .type = PER_SAVE,
        .is_static = 1,
    },
    {
        .group = "_",
        .key = "key2",
        .length = 0,
        .type = PER_REBOOT,
        .is_static = 0,
    },
    {
        .group = "_",
        .key = "key1",
        .data = (guint8 *)&per_power_off_key3,
        .length = sizeof(per_power_off_key3),
        .type = PER_SAVE,
        .is_static = 1,
    },
    {
        .group = "_",
        .key = "key2",
        .length = 0,
        .type = PER_REBOOT,
        .is_static = 0,
    },
    {
        .group = NULL,
        .key = "key2",
        .length = 0,
        .type = PER_REBOOT,
        .is_static = 0,
    },
};

static void test_persistence_new()
{
    cleanup_gfree gchar *reboot_temp_file = get_rootfs_path("/opt/data/persistence/reboot/com.litebmc.Test/temp.ini");
    cleanup_gfree gchar *reboot_data_file = get_rootfs_path("/opt/data/persistence/reboot/com.litebmc.Test/data.ini");

    gchar data[16] = {0};
    gint rand_sum = rand();
    gint ret = per_load(per_items);
    log_info("load per_power_off_key1 ret: %d", ret);
    if (ret == 0) {
        log_info("per_power_off_key1 = %d", per_power_off_key1);
    }
    ret = per_load(per_items + 1);
    log_info("load per_power_off_key2 ret: %d", ret);
    if (ret == 0) {
        log_info("per_power_off_key2 = %s", (gchar *)per_items[1].data);
    }

    per_power_off_key1 = rand_sum;
    ret = per_save(per_items);
    g_assert_cmpint(ret, ==, 0);
    log_info("load per_power_off_key1 ret: %d", ret);
    snprintf(data, sizeof(data), "%d", rand());
    per_items[1].data = (guint8 *)data;
    per_items[1].length = strlen(data) + 1;
    per_items[1].is_static = 1;
    ret = per_save(per_items + 1);
    g_assert_cmpint(ret, ==, 0);
    // 重复保存，未变更时不写文件
    ret = per_save(per_items + 1);
    g_assert_cmpint(ret, ==, 0);
    // 内容发生变更但文件权限不正确时
    snprintf(data, sizeof(data), "%d", rand());
    per_items[1].data = (guint8 *)data;
    per_items[1].length = strlen(data) + 1;
    // 构造临时文件并设置为只写场景, 期望：per_save保存失败
    FILE *fp = fopen(reboot_temp_file, "w+");
    fclose(fp);
    (void)chmod(reboot_temp_file, S_IRUSR);
    ret = per_save(per_items + 1);
    // root用户屏蔽新建持久化文件用例
    const gchar *home = g_getenv("HOME");
    if (g_strcmp0(home, "/root") == 0) {
        g_assert_cmpint(ret, ==, 0);
    } else {
        g_assert_cmpint(ret, ==, -1);
    }
    // 恢复临时文件权限为读写
    (void)chmod(reboot_temp_file, S_IRUSR | S_IWUSR);
    // 构造数据文件并设置为只读场景, 期望：per_save保存失败
    (void)chmod(reboot_data_file, S_IRUSR);
    ret = per_save(per_items + 1);
    if (g_strcmp0(home, "/root") == 0) {
        g_assert_cmpint(ret, ==, 0);
    } else {
        g_assert_cmpint(ret, ==, -1);
    }

    // 恢复读写文件权限为读写，期望：per_save保存成功
    (void)chmod(reboot_data_file, S_IRUSR | S_IWUSR);
    ret = per_save(per_items + 1);
    g_assert_cmpint(ret, ==, 0);

    ret = per_load(per_items + 2);
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpint(per_power_off_key3, ==, rand_sum);

    ret = per_load(per_items + 3);
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpstr((gchar *)per_items[3].data, ==, data);

    // group为空时默认设置为"_"
    ret = per_load(per_items + 4);
    g_assert_cmpint(ret, ==, 0);
    g_assert_cmpstr((gchar *)per_items[4].data, ==, data);

    // per_items[1]的长度置为零后保存持久化，删除数据清零
    per_items[1].length = 0;
    ret = per_save(per_items + 1);
    g_assert_cmpint(ret, ==, 0);
    // 重复删除，返回－1
    ret = per_save(per_items + 1);
    g_assert_cmpint(ret, ==, -1);
    // 异常情况
    ret = per_save(NULL);
    g_assert_cmpint(ret, ==, -1);
    // data为空
    g_free(per_items[3].data);
    per_items[3].data = NULL;
    ret = per_save(NULL);
    g_assert_cmpint(ret, ==, -1);

    ret = per_load(NULL);
    g_assert_cmpint(ret, ==, -1);

}

static void test_show_all_objects(void)
{
    lb_print_all_objects();
}

void test_add_test_case(void)
{
    if (lb_log_get_type() == LOG_TO_FILE) {
        lb_log_set_level(LOG_INFO);
    }
    // connector加载需要时间，此处预留1秒
    sleep(1);
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_usec);
    g_test_add_func("/test/lb_get_bus", test_lb_get_bus);
    g_test_add_func("/test/lb_get_interface", test_lb_get_interface);
    g_test_add_func("/test/lb_interfaces", test_lb_interfaces_origin);
    g_test_add_func("/test/global_mds_loaded", test_global_mds_loaded);
    g_test_add_func("/test/show_connector", test_show_connector);
    g_test_add_func("/test/object_ref", test_object_ref);
    g_test_add_func("/test/object_new_ref", test_object_new_ref);
    g_test_add_func("/test/object_get_ref", test_object_get_ref);
    g_test_add_func("/test/object_list_ref1", test_object_list_ref1);
    g_test_add_func("/test/object_list_ref2", test_object_list_ref2);
    g_test_add_func("/test/object_nth_dec_ref", test_object_nth_dec_ref);
    g_test_add_func("/test/object_nth_ref", test_object_nth_ref);
    g_test_add_func("/test/connector_load", test_connector_load);
    g_test_add_func("/test/connector_load_fuzz", test_connector_load_fuzz);
    g_test_add_func("/test/formatter", test_formatter);
    g_test_add_func("/test/formatter_fuzz", test_formatter_fuzz);
    g_test_add_func("/test/expression", test_expression);
    g_test_add_func("/test/expression_fuzz", test_expression_double_fuzz);
    g_test_add_func("/test/reference_property", test_reference_property);
    g_test_add_func("/test/reference_object", test_reference_object);
    g_test_add_func("/test/persistence", test_persistence);
    g_test_add_func("/test/persistence_new", test_persistence_new);
    g_test_add_func("/test/show_all_all_objects", test_show_all_objects);
}

static void _test_start_0(void)
{
    lb_log_set_alert_abort(FALSE);
    log_info("Test service start.");
}

static void __attribute__((constructor(200))) _test_init_0(void)
{
    lb_module_register(_test_start_0, "_test_start_0", 0);
}
