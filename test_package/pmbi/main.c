#include "lb_core.h"
#include "test.service.h"

int main(int argc, char *argv[])
{
    unlink("/dev/shm/per.db");
    lb_start("com.litebmc.Test");
    g_test_init(&argc, &argv, NULL);
    test_add_test_case();
    return g_test_run();
}
