#include "lb_core.h"
#include "lb_internal.h"
#include "server/org.freedesktop.DBus.ObjectManager.h"
#include "client/org.freedesktop.DBus.h"
#include "client/com.litebmc.InterfaceManager.h"

// InterfaceManager客户端对象
InterfaceManager_Cli im_cli = NULL;
// InterfaceEvent事件队列，收到事件后异步处理
static GAsyncQueue *interface_event_queue = NULL;
typedef struct {
    gchar *name;
    LBList list_head;
} WellKnownServiceInfo;

typedef struct {
    LBO *obj;
    LBList list_node;
} WellKnownObjectInfo;
// 以LBO对象内存地址为key，值为WellKnownObjectInfo对象地址，存储所有对象基本信息
static GHashTable *obj_table = NULL;

// unique_name_to_well_known
static GHashTable *u2w_table = NULL;
static GHashTable *w2u_table = NULL;
// InterfaceManager锁
G_LOCK_DEFINE_STATIC(im_lock);
// org.freedesktop.DBus客户端对象
static DBus_Cli dbus_cli = NULL;
// 订阅的接口列表
static GSList *sub_interfaces = NULL;
static GMutex sub_lock;
static GCond sub_cond;

static GHashTable *local_objects;
G_LOCK_DEFINE_STATIC(lock);

typedef struct {
    LBO *obj;
} InterfaceInstance;

typedef struct {
    const gchar *object_prefix;
    ObjectData *obj_dict;
} GetAllObjectsData;

static void make_interface_properties(LBO *object, const LBInterface *desc, InterfaceData *intf_dict)
{
    const LBProperty *properties = desc->properties;
    PropertyData *prop_dict = PropertyData_new();
    for (; properties && properties->name; properties++) {
        if (properties->flags & LB_FLAGS_PROPERTY_PRIVATE) {
            continue;
        }
        GVariant *val = lbo_get_memory(object, properties);
        if (!val) {
            continue;
        }
        PropertyDataItem *prop = g_new0(PropertyDataItem, 1);
        prop->value = val;
        prop_dict->insert(prop_dict, desc->name, &prop);
    }
    InterfaceDataItem *intf_item = g_new0(InterfaceDataItem, 1);
    intf_item->PropertyData = prop_dict;
    intf_dict->insert(intf_dict, desc->name, &intf_item);
}

static void foreach_object(const gchar *object_name, GSList *objects, GetAllObjectsData *data)
{
    log_debug("object_name: %s, prefix: %s", object_name, data->object_prefix);
    if (g_str_has_prefix(object_name, data->object_prefix) == FALSE) {
        return;
    }

    for (GSList *item = objects; item && item->data; item = item->next) {
        InterfaceInstance *ins = (InterfaceInstance *)item->data;
        if (lbo_present(ins->obj) == FALSE) {
            continue;
        }
        const LBInterface *desc = lbo_interface(ins->obj);
        if (desc == OBJECT_MANAGER) {
            continue;
        }
        ObjectDataItem *obj_data = g_new0(ObjectDataItem, 1);
        obj_data->InterfaceData = InterfaceData_new();
        make_interface_properties(ins->obj, desc, obj_data->InterfaceData);
        data->obj_dict->insert(data->obj_dict, lbo_name(ins->obj), &obj_data);
    }
}

#ifdef LB_VERSION_BE_0_8_2
int get_all_objects(ObjectManager object, const ObjectManager_GetManagedObjects_Req *req,
    ObjectManager_GetManagedObjects_Rsp **rsp, GError **error, LBMethodExtData *ext_data)
#else
int get_all_objects(ObjectManager object, const ObjectManager_GetManagedObjects_Req *req,
    ObjectManager_GetManagedObjects_Rsp **rsp, GError **error, gpointer ext_data)
#endif
{
    GetAllObjectsData data = {.object_prefix = lbo_name(object)};
    *rsp = g_new0(ObjectManager_GetManagedObjects_Rsp, 1);
    (*rsp)->Objects = ObjectData_new();
    data.obj_dict = (*rsp)->Objects;
    G_LOCK(lock);
    g_hash_table_foreach(local_objects, (GHFunc)foreach_object, &data);
    G_UNLOCK(lock);
    return 0;
}

static void object_manager_added(LBO *object, ObjectManager om)
{
    const gchar *obj_name = lbo_name(object);
    G_LOCK(lock);
    GSList *interfaces = (GSList *)g_hash_table_lookup(local_objects, obj_name);
    gboolean interface_inserted = FALSE;
    for (GSList *item = interfaces; item && item->data; item = item->next) {
        InterfaceInstance *ins = (InterfaceInstance *)item->data;
        if (ins->obj == object) {
            interface_inserted = TRUE;
            break;
        }
    }
    if (interface_inserted == FALSE) {
        InterfaceInstance *ins = g_new0(InterfaceInstance, 1);
        ins->obj = object;
        interfaces = g_slist_append(interfaces, ins);
    }
    if (interfaces->next == NULL) {
        g_hash_table_insert(local_objects, g_strdup(obj_name), interfaces);
        ObjectManager_present_set(om, TRUE);
    }
    G_UNLOCK(lock);
}

static void object_manager_removed(LBO *object, ObjectManager om)
{
    const gchar *obj_name = lbo_name(object);
    G_LOCK(lock);
    GSList *interfaces = (GSList *)g_hash_table_lookup(local_objects, obj_name);
    gboolean interface_all_removed = TRUE;
    for (GSList *item = interfaces; item && item->data; item = item->next) {
        InterfaceInstance *ins = (InterfaceInstance *)item->data;
        if (ObjectManager_present(ins->obj) == TRUE) {
            interface_all_removed = FALSE;
            break;
        }
    }
    G_UNLOCK(lock);
    if (interface_all_removed) {
        ObjectManager_present_set(om, FALSE);
    }
}

/* 新增或删除对象时发送ObjectAdded和ObjectRemoved信号 */
static void obj_present_changed(LBO *object, gpointer user_data)
{
    GError *error = NULL;
    gboolean ret;
    const LBInterface *desc = lbo_interface(object);
    const gchar *obj_name = lbo_name(object);

    log_mass("is_remote: %d, interface:%s, object: %s", desc->is_remote, desc->name, obj_name);
    // 忽略远端对象或私有对象
    if (desc->is_remote || !g_variant_is_object_path(obj_name)) {
        return;
    }

    const gchar *signal_name = NULL;
    ObjectManager om = ObjectManager_new(obj_name, NULL);
    if (lbo_present(object)) {
        object_manager_added(object, om);
        log_debug("add object: %s", lbo_name(om));
        signal_name = "InterfacesAdded";
        ObjectManager_InterfacesAdded_Msg msg = {NULL};
        msg.ObjectPath = obj_name;
        msg.Interfaces = InterfaceData_new();
        make_interface_properties(object, desc, msg.Interfaces);
        #ifdef LB_VERSION_BE_0_8_2
        ret = ObjectManager_Emit_InterfacesAdded(om, NULL, &msg, &error);
        #else
        ret = ObjectManager_InterfacesAdded_Signal(om, NULL, &msg, &error);
        #endif
        InterfaceData_free(&msg.Interfaces);
    } else {
        signal_name = "InterfacesRemoved";
        log_debug("del object: %s", lbo_name(om));
        cleanup_gfree gchar *intf_name = g_strdup(desc->name);
        gchar *interfaces[2] = {intf_name, NULL};
        ObjectManager_InterfacesRemoved_Msg msg = {
            .ObjectPath = obj_name,
            .Interfaces = interfaces,
        };
        object_manager_removed(object, om);
        #ifdef LB_VERSION_BE_0_8_2
        ret = ObjectManager_Emit_InterfacesRemoved(om, NULL, &msg, &error);
        #else
        ret = ObjectManager_InterfacesRemoved_Signal(om, NULL, &msg, &error);
        #endif
    }
    if (ret != TRUE) {
        if (error) {
            log_warn("Send %s failed, error: %s", signal_name, error->message);
            g_error_free(error);
        } else {
            log_warn("Send %s failed", signal_name);
        }
    }
}

typedef struct {
    IMObject obj;
    gboolean is_added;
} InterfaceEventMsg;

static void _interface_event_msg_new(const IMObject *obj, gboolean is_added)
{
    if (!g_dbus_is_name(obj->well_known) || !g_dbus_is_unique_name (obj->unique_name) ||
        !g_variant_is_object_path(obj->path) || !g_dbus_is_interface_name(obj->interface)) {
        return;
    }
    InterfaceEventMsg *msg = g_new0(InterfaceEventMsg, 1);
    msg->is_added = is_added;
    msg->obj.interface = g_strdup(obj->interface);
    msg->obj.path = g_strdup(obj->path);
    msg->obj.well_known = g_strdup(obj->well_known);
    msg->obj.unique_name = g_strdup(obj->unique_name);
    g_async_queue_push(interface_event_queue, msg);
}

#ifdef LB_VERSION_BE_0_8_2
static void _interface_manager_event(InterfaceManager_Cli object, const gchar *destination,
    const InterfaceManager_InterfaceEvent_Msg *req, gpointer user_data, const LBSignalExtData *ext)
#else
static void _interface_manager_event(InterfaceManager_Cli object, const gchar *destination,
    const InterfaceManager_InterfaceEvent_Msg *req, gpointer user_data)
#endif
{
    for (int i = 0; req->objects && req->objects[i]; i++) {
        // 推送到异步队列等待处理
        _interface_event_msg_new(req->objects[i], req->is_added);
    }
}

static void _subscribe_interface_event(gpointer user_data)
{
    while (TRUE) {
        // InterfaceEvent信号只发给关注了特定接口的服务，所以此处关注即可
        guint sub_id = InterfaceManager_Cli_Subscribe_InterfaceEvent(_interface_manager_event,
            INTERFACE_MAMAGER_SERVICE, INTERFACE_MAMAGER_PATH, NULL, NULL);
        if (sub_id > 0) {
            break;
        }
        log_warn("Subscribe InterfaceEvent failed");
        sleep(1);
    }
}

/**
 * @brief  w2u_table成员释放函数
 * @note   从w2u_table删除成员(g_hash_table_remove)受im_lock保护，故此函数不需要加锁保护
 * @param  arg: WellKnownServiceInfo对象
 * @retval None
 */
static void free_well_known_service_info(gpointer arg)
{
    WellKnownServiceInfo *info = (WellKnownServiceInfo *)arg;
    for (LBList *node = info->list_head.next; node != &info->list_head;) {
        LBList *next = node->next;
        WellKnownObjectInfo *oinfo = CONTAINER_OF(node, WellKnownObjectInfo, list_node);
        lb_list_remove(&oinfo->list_node);
        g_hash_table_remove(obj_table, oinfo->obj);
        lbo_unref(&oinfo->obj);
        g_free(oinfo);
        node = next;
    }
    g_free(info->name);
    memset(info, 0, sizeof(*info));
    g_free(info);
}

static WellKnownServiceInfo *get_well_known_service_info(const gchar *unique_name, const gchar *well_known)
{
    WellKnownServiceInfo *info = g_hash_table_lookup(w2u_table, well_known);
    if (!info) {
        info = g_new0(WellKnownServiceInfo, 1);
        info->name = g_strdup(unique_name);
        lb_list_init(&info->list_head);
        g_hash_table_insert(w2u_table, g_strdup(well_known), info);
    }
    return info;
}

// 异步任务，用于处理接口事件
static void _interface_event_worker(GAsyncQueue *queue)
{
    InterfaceEventMsg *msg = NULL;
    while (1) {
        msg = (InterfaceEventMsg *)g_async_queue_pop(queue);
        const LBInterface *intf_desc = lb_get_interface(TRUE, msg->obj.interface);
        if (!intf_desc) {
            log_warn("Unkonw interface %s received, if you need subscribe it, you must add it's client package to build requires",
                msg->obj.interface);
            IMObject_clean(&msg->obj);
            g_free(msg);
            continue;
        }
        G_LOCK(im_lock);
        WellKnownServiceInfo *info = get_well_known_service_info(msg->obj.unique_name, msg->obj.well_known);
        if (msg->is_added) {
            // 创建本地对象，加引用计数1
            LBO *lb_obj = lbo_cli_new(intf_desc, msg->obj.well_known, msg->obj.path);
            WellKnownObjectInfo *oinfo = g_new0(WellKnownObjectInfo, 1);
            oinfo->obj = lb_obj;
            lb_list_insert_before(&info->list_head, &oinfo->list_node);
            g_hash_table_insert(obj_table, (gpointer)lb_obj, oinfo);
        } else {
            // 查询本地对象，加引用计数1
            LBO *lb_obj = lbo_cli_get(intf_desc, msg->obj.well_known, msg->obj.path);
            // 卸载对象
            if (lb_obj) {
                // 从对象列表中删除
                WellKnownObjectInfo *oinfo = g_hash_table_lookup(obj_table, (gpointer)lb_obj);
                if (oinfo) {
                    lb_list_remove(&oinfo->list_node);
                    g_hash_table_remove(obj_table, lb_obj);
                    lbo_unref(&oinfo->obj);
                    g_free(oinfo);
                }
                lbo_present_set(lb_obj, FALSE);
                // 减两次引用计数释放对象
                lbo_unref(&lb_obj);
            }
        }
        G_UNLOCK(im_lock);
        // 释放资源
        IMObject_clean(&msg->obj);
        g_free(msg);
    }
}

static gint _subscribe_interface_task(const gchar *intf);
static void _monitor_subscrbe_interface_stat(gchar *intf_name)
{
    while (1) {
        g_mutex_lock(&sub_lock);
        g_cond_wait(&sub_cond, &sub_lock);
        g_mutex_unlock(&sub_lock);
        _subscribe_interface_task(intf_name);
    }
}

static void _subscribe_interface_insert(const gchar *intf)
{
    G_LOCK(im_lock);
    for (GSList *item = sub_interfaces; item; item = item->next) {
        gchar *intf_tmp = (gchar *)item->data;
        if (g_strcmp0(intf_tmp, intf) == 0) {
            G_UNLOCK(im_lock);
            return;
        }
    }
    gchar *new_intf = g_strdup(intf);
    sub_interfaces = g_slist_append(sub_interfaces, new_intf);
    G_UNLOCK(im_lock);
    GThread *th = g_thread_new("SubIntfMnt", (GThreadFunc)_monitor_subscrbe_interface_stat, new_intf);
    g_thread_unref(th);
}

// 先订阅接口再获取所有对象
static gint _subscribe_interface_task(const gchar *intf)
{
    gint ret = 0;
    InterfaceManager_SubscribeInterface_Req req = {
        .interface = intf
    };
    // 订阅接口
    for (int i = 0; i < 60; i++) {
        if (!im_cli) {
            sleep(1);
            continue;
        }
        cleanup_error GError *error = NULL;
        // 订阅接口
        ret = InterfaceManager_Cli_Call_SubscribeInterface(im_cli, &req, 10000, &error);
        if (ret != 0) {
            log_error("Subscribe interface %s failed, ret: %d, msg: %s", intf, ret, error->message);
            sleep(1);
            continue;
        }
        log_info("Subscribe interface %s success", intf);
        break;
    }
    if (ret == 0) {
        _subscribe_interface_insert(intf);
    }
    return ret;
}

#ifdef LB_VERSION_BE_0_8_2
static void _name_owner_changed(DBus_Cli object, const gchar *destination,
    const DBus_NameOwnerChanged_Msg *req, gpointer user_data, const LBSignalExtData *ext)
#else
static void _name_owner_changed(DBus_Cli object, const gchar *destination,
    const DBus_NameOwnerChanged_Msg *req, gpointer user_data)
#endif
{
    if (req->name[0] == ':') {
        return;
    }
    G_LOCK(im_lock);
    if (req->new_owner[0] == '\0') {
        // 服务下线，old_owner必然是unique_name
        g_hash_table_remove(u2w_table, req->old_owner);
        g_hash_table_remove(w2u_table, req->name);
    } else {
        // 服务上线
        if (g_strcmp0(req->name, INTERFACE_MAMAGER_SERVICE) == 0) {
            g_mutex_lock(&sub_lock);
            g_cond_broadcast(&sub_cond);
            g_mutex_unlock(&sub_lock);
        }
        g_hash_table_insert(u2w_table, g_strdup(req->new_owner), g_strdup(req->name));
        (void)get_well_known_service_info(req->new_owner, req->name);
    }
    G_UNLOCK(im_lock);
}

static void _list_names(void)
{
    // 关注服务状态变更信号
    guint sub_id = DBus_Cli_Subscribe_NameOwnerChanged(_name_owner_changed,
        DBUS_SERVICE, DBUS_PATH, NULL, NULL);
    if (sub_id == 0) {
        log_alert("Subscripbe NameOwnerChanged failed");
    }

    // 查找系统所有服务
    DBus_ListNames_Rsp *rsp = NULL;
    GError *error = NULL;
    gint ret = DBus_Cli_Call_ListNames(dbus_cli, &rsp, 10000, &error);
    if (ret != 0) {
        log_alert("Call ListNames failed, error: %s", error->message);
    }
    guint len = g_strv_length(rsp->names);
    for (; len > 0; len--) {
        gchar *name = rsp->names[len -1];
        // 只处理well-known服务
        if (name[0] == ':') {
            continue;
        }
        DBus_GetNameOwner_Req req1 = {
            .name = name
        };
        DBus_GetNameOwner_Rsp *rsp = NULL;
        cleanup_error GError *error = NULL;
        // 获取well_known服务名对应的unique_name
        ret = DBus_Cli_Call_GetNameOwner(dbus_cli, &req1, &rsp, 10000, &error);
        if (ret != 0) {
            log_warn("GetNameOwner failed, well_known: %s, msg: %s", name, error->message);
        } else {
            G_LOCK(im_lock);
            g_hash_table_insert(u2w_table, g_strdup(rsp->owner), g_strdup(name));
            (void)get_well_known_service_info(rsp->owner, name);
            G_UNLOCK(im_lock);
            DBus_GetNameOwner_Rsp_free(&rsp);
        }
    }
    DBus_ListNames_Rsp_free(&rsp);
}

static void _object_manage_start(void)
{
    log_info("Start ObjectManager");
    local_objects = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
    ObjectManager_Methods *methods = ObjectManager_methods();
    methods->GetManagedObjects.handler = get_all_objects;

    GSList *interfaces = lb_interfaces(FALSE);
    for (GSList *item = interfaces; item && item->data; item = item->next) {
        const LBInterface *desc = (const LBInterface *)item->data;
        // 忽略ObjectManager接口
        if (g_strcmp0(desc->name, OBJECT_MANAGER->name) == 0 || g_dbus_is_interface_name(desc->name) == FALSE) {
            continue;
        }
        log_mass("listen interface: %s", desc->interface->name);
        lb_impl._on_changed(desc, obj_present_changed, NULL, NULL);
    }
    g_slist_free(interfaces);

    // 创建服务上线条件变量
    g_mutex_init(&sub_lock);
    g_cond_init(&sub_cond);

    // 创建DBus对象
    dbus_cli = lbo_cli_new(DBUS_CLI, DBUS_SERVICE, DBUS_PATH);
    u2w_table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    w2u_table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, free_well_known_service_info);
    obj_table = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, NULL);
    interface_event_queue = g_async_queue_new();
    GThread *th = g_thread_new("IntfEventChg", (GThreadFunc)_interface_event_worker, (gpointer)interface_event_queue);
    g_thread_unref(th);
    th = g_thread_new("SubIntfEvent", (GThreadFunc)_subscribe_interface_event, NULL);
    g_thread_unref(th);
    im_cli = lbo_cli_new(INTERFACE_MANAGER_CLI, INTERFACE_MAMAGER_SERVICE, INTERFACE_MAMAGER_PATH);
    _list_names();
    // 订阅InterfaceManager接口
    #ifndef BUILD_TEST
    if (g_strcmp0(lb_bus_name(), INTERFACE_MAMAGER_SERVICE)) {
        _subscribe_interface_insert(INTERFACE_MANAGER_CLI->name);
    }
    _subscribe_interface_insert(DBUS_CLI->name);
    #endif
}

// 订阅接口，从interface_manager获取整个系统所有实现该接口的对象并接收对象创建、销毁事件
gint lb_subscribe_interface(const gchar *interface_name)
{
    const LBInterface *intf_desc = lb_get_interface(TRUE, interface_name);
    if (!intf_desc) {
        log_error("Unregisted interface %s found, subscribe failed", interface_name);
        return -1;
    }
    if (!intf_desc->is_remote) {
        log_error("Only client interface can be subscribed, %s is a server interface", interface_name);
        return -1;
    }
    return _subscribe_interface_task(interface_name);
}

// 使用unique_name获取对象
gchar *lb_well_known(const gchar *unique_name)
{
    gchar *well_known = NULL;
    G_LOCK(im_lock);
    gchar *name = (gchar *)g_hash_table_lookup(u2w_table, unique_name);
    if (name) {
        well_known = g_strdup(name);
    }
    G_UNLOCK(im_lock);
    return well_known;
}

// 使用unique_name获取对象
gchar *lb_unique_name(const gchar *well_known)
{
    gchar *unique_name = NULL;
    G_LOCK(im_lock);
    WellKnownServiceInfo *info = (WellKnownServiceInfo *)g_hash_table_lookup(w2u_table, well_known);
    if (info) {
        unique_name = g_strdup(info->name);
    }
    G_UNLOCK(im_lock);
    return unique_name;
}

static void __attribute__((constructor(200))) _object_manage_init(void)
{
    log_info("Init ObjectManager");
    // 需要在interfaces_start完成接口注册后启动，优先级设置为5
    lb_module_register(_object_manage_start, OBJECT_MANAGER->name, 5);
}