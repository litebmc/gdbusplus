#include "lb_core.h"
#include "server/com.litebmc.Expression.h"
#include "muParserDLL.h"

static void _on_mup_error(muParserHandle_t parser)
{
    log_error("muParser Error, Message: %s, Errc: %d", mupGetErrorMsg(parser), mupGetErrorCode(parser));
}

static void _property_changed(Expression exp, const LBProperty *desc, GVariant *value, gpointer user_data)
{
    muParserHandle_t parser = (muParserHandle_t)Expression_data(exp);
    if (!parser) {
        parser = mupCreate(muBASETYPE_FLOAT);
        mupSetErrorHandler(parser, _on_mup_error);
        mupDefineVar(parser, "a", (muFloat_t *)&exp->a);
        mupDefineVar(parser, "b", (muFloat_t *)&exp->b);
        mupDefineVar(parser, "c", (muFloat_t *)&exp->c);
        mupDefineVar(parser, "d", (muFloat_t *)&exp->d);
        Expression_bind(exp, (gpointer)parser, (GDestroyNotify)mupRelease);
    }
    mupSetExpr(parser, (const muChar_t *)(exp->formula ? exp->formula : "0"));
    muFloat_t out = mupEval(parser);
    Expression_set_value(exp, (double)out);
}

static void _expression_start(void)
{
    Expression_a_hook(NULL, _property_changed, NULL);
    Expression_b_hook(NULL, _property_changed, NULL);
    Expression_c_hook(NULL, _property_changed, NULL);
    Expression_d_hook(NULL, _property_changed, NULL);
    Expression_formula_hook(NULL, _property_changed, NULL);
}

static void __attribute__((constructor(200))) _expression_init(void)
{
    log_info("Init expression");
    lb_module_register(_expression_start, EXPRESSION->name, 8);
}
