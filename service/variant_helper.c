#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <glib-2.0/glib.h>
#include "lb_base.h"
#include "variant_helper.h"

gdouble variant_to_double(GVariant *value)
{
    if (!value)
        return 0;
    const gchar *sig = g_variant_get_type_string(value);
    if (!sig || !sig[0] || !strchr("bynqiuxthds", sig[0]) || sig[1])
        return 0;
    switch (sig[0]) {
        case 'b':
            return (gdouble)g_variant_get_boolean(value);
        case 'y':
            return (gdouble)g_variant_get_byte(value);
        case 'n':
            return (gdouble)g_variant_get_int16(value);
        case 'q':
            return (gdouble)g_variant_get_uint16(value);
        case 'i':
            return (gdouble)g_variant_get_int32(value);
        case 'u':
            return (gdouble)g_variant_get_uint32(value);
        case 'x':
            return (gdouble)g_variant_get_int64(value);
        case 't':
            return (gdouble)g_variant_get_uint64(value);
        case 'd':
            return (gdouble)g_variant_get_double(value);
        case 'h':
            return (gdouble)g_variant_get_handle(value);
        case 's':
            return (gdouble)g_ascii_strtod(g_variant_get_string(value, NULL), NULL);
    }
    return 0;
}

GVariant *variant_by_double(gdouble value, gchar tgt_type, gboolean src_is_bool)
{
    GString *str;
    gchar *str_t;
    GVariant *out;
    switch (tgt_type) {
        case 'b':
            return g_variant_new_boolean(value ? TRUE : FALSE);
        case 'y':
            return g_variant_new_byte((guint8)value);
        case 'n':
            return g_variant_new_int16((gint16)value);
        case 'q':
            return g_variant_new_uint16((guint16)value);
        case 'i':
            return g_variant_new_int32((gint)value);
        case 'u':
            return g_variant_new_uint32((guint32)value);
        case 'x':
            return g_variant_new_int64((gint64)value);
        case 't':
            return g_variant_new_uint64((guint64)value);
        case 'd':
            return g_variant_new_double(value);
        case 'h':
            return g_variant_new_handle((gint)value);
        case 's':
            if (src_is_bool)
                return g_variant_new_string(value ? "true" : "false");

            str = g_string_sized_new(16);
            if ((((gint64)value) / 1.0) == value)
                g_string_printf(str, "%f", value);
            else
                g_string_printf(str, "%.6f", value);
            str_t = g_string_free(str, FALSE);
            out = g_variant_new_string(str_t);
            g_free(str_t);
            return out;
        default:
            return NULL;
    }
}

gdouble *variant_to_double_array(GVariant *value, gint *cnt)
{
    if (!value || !cnt)
        return NULL;
    *cnt = 0;
    const gchar *sig = g_variant_get_type_string(value);
    if (!sig || !sig[0] || sig[0] != 'a' || !sig[1] || !strchr("bynqiuxtdhs", sig[1]) || sig[2])
        return NULL;
    *cnt = g_variant_n_children(value);
    gdouble *out = g_new0(gdouble, *cnt);
    for (int i = 0; i < *cnt; i++) {
        GVariant *v = g_variant_get_child_value(value, i);
        out[i] = variant_to_double(v);
        g_variant_unref(v);
    }
    return out;
}

GVariant *variant_by_double_array(const gdouble *value, gint cnt, const gchar *tgt_type, gboolean src_is_bool)
{
    GVariantBuilder builder;
    if (!value || !cnt)
        return NULL;
    if (!tgt_type || !tgt_type[0] || tgt_type[0] != 'a' || !tgt_type[1] || !strchr("bynqiuxtdhs", tgt_type[1]) || tgt_type[2])
        return NULL;
    g_variant_builder_init(&builder, G_VARIANT_TYPE(tgt_type));
    for (int i = 0; i < cnt; i++)
        g_variant_builder_add_value(&builder, variant_by_double(value[i], tgt_type[1], src_is_bool));
    return g_variant_builder_end(&builder);
}
