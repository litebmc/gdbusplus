/**
 * @file plugin.c
 * @author xuhj@litebmc.com
 * @brief 插件注册功能
 * @version 0.1
 * @date 2024-04-23
 * @nodes 仅提供加载接口，由component.json描述需要加载的插件路径
 * @copyright Copyright (c) 2024-2024
 *
 */
#include <dlfcn.h>
#include "lb_core.h"

static GSList *dl_list = NULL;

typedef int (*plugin_start)(void);

static void _load_plugin_file(const gchar *plugin_name)
{
    log_info("Start load plugin %s", plugin_name);
    void *dl = dlopen(plugin_name, RTLD_LAZY);
    if (!dl) {
        log_warn("Load plugin failed, msg: %s", dlerror());
        return;
    }
    plugin_start start = (plugin_start)dlsym(dl, "plugin_start");
    if (start == NULL) {
        log_warn("Can't get plugin_start symbol, msg: %s", dlerror());
        dlclose(dl);
        return;
    }
    int ret = start();
    if (ret != 0) {
        log_warn("Run plugin %s failed", plugin_name);
        dlclose(dl);
        return;
    }
    dl_list = g_slist_append(dl_list, dl);
}

static void _load_plugin_dir(const gchar *dir_path)
{
    DIR *dir = opendir(dir_path);
    if (!dir) {
        if (errno != ENOENT) {
            log_warn("plugin directory not exist, msg: %s", strerror(errno));
        }
        return;
    }
    GString *tmp = g_string_new(dir_path);
    g_string_append(tmp, "/");
    gsize len = tmp->len;
    while (TRUE) {
        g_string_truncate(tmp, len);
        struct dirent *ent = readdir(dir);
        if (ent == NULL) {
            break;
        }
        g_string_append(tmp, ent->d_name);
        if (ent->d_type == DT_REG) {
            _load_plugin_file(tmp->str);
        } else if (ent->d_type == DT_LNK) {
            log_warn("Skip load plugin file because file %s not a regular file", tmp->str);
        }
    }
    closedir(dir);
    g_string_free(tmp, TRUE);
}

static void _plugin_start(void)
{
    log_info("plugin start");
    // 从公共插件目录中加载插件
    const gchar *app_path = get_app_dir();
    cleanup_gfree gchar *self_plugin_path = lb_printf("%s/plugin", app_path);
    _load_plugin_dir(self_plugin_path);

    cleanup_gfree gchar *public_plugin_dir = get_rootfs_path("/opt/litebmc/plugins");
    _load_plugin_dir(public_plugin_dir);

#ifdef LB_CODEGEN_BE_5_2
    // 从接口申明的插件位置加载插件
    GSList *intfs = lb_interfaces_origin();
    for (GSList *item = intfs; item; item = item->next) {
        LBInterface *intf = (LBInterface *)item->data;
        if (intf->plugin_dir) {
            cleanup_gfree gchar *intf_plugin_dir = get_rootfs_path(intf->plugin_dir);
            _load_plugin_dir(intf_plugin_dir);
        }
    }
    g_slist_free(intfs);
#endif
}

static void __attribute__((constructor(200))) _plugin_init(void)
{
    log_info("plugin init");
    // 在持久化之后启动，优先级设置为1
    lb_module_register(_plugin_start, "plugin", 1);
}
