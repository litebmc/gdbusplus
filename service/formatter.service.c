#include "lb_core.h"
#include "server/com.litebmc.Formatter.h"

#if 0
static gchar *_format_variant_value(GVariant *value)
{
    const gchar *val_type;
    GString *out = g_string_new("???");
    gchar *tmp_str;

    val_type = g_variant_get_type_string(value);
    switch (val_type[0]) {
        case G_VARIANT_CLASS_BOOLEAN:
            if (g_variant_get_boolean(value))
                g_string_assign(out, "True");
            else
                g_string_assign(out, "False");
            break;
        case G_VARIANT_CLASS_INT32:
            g_string_printf(out, "%d", g_variant_get_int32(value));
            break;
        case G_VARIANT_CLASS_UINT32:
            g_string_printf(out, "%u", g_variant_get_uint32(value));
            break;
        case G_VARIANT_CLASS_INT64:
            g_string_printf(out, "%"G_GINT64_FORMAT, g_variant_get_int64(value));
            break;
        case G_VARIANT_CLASS_UINT64:
            g_string_printf(out, "%"G_GUINT64_FORMAT, g_variant_get_uint64(value));
            break;
        case G_VARIANT_CLASS_DOUBLE:
            g_string_printf(out, "%f", g_variant_get_double(value));
            break;
        case G_VARIANT_CLASS_STRING:
        case G_VARIANT_CLASS_OBJECT_PATH:
            g_string_printf(out, "%s", g_variant_get_string(value, NULL));
            break;
        default:
            tmp_str = g_variant_print(value, FALSE);
            g_string_assign(out, tmp_str);
            g_free(tmp_str);
    }
    return g_string_free(out, FALSE);
}
#endif

static void _property_changed(Formatter fmt, const LBProperty *desc, GVariant *value, gpointer user_data)
{
    gchar *pos, *last_pos;
    const gchar *arg_str[4];

    if (!fmt->format) {
        Formatter_set_value(fmt, "");
        return;
    }
    if (!fmt->a || !fmt->b || !fmt->c || !fmt->d)
        return;

    arg_str[0] = fmt->a;
    arg_str[1] = fmt->b;
    arg_str[2] = fmt->c;
    arg_str[3] = fmt->d;
    GString *output = g_string_new("");
    gchar *format = g_strdup(fmt->format);
    last_pos = pos = format;
    while ((pos = strchr(pos, '$')) != NULL) {
        /* $符号前带'\'转义，不需要替换 */
        if (pos != last_pos && pos[-1] == '\\') {
            pos[-1] = '$';
            pos[0] = '\0';
            output = g_string_append(output, last_pos);
            last_pos = pos = pos + 1;
            continue;
        }
        /* $符号后跟的不是a~d，不需要替换 */
        if (pos[1] > 'd' || pos[1] < 'a') {
            pos++;
            continue;
        }
        pos[0] = '\0';
        if (pos != last_pos)
            output = g_string_append(output, last_pos);
        output = g_string_append(output, arg_str[pos[1] - 'a']);
        last_pos = pos = pos + 2;
    }
    if (last_pos[0] != '\0')
        output = g_string_append(output, last_pos);
    gchar *str_val = g_string_free(output, FALSE);
    log_debug("Formatter object %s.value: %s, format: %s", Formatter_name(fmt), str_val, fmt->format);
    Formatter_set_value(fmt, str_val);
    g_free(str_val);
    g_free(format);
}
static void _fomatter_start(void)
{
    Formatter_a_hook(NULL, _property_changed, NULL);
    Formatter_b_hook(NULL, _property_changed, NULL);
    Formatter_c_hook(NULL, _property_changed, NULL);
    Formatter_d_hook(NULL, _property_changed, NULL);
    Formatter_format_hook(NULL, _property_changed, NULL);
}

static void __attribute__((constructor(200))) _formatter_init(void)
{
    log_info("Init formatter");
    lb_module_register(_fomatter_start, FORMATTER->name, 8);
}
