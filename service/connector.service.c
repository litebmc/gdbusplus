/**
 * @file connector.service.c
 * @author xuhj@litebmc.com
 * @brief 对象连接器
 * @version 0.1
 * @date 2022-11-19
 * @nodes 如果flags带ro的s和as属性将会根据连接器的position做对象重名称
 * @copyright Copyright (c) 2022
 *
 */
#include "lb_core.h"
#include "lb_internal.h"
#include "variant_helper.h"
#include "server/com.litebmc.Connector.h"

// 仅支持a-zA-Z_-0-9/
gint is_valid_object(const gchar *key)
{
    int i;
    if (!key || key[0] == '\0')
        return FALSE;
    for (i = 0; i < strlen(key); i++) {
        if (!(isalnum(key[i]) || key[i] == '_' || key[i] == '-' || key[i] == '.' || key[i] == '/')) {
            return FALSE;
        }
    }
    return TRUE;
}

#define CONNECTOR_POS_STR_MAX_LEN 33
typedef struct _VConnector VConnector;
struct _VConnector {
    const gchar *mds_path;
    // 上级连接器
    VConnector *prev;
    // 下级连接器
    GSList *next;
    Connector conn;
    struct {
        gchar *filename;
        guint8 position;
        guint8 physical;
        guint8 present;
    } back;
    gchar *pos;
    union {
        struct {
            guint last_err : 1; // 避免重复打印日志,标记上次是否出错,只有上次OK情况下才会打印错误日志
        };
    };
    GSList *objects; // list of LBO *object
};

struct _xml_property {
    gchar *name;
    gchar **flags;
    gboolean find_value;
    GSList *val_list;
};

struct _xml_node {
    gchar *object_name;
    gchar *interface;
    GSList *prop_list;
};

#define _XML_NAME "name"
#define _XML_FLAGS "flags"
#define _XML_PROPERTY "property"
#define _XML_OBJECT "object"
#define _XML_INTERFACE "interface"
#define _XML_VALUE "value"
#define _XML_SERVICE "service"

struct xml_content {
    GSList *output;
    const gchar *odf_file;
    gchar *bus_name;
    struct _xml_node *node;
    struct _xml_property *prop; /* 临时存在node的属性 */
    struct {
        guint32 parse_root_ele : 1; /* 正在处理OBJECTS的子元素 */
    };
};

typedef struct {
    LBO *recv_handler;
    const LBProperty *recv_desc;
} PropListener;

static VConnector g_top_vconn = {.pos = ""};


static gchar *_get_ref_interface(const gchar *ref_str)
{
    cleanup_gfree gchar *tmp = g_strdup(ref_str);
    gchar *pos = tmp + 2;
    while (*pos == '<' || *pos == ':') {
        pos++;
    }
    gchar *next_pos = strchr(pos, ':');
    *next_pos = '\0';
    return g_strdup(pos);
}

static gchar *_rename_object(const VConnector *vconn, const gchar *ref_str)
{
    gint copy_len;
    const gchar *pos = NULL;
    gchar *output = NULL;

    // "^\\$\\{([<]*|:)(([a-zA-Z_][A-Za-z0-9_]*)(\\.[a-zA-Z_][A-Za-z0-9_]*)+:)[/]?[A-Z0-9a-z_]+(/[A-Z0-9a-z_]+)*\\}$"
    if (g_regex_match(lb_ref_obj_regex(), ref_str, 0, NULL)) {
        pos = ref_str + 2;
        if (*pos == ':') {
            output = g_strdup(strchr(pos + 1, ':') + 1);
            output[strlen(output) - 1] = '\0';
        } else {
            copy_len = strlen(vconn->pos);
            while (*pos == '<') {
                pos++;
                copy_len -= 2;
            }
            const gchar *real_obj = strchr(pos, ':') + 1;
            if (copy_len >= 2) {
                GString *name = g_string_new(real_obj);
                name->str[name->len - 1] = '\0';
                name->len--;
                name = g_string_append_len(name, vconn->pos, copy_len);
                output = g_string_free(name, FALSE);
            } else {
                output = g_strdup(real_obj);
                output[strlen(output) - 1] = '\0';
            }

        }
        // 以'}'结束，需要将最后一个字符设置为结束符
        log_mass("rename object_pat %s, new name: %s, pos: %s, copy_len: %d", ref_str, output, vconn->pos,
                copy_len + 1);
        return output;
    }
    if (g_regex_match(lb_obj_regex(), ref_str, 0, NULL)) {
        copy_len = strlen(vconn->pos);
        GString *name = g_string_new(ref_str);
        name = g_string_append_len(name, vconn->pos, copy_len);
        return g_string_free(name, FALSE);
    }
    return NULL;
}

static void _remote_property_changed(LBO *object, const LBProperty *desc, GVariant *value, gpointer user_data)
{
    GVariant *new = NULL;
    PropListener *pl = (PropListener *)user_data;
    const gchar *src = g_variant_get_type_string(value);
    const gchar *tgt = pl->recv_desc->info->signature;
    if (g_strcmp0(tgt, src) == 0) {
        lbo_set_memory(pl->recv_handler, pl->recv_desc, value);
        return;
    }
    if (strlen(src) > 2)
        return;
    if (tgt[0] == 'a') {
        if (!tgt[1] || tgt[2])
            return;
        if (!strchr("bynqiuxtdhs", tgt[1]))
            return;
    } else if (tgt[1] == '\0') {
        if (!strchr("bynqiuxtdhs", tgt[0]))
            return;
    } else {
        return;
    }
    if (src[0] == 'a') {
        if (!src[1] || src[2])
            return;
        if (!strchr("bynqiuxtdhs", src[1]))
            return;
        gint n = 0;
        gdouble *dbl_arr = variant_to_double_array(value, &n);
        if (!dbl_arr)
            return;
        if (tgt[0] == 'a')
            new = variant_by_double_array(dbl_arr, n, tgt, src[1] == 'b');
        else
            new = variant_by_double(dbl_arr[0], tgt[0], src[1] == 'b');
        g_free(dbl_arr);
    } else if (src[1] == '\0') {
        if (!strchr("bynqiuxtdhs", src[0]))
            return;
        gdouble dbl_var = variant_to_double(value);
        if (tgt[0] == 'a')
            new = variant_by_double_array(&dbl_var, 1, tgt, src[0] == 'b');
        else
            new = variant_by_double(dbl_var, tgt[0], src[0] == 'b');
    } else {
        return;
    }
    if (!new)
        return;
    lbo_set_memory(pl->recv_handler, pl->recv_desc, new);
    g_variant_unref(new);
}

static void _save_persist_data(LBO *object, const LBProperty *desc, GVariant *value, gpointer user_data)
{
    guint per_type = GPOINTER_TO_UINT(user_data);
    persist_save_property(lbo_name(object), desc->info->name, value, per_type);
}

typedef struct {
    gconstpointer prop_listen_pointer;
    LBO *listen_handler;
    gchar *prop_name;
} PropReceiver;

static void on_recv_handler_free(PropReceiver *pr)
{
    lb_impl._on_prop_changed_cancel(pr->listen_handler, pr->prop_name, _remote_property_changed, pr->prop_listen_pointer);
    lbo_unref(&pr->listen_handler);
    g_free(pr->prop_name);
    g_free(pr);
}

/**
 * @brief 注册属性变更回调函数
 *
 * @param name 类名
 * @param cb 回调函数
 */
static void on_property_changed(const LBInterface *intf, const gchar *listen_obj, const gchar *listen_prop,
                                LBO *recv_handler, const gchar *recv_prop)
{
    g_assert(listen_obj && listen_prop && recv_handler && recv_prop);

    // 检查接收属性是否存在
    const LBProperty *recv_desc = lbo_get_property_desc(recv_handler, recv_prop);
    if (!recv_desc) {
        log_warn("Unkown receive property, skip reference property: %s.%s, receive property; %s", listen_obj,
                 listen_prop, lbo_name(recv_handler), recv_prop);
        return;
    }

    // rp需要一个对象，那就new一个，但不改变在位状态
    LBO *listen_handler = lbo_new(intf, listen_obj, NULL);
    if (!listen_handler) {
        log_warn("Get listen object failed , skip reference property: %s.%s, receive property; %s", listen_obj,
                 listen_prop, lbo_name(recv_handler), recv_prop);
        return;
    }
    PropListener *pl = g_new0(PropListener, 1);
    pl->recv_handler = recv_handler;
    pl->recv_desc = recv_desc;
    PropReceiver *pr = g_new0(PropReceiver, 1);
    pr->prop_name = g_strdup(listen_prop);
    pr->listen_handler = listen_handler;
    pr->prop_listen_pointer = pl;
    // recv_handler对象释放时触发listen_handler取消监听并且减引用计数
    lb_impl._before_destroy(recv_handler, (GHookFunc)on_recv_handler_free, pr);
    lb_impl._on_prop_changed(listen_handler, listen_prop, _remote_property_changed, (gpointer)pl, g_free);
}

static void _load_reference_property(const VConnector *vconn, LBO *obj, const LBProperty *prop, const gchar *ref_str)
{
    cleanup_gfree gchar *ref_interface = _get_ref_interface(ref_str);
    const LBInterface *intf = lb_get_interface(FALSE, ref_interface);
    if (!intf) {
        log_warn("Unkown registered interface, rp failed, property: %s.%s, ref prop; %s", lbo_name(obj),
                 prop->name, ref_str);
        return;
    }
    // 引用的属性名在最后一个.之后
    cleanup_gfree gchar *ref_prop = g_strdup(strrchr(ref_str, '.') + 1);
    ref_prop[strlen(ref_prop) - 1] = '\0';
    // 模拟对象引用语法，以利用_rename_object函数
    cleanup_gfree gchar *ref_obj = g_strdup(ref_str);
    gchar *pos = strrchr(ref_obj, '.');
    pos[0] = '}';
    pos[1] = '\0';
    // 删除最后一个'}'
    cleanup_gfree gchar *rename_obj = _rename_object(vconn, ref_obj);

    on_property_changed(intf, rename_obj, ref_prop, obj, prop->name);
}

static gint _load_persist_data(LBO *object, const LBProperty *desc, const gchar *flags)
{
    gint ret;
    gint result = -1;
    GVariant *value = NULL;
    cleanup_strv gchar **strv = g_strsplit(flags, ",", -1);
    PER_TYPE per_type;
    if (strstr(flags, "per_save")) {
        per_type = PER_SAVE;
    } else if (strstr(flags, "per_poweroff")) {
        per_type = PER_POWER_OFF;
    } else if (strstr(flags, "per_reboot")) {
        per_type = PER_REBOOT;
    } else {
        return 0;
    }
    ret = persist_load_property(lbo_name(object), desc->info->name, &value, per_type);
    if (ret == 0 && value) {
        const gchar *val_type = g_variant_get_type_string(value);
        if (g_strcmp0(desc->info->signature, val_type) == 0) {
            lbo_set_memory(object, desc, value);
            result = 0;
        }
        g_variant_unref(value);
    }
    // 注册事件时会触发变更事件，为避免异步事件覆盖持久化读取的值，先设置值再注册事件
    lb_impl._on_prop_changed(object, desc->info->name, _save_persist_data, GUINT_TO_POINTER(per_type), NULL);
    if (result != 0) {
        // 未正确加载时尝试从数据库删除数据
        persist_save_property(lbo_name(object), desc->info->name, NULL, per_type);
    }
    return result;
}

static inline gchar *_yaml_scalar(yaml_node_t *node)
{
    return (gchar *)node->data.scalar.value;
}

static void ref_load_ref_obj(LBO *obj, const LBProperty *prop, yaml_document_t *doc,
    yaml_node_t *node, gpointer user_data, const gchar *flags)
{
    VConnector *vconn = (VConnector *)user_data;
    if (doc == NULL || node == NULL) {
        g_assert(flags);
        _load_persist_data(obj, prop, flags);
        return;
    }
    if (node->type == YAML_SCALAR_NODE) {
        if (g_regex_match(lb_ref_obj_regex(), _yaml_scalar(node), 0, NULL)) {
            g_assert(g_strcmp0(prop->info->signature, "s") == 0);
            cleanup_gfree gchar *object_path = _rename_object(vconn, _yaml_scalar(node));
            cleanup_unref GVariant *data = g_variant_new_string(object_path);
            lbo_set_memory(obj, prop, data);
        }
        if (g_regex_match(lb_ref_prop_regex(), _yaml_scalar(node), 0, NULL)) {
            _load_reference_property(vconn, obj, prop, _yaml_scalar(node));
            return;
        }
    } else if (node->type == YAML_SEQUENCE_NODE) {
        g_assert(g_strcmp0(prop->info->signature, "as") == 0);

        yaml_node_item_t *top = node->data.sequence.items.top;
        yaml_node_item_t *start = node->data.sequence.items.start;
        gsize cnt = ((gsize)top - (gsize)start) / sizeof(yaml_node_item_t);
        cleanup_strv gchar **strv = g_new0(gchar *, cnt + 1);
        cnt = 0;
        for (yaml_node_item_t *item = start; item < top; item++) {
            yaml_node_t *value = yaml_document_get_node(doc, *item);
            if (value->type != YAML_SCALAR_NODE) {
                log_error("The type of %s.%s is not a scalar", lbo_name(obj), prop->name);
                continue;
            }
            gchar *object_path = _rename_object(vconn, _yaml_scalar(value));
            if (object_path) {
                strv[cnt++] = object_path;
            }
        }
        cleanup_unref GVariant *data = lb_array_string_encode((gchar **)strv);
        lbo_set_memory(obj, prop, data);
    }
}

static const gchar *_get_interface(yaml_document_t *doc, yaml_node_t *node)
{
    yaml_node_t *key;
    yaml_node_t *val;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        if (pair->key == 0) {
            continue;
        }
        key = yaml_document_get_node(doc, pair->key);
        if (!key || key->type != YAML_SCALAR_NODE) {
            log_mass("unexcept node get, key type: %d, need: 1(YAML_SCALE_NODE)", key->type);
            continue;
        }
        val = yaml_document_get_node(doc, pair->value);
        if (g_strcmp0(_yaml_scalar(key), "interface") == 0 && val && val->type == YAML_SCALAR_NODE) {
            log_mass("get interface: %s", _yaml_scalar(val));
            return _yaml_scalar(val);
        }
    }
    return NULL;
}

static yaml_node_t *_get_properties(yaml_document_t *doc, yaml_node_t *node)
{
    yaml_node_t *key;
    yaml_node_t *val;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        if (pair->key == 0) {
            continue;
        }
        key = yaml_document_get_node(doc, pair->key);
        if (!key || key->type != YAML_SCALAR_NODE) {
            log_mass("unexcept node get, key type: %d, need: 1(YAML_SCALE_NODE)", key->type);
            continue;
        }
        val = yaml_document_get_node(doc, pair->value);
        if (g_strcmp0(_yaml_scalar(key), "properties") == 0 && val && val->type == YAML_MAPPING_NODE) {
            return val;
        }
    }
    return NULL;
}

static void _make_connector_position(VConnector *vconn)
{
    Connector conn = vconn->conn;

    if (!conn->physical) {
        vconn->pos = g_strdup(vconn->prev->pos);
        return;
    }
    GString *tmp = g_string_sized_new(32);
    if (vconn->prev->pos[0] == '\0')
        g_string_append_printf(tmp, "_%02x", (unsigned char)(conn->position & 0xff));
    else
        g_string_append_printf(tmp, "%s%02x", vconn->prev->pos, (unsigned char)(conn->position & 0xff));
    vconn->pos = g_string_free(tmp, FALSE);
}

static void _del_connector(VConnector **vconn)
{
    g_free((*vconn)->pos);
    g_free((*vconn)->back.filename);
    g_free((*vconn));
    *vconn = NULL;
}

static void _add_connector(VConnector *prev_vconn, LBO *object)
{
    VConnector *new_vconn;

    new_vconn = g_new0(VConnector, 1);
    new_vconn->prev = prev_vconn;
    new_vconn->mds_path = prev_vconn->mds_path;
    new_vconn->conn = (Connector)object;
    _make_connector_position(new_vconn);
    if (prev_vconn)
        prev_vconn->next = g_slist_prepend(prev_vconn->next, (gpointer)new_vconn);
}

static void load_odf_objects(VConnector *vconn, yaml_document_t *doc, yaml_node_t *node)
{
    yaml_node_t *key;
    yaml_node_t *val;
    gboolean exist = FALSE;
    yaml_node_pair_t *top = node->data.mapping.pairs.top;
    yaml_node_pair_t *start = node->data.mapping.pairs.start;
    gsize n = ((gsize)top - (gsize)start) / sizeof(yaml_node_pair_t);
    LBO **items = g_new0(LBO *, n + 1);
    gint cnt = 0;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        if (pair->key == 0) {
            continue;
        }
        key = yaml_document_get_node(doc, pair->key);
        val = yaml_document_get_node(doc, pair->value);
        const gchar *object_path = _yaml_scalar(key);
        log_mass("load object %s", object_path);
        const gchar *interface = _get_interface(doc, val);
        if (!interface) {
            log_error("Get interface of object %s failed", object_path);
            continue;
        }
        yaml_node_t *properties = _get_properties(doc, val);
        const LBInterface *intf = lb_get_interface(FALSE, interface);
        if (!intf) {
            log_error("Unknown interface %s get", interface);
            continue;
        }
        exist = FALSE;
        cleanup_gfree gchar *real_obj = _rename_object(vconn, object_path);
        if (!real_obj) {
            log_error("rename object %s failed", object_path);
            continue;
        }
        LBO *obj = lbo_new(intf, real_obj, &exist);
        if (obj == NULL) {
            log_error("Create object %s failed", real_obj);
            continue;
        }
        lbo_present_set(obj, FALSE);

        if (properties) {
            lbo_lock(obj);
            intf->load_from_odf(doc, properties, obj, ref_load_ref_obj, vconn);
            lbo_unlock(obj);
        }

        if (!exist) {
            if (!g_strcmp0(interface, CONNECTOR->name)) {
                log_info("Add connector object: %s", lbo_name(obj));
                _add_connector(vconn, obj);
            }
            lbo_ref(obj);
            vconn->objects = g_slist_prepend(vconn->objects, (gpointer)obj);
        }
        items[cnt++] = obj;
    }
    for (int i = 0; i < cnt; i++) {
        lbo_present_set(items[i], TRUE);
        lbo_unref(&(items[i]));
    }
    g_free(items);
}

static void _load_odf_top(yaml_document_t *doc, yaml_node_t *node, VConnector *vconn, const gchar *odf_file)
{
    yaml_node_t *key;
    yaml_node_t *val;
    const gchar *service = NULL;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        if (pair->key == 0) {
            continue;
        }
        key = yaml_document_get_node(doc, pair->key);
        val = yaml_document_get_node(doc, pair->value);
        if (key->type == YAML_SCALAR_NODE) {
            if (strcmp(_yaml_scalar(key), "service") == 0 && val->type == YAML_SCALAR_NODE) {
                service = _yaml_scalar(val);
            }
        }
    }
    if (!service) {
        log_error("ODF %s with error: service is missing", odf_file);
        return;
    }
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        if (pair->key == 0) {
            continue;
        }
        key = yaml_document_get_node(doc, pair->key);
        val = yaml_document_get_node(doc, pair->value);
        if (key->type == YAML_SCALAR_NODE && strcmp(_yaml_scalar(key), "objects") == 0) {
            if (val->type != YAML_MAPPING_NODE) {
                log_error("ODF %s with error, \"objects\" is not a object", odf_file);
            } else {
                load_odf_objects(vconn, doc, val);
            }
            return;
        }
    }
}

static gint _load_odf_file(VConnector *vconn, const gchar *odf_file)
{
    FILE *file;
    yaml_parser_t parser;
    yaml_document_t document;
    yaml_node_t *node = NULL;
    gint ret = -1;

    // 首先调用插件完成ODF验证，未通过验证的不予以加载
    Connector_OdfValidate_Req req = {
        .odf_file = odf_file
    };
    ret = Connector_OdfValidate_run(NULL, &req);
    if (ret != 0) {
        log_warn("Valiate odf %s failed, skip load", odf_file);
        return -1;
    }
    file = fopen(odf_file, "rb");
    if (!file) {
        log_warn("Open odf file %s failed, error: %s", odf_file, strerror(errno));
        return ret;
    }

    yaml_parser_initialize(&parser);
    yaml_parser_set_input_file(&parser, file);
    if (vconn->conn)
        log_info("Start load odf file %s, position: %u, physical: %u", odf_file, vconn->conn->position, vconn->conn->physical);
    else
        log_info("Start load odf file %s", odf_file);

    do {
        if (!yaml_parser_load(&parser, &document)) {
            break;
        }

        node = yaml_document_get_root_node(&document);
        if (node && node->type == YAML_MAPPING_NODE) {
            _load_odf_top(&document, node, vconn, odf_file);
            ret = 0;
        }

        yaml_document_delete(&document);

    } while (node);

    yaml_parser_delete(&parser);

    fclose(file);
    return ret;
}

static gchar *_make_odf_pathname(VConnector *vconn)
{
    GString *odf_file;

    odf_file = g_string_sized_new(64);
    /* 第一层odf属于平台层，固定为app同级mds目录下的base.yaml */
    if (vconn == &g_top_vconn) {
        g_string_printf(odf_file, "%s/mds/base.yaml", vconn->mds_path);
    } else {
        Connector conn = vconn->conn;
        if (!conn->filename) {
            log_warn("connector (%s) filename is nil, return NULL", lbo_name((LBO *)vconn->conn));
            return NULL;
        }
        g_string_printf(odf_file, "%s/mds/%s", vconn->mds_path, conn->filename);
    }
    return g_string_free(odf_file, FALSE);
}

/*
 * Notes: 加载conn前需要从对象加载conn的属性
 */
static gint _load_odf_profile(VConnector *vconn)
{
    cleanup_gfree gchar *odf_file = _make_odf_pathname(vconn);
    if (!odf_file) {
        log_error("odf file %s not exist", odf_file);
        return -1;
    }
    return _load_odf_file(vconn, odf_file);
}

static void _connector_unload(VConnector *vconn)
{
    log_debug("unload connector %s", lbo_name((LBO *)vconn->conn));
    for (GSList *item = vconn->next; item; item = item->next) {
        VConnector *next = (VConnector *)item->data;
        _connector_unload(next);
        _del_connector(&next);
    }
    g_slist_free(vconn->next);
    vconn->next = NULL;
    for (GSList *item = vconn->objects; item; item = item->next) {
        LBO *obj = (LBO *)item->data;
        lbo_present_set(obj, FALSE);
        lbo_unref(&obj);
    }
    g_slist_free(vconn->objects);
    vconn->objects = NULL;
    vconn->back.present = 0;
}

static inline gint _connector_load(VConnector *vconn)
{
    return _load_odf_profile(vconn);
}

static void _connector_check(VConnector *vconn)
{
    Connector conn = vconn->conn;

    log_mass("check %s, present: %d", lbo_name(conn), conn->present);
    if (conn->present) {
        if (g_strcmp0(conn->filename, vconn->back.filename) || conn->position != vconn->back.position || conn->physical != vconn->back.physical) {
            /* 在位时连接器基本属性变更，先卸载，如果在位，由下个循环完成重新加载 */
            log_debug("Connector %s changed, unload first", lbo_name((LBO *)conn));
            _connector_unload(vconn);

            /* 记录已处理的状态 */
            vconn->back.position = conn->position;
            vconn->back.physical = conn->physical;
            lbo_lock((LBO *)conn);
            g_free(vconn->back.filename);
            vconn->back.filename = g_strdup(conn->filename);
            vconn->back.present = 0;
            lbo_unlock((LBO *)conn);
        }
        if (!vconn->back.present) {
            log_debug("Connector %s loading", lbo_name((LBO *)conn));
            if (_connector_load(vconn) != 0)
                log_warn("Connector %s loaded failed, skip", lbo_name((LBO *)conn));
            vconn->back.present = 1;
        }
        /* 连接器信息未变更时检查下一级 */
        for (GSList *item = vconn->next; item; item = item->next)
            _connector_check((VConnector *)item->data);
    } else if (vconn->back.present) {
        log_debug("Connector %s unload because of not present", lbo_name((LBO *)conn));
        _connector_unload(vconn);
        vconn->back.present = 0;
    }
}

static void _connector_monitor(VConnector *vconn)
{
    while (1) {
        g_usleep(G_USEC_PER_SEC / 100);
        for (GSList *item = vconn->next; item; item = item->next) {
            _connector_check((VConnector *)item->data);
        }
    }
}

// 加载进程私有的mds
static void _load_global_mds(void)
{
    struct stat st;
    cleanup_gfree gchar *rootfs_path = get_rootfs_path("/");
    cleanup_gstring GString *mds_path = g_string_sized_new(128);
    if (lb_get_bus_type() == G_BUS_TYPE_SESSION) {
        g_string_printf(mds_path, "%sopt/litebmc/mds/%s/session/", rootfs_path, lb_bus_name());
    } else {
        g_string_printf(mds_path, "%sopt/litebmc/mds/%s/system/", rootfs_path, lb_bus_name());
    }
    cleanup_gstring GString *odf_file = g_string_new(mds_path->str);
    g_string_append(odf_file, "base.yml");
    if (stat(odf_file->str, &st) != 0) {
        log_debug("open global mds %s failed, error: %s", odf_file->str, strerror(errno));
        return;
    }
    if (!S_ISREG(st.st_mode)) {
        log_warn("global mds %s not a regular file, load failed", odf_file->str);
        return;
    }

    VConnector *new_vconn;

    new_vconn = g_new0(VConnector, 1);
    new_vconn->mds_path = g_string_free(mds_path, FALSE);
    new_vconn->pos = "";
    mds_path = NULL;
    gint ret = _load_odf_file(new_vconn, odf_file->str);
    if (ret == 0) {
        GThread *task = g_thread_new("ConnMonitor", (GThreadFunc)_connector_monitor, new_vconn);
        g_thread_unref(task);
    }
}

// 加载程序自管理的mds
static void _load_private_mds(void)
{
    gint ret;
    GThread *task;
    log_info("Start connector");
    g_top_vconn.mds_path = get_app_dir();
    ret = _load_odf_profile(&g_top_vconn);
    if (ret == 0) {
        log_info("load top connector success");
    }

    task = g_thread_new("ConnMonitor", (GThreadFunc)_connector_monitor, &g_top_vconn);
    g_thread_unref(task);
}

static void _connector_start(void)
{
    // TODO: 需要新增机制检测私有mds和全局mds使用相同对象名场景
    _load_private_mds();
    _load_global_mds();
}

static void __attribute__((constructor(200))) _lb_init(void)
{
    log_info("Init connector");
    // 最低优先级，最后加载
    lb_module_register(_connector_start, "connector", 9);
}
