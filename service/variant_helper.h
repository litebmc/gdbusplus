#ifndef __VARIANT_HELPER_H__
#define __VARIANT_HELPER_H__

#ifdef __cplusplus
extern "C" {
#endif

gdouble variant_to_double(GVariant *value);
gdouble *variant_to_double_array(GVariant *value, gint *cnt);

GVariant *variant_by_double(gdouble value, gchar tgt_type, gboolean src_is_bool);
GVariant *variant_by_double_array(const gdouble *value, gint cnt, const gchar *tgt_type, gboolean src_is_bool);

#ifdef __cplusplus
}
#endif

#endif